import numpy as np
#import matplotlib.pyplot as plt
#import matplotlib.patches as patches
import sys
import caffe
import cv2
import os
import json
from scipy.io import loadmat
from scipy.misc import imshow, imread

# Set the right path to your model definition file, pretrained model weights,
# and the image you would like to classify.
MODEL_FILE = 'models/itracker_deploy.prototxt'
PRETRAINED = 'models/itracker_iter_92000.caffemodel'

# Choose infer data
base_dir = "data/00033"

def normalize(img):
    xy_dim = 224/img.shape[0]
    img = cv2.resize(img, dsize=(224,224), fx=xy_dim, fy=xy_dim, interpolation = cv2.INTER_CUBIC)
    img *=255
    img = img.astype(np.uint8)
    return img

def getFrameFilePaths(frames_fp):
    f = open(frames_fp, "r")
    frames_fps = json.loads(f.read())
    f.close()
    return [os.path.join(base_dir, "frames", fp) for fp in frames_fps]

def getJSON(fp):
    f = open(fp, "r")
    obj = json.loads(f.read())
    f.close()
    return obj

def getValidPatch(obj, i):
    if not obj["IsValid"]:
        return None
    xy = (obj["X"][i], obj["Y"][i])
    h, w = (obj["H"][i], obj["W"][i])
    return [xy, w, h]

#def getRect(patch, linewidth=1, edgecolor="r", facecolor="none"):
#    return patches.Rectangle(
#        xy=patch[0],
#        width=patch[1],
#        height=patch[2],
#        linewidth=linewidth,
#        edgecolor=edgecolor,
#        facecolor=facecolor)

def getCroppedImg(img, patch):
    x, y = patch[0]
    h, w = (patch[1], patch[2])
    return img[int(y):int(y+h), int(x):int(x+w)]

def fillGrid(img, patch, scale=1):
    x, y = patch[0]
    h, w = (patch[1], patch[2])
    for i in range(h*scale):
        for j in range(w*scale):
            img[y+i, x+j] =  0
    return img

left_eye_fp = os.path.join(base_dir, "appleLeftEye.json")
right_eye_fp = os.path.join(base_dir, "appleRightEye.json")
face_fp = os.path.join(base_dir, "appleFace.json")
facegrid_fp = os.path.join(base_dir, "faceGrid.json")
frames_fp = os.path.join(base_dir, "frames.json")

frames_fp = getFrameFilePaths(frames_fp)

left_eye_obj = getJSON(left_eye_fp)
right_eye_obj = getJSON(right_eye_fp)
face_obj = getJSON(face_fp)
facegrid_obj = getJSON(facegrid_fp)

# Visualize data
# for i, f in enumerate(frames_fp):
#     face_patch = getValidPatch(face_obj, i)
#     left_eye_patch = getValidPatch(left_eye_obj, i)
#     right_eye_patch = getValidPatch(right_eye_obj, i)
#     face_grid_patch = getValidPatch(face_grid_obj, i)
#
#     left_eye_patch[0] = tuple([sum(x) for x in zip(face_patch[0], left_eye_patch[0])])
#     right_eye_patch[0] = tuple([sum(x) for x in zip(face_patch[0], right_eye_patch[0])])
#
#     if (left_eye_patch or right_eye_patch or face_patch or face_grid_patch) == None:
#         continue
#
#     left_eye_rect = getRect(left_eye_patch)
#     right_eye_rect = getRect(right_eye_patch, edgecolor="b")
#     face_rect = getRect(face_patch, edgecolor="g")
#     face_grid_rect = getRect(face_grid_patch, edgecolor="k")
#
#     fig = plt.figure()
#     ax = fig.add_subplot(2,1,1)
#     ax2 = fig.add_subplot(2,1,2)
#
#     img = imread(f)
#     ax.imshow(img)
#     ax.add_patch(left_eye_rect)
#     ax.add_patch(right_eye_rect)
#     ax.add_patch(face_rect)
#
#     arr = np.ones((25,25, 3), dtype=np.uint8)
#     arr *= 255
#     ax2.imshow(arr)
#     ax2.add_patch(face_grid_rect)
#
#     plt.show()

# Process data for network

left_eye_batch = np.array([])
right_eye_batch = np.array([])
face_batch = np.array([])
facegrid_batch = np.array([])

for i, f in enumerate(frames_fp):
    img = imread(f)
    face_patch = getValidPatch(face_obj, i)
    left_eye_patch = getValidPatch(left_eye_obj, i)
    right_eye_patch = getValidPatch(right_eye_obj, i)
    facegrid_patch = getValidPatch(facegrid_obj, i)

    left_eye_patch[0] = tuple([sum(x) for x in zip(face_patch[0], left_eye_patch[0])])
    right_eye_patch[0] = tuple([sum(x) for x in zip(face_patch[0], right_eye_patch[0])])

    face_img = getCroppedImg(img, face_patch)
    left_eye_img = getCroppedImg(img, left_eye_patch)
    right_eye_img = getCroppedImg(img, right_eye_patch)

    scale = 1
    arr = np.ones((25*scale,  25*scale, 3), dtype=np.uint8)
    arr *= 255
    facegrid_img = fillGrid(arr, facegrid_patch, scale)

    right_eye = normalize(right_eye_img)
    right_eye = np.moveaxis(right_eye, -1, 0)

    left_eye = normalize(right_eye_img)
    left_eye = np.moveaxis(left_eye, -1, 0)

    face = normalize(face_img)
    face = np.moveaxis(face, -1, 0)

    facegrid = facegrid_img[:,:,0].flatten()
    facegrid = np.reshape(facegrid, facegrid.shape+(1,1))

    left_eye = np.reshape(left_eye, (1,)+ left_eye.shape)
    right_eye = np.reshape(right_eye, (1,)+right_eye.shape)
    face = np.reshape(face, (1,)+face.shape)
    facegrid = np.reshape(facegrid, (1,)+facegrid.shape)

    if i == 0:
        left_eye_batch = left_eye
        right_eye_batch = right_eye
        face_batch = face
        facegrid_batch = facegrid
    else:
        left_eye_batch = np.concatenate((left_eye_batch, left_eye), axis=0)
        right_eye_batch = np.concatenate((right_eye_batch, right_eye), axis=0)
        face_batch = np.concatenate((face_batch, face), axis=0)
        facegrid_batch = np.concatenate((facegrid_batch, facegrid), axis=0)

    #imshow(face_batch[i])
    #imshow(left_eye_batch[i])
    #imshow(right_eye_batch[i])

    print(face_batch.shape, left_eye_batch.shape, right_eye_batch.shape, facegrid_batch.shape)

    if i == 9:
        break

# load the model
caffe.set_mode_gpu()
# caffe.set_mode_cpu()
caffe.set_device(0)
net = caffe.Net(MODEL_FILE, PRETRAINED, caffe.TEST)
                       # mean=np.load('data/train_mean.npy').mean(1).mean(1),
                       # mean=loadmat('models/mean_face_224.mat')['image_mean'].mean()
                       # channel_swap=(2,1,0),
                       # raw_scale=255,
                       # image_dims=(256, 256)

print("successfully loaded classifier")

net.blobs['image_left'].data[...] = left_eye_batch
net.blobs['image_right'].data[...] = right_eye_batch
net.blobs['image_face'].data[...] = face_batch
net.blobs['facegrid'].data[...] = facegrid_batch

# predict takes any number of images,
# and formats them for the Caffe net automatically
pred = net.forward()
# pred = net.forward(input)
print(pred)
