import tensorflow as tf
import numpy as np
import glob
import json
import cv2
from scipy.misc import imread, imshow
import os


# Network Parameters
img_size = 64

def getJSON(fp):
    f = open(fp, "r")
    obj = json.loads(f.read())
    f.close()
    return obj

def next_batch(data, batch_size):
    for i in np.arange(0, data[0].shape[0], batch_size):
        # yield a tuple of the current batched data
        yield [each[i: i + batch_size] for each in data]

def load(sess, save_dir, **builder_kwargs):
    """
    Loads metagraph and weights to current session from data in save_dir.
    """
    if not "tags" in builder_kwargs:
        builder_kwargs["tags"] = []
    tf.saved_model.loader.load(sess, export_dir=save_dir, **builder_kwargs)

def normalize(data):
    shape = data.shape
    data = np.reshape(data, (shape[0], -1))
    data = data.astype('float32') / 255. # scaling
    data = data - np.mean(data, axis=0) # normalizing
    return np.reshape(data, shape)

def normalize_list(data):

    for i in range(len(data)):
        img = data[i]
        img = img.astype("float32")/255
        img -= total_mean
        # for j in range(3):
        #     img[:,:,j] -= np.mean(img[:,:,j], axis=0)
        data[i] = img
    return data

def prepare_data(data):
    eye_left, eye_right, face, face_mask, y = data
    eye_left = normalize(eye_left)
    eye_right = normalize(eye_right)
    face = normalize(face)
    face_mask = np.reshape(face_mask, (face_mask.shape[0], -1)).astype('float32')
    y = y.astype('float32')
    # print(eye_left.shape)
    # exit(1)
    print(eye_left.shape, eye_right.shape, face.shape, face_mask.shape, y.shape)
    return [eye_left, eye_right, face, face_mask, y]

def load_data(file):
    npzfile = np.load(file)
    train_eye_left = npzfile["train_eye_left"]
    train_eye_right = npzfile["train_eye_right"]
    train_face = npzfile["train_face"]
    train_face_mask = npzfile["train_face_mask"]
    train_y = npzfile["train_y"]
    val_eye_left = npzfile["val_eye_left"]
    val_eye_right = npzfile["val_eye_right"]
    val_face = npzfile["val_face"]
    val_face_mask = npzfile["val_face_mask"]
    val_y = npzfile["val_y"]
    return [train_eye_left, train_eye_right, train_face, train_face_mask, train_y], [val_eye_left, val_eye_right, val_face, val_face_mask, val_y]

def load_subject_data(subject_dir):
    face_fps = glob.glob(os.path.join(subject_dir, "appleFace/*"))
    left_eye_fps = glob.glob(os.path.join(subject_dir, "appleLeftEye/*"))
    right_eye_fps = glob.glob(os.path.join(subject_dir, "appleRightEye/*"))
    facegrid_fps = glob.glob(os.path.join(subject_dir, "faceGrid/*"))

    dot_info_fp = os.path.join(subject_dir, "dotInfo.json")
    dot_info = getJSON(dot_info_fp)
    dot_indices = [int(os.path.basename(fp).split(".")[0]) for fp in face_fps]

    #Read images
    faces = [cv2.resize(imread(fp), dsize=(img_size, img_size),
            interpolation=cv2.INTER_AREA) for fp in face_fps]
    left_eyes = [cv2.resize(imread(fp), dsize=(img_size, img_size),
            interpolation=cv2.INTER_AREA) for fp in left_eye_fps]
    right_eyes = [cv2.resize(imread(fp), dsize=(img_size, img_size),
            interpolation=cv2.INTER_AREA) for fp in right_eye_fps]
    facegrids = [(imread(fp)//255).astype("float32") for fp in facegrid_fps]

    y = [np.array([dot_info["XCam"][i], dot_info["YCam"][i]]) for i in dot_indices]

    subject_facegrid = [np.reshape(fg, fg.shape[-1]) for fg in facegrids]

    # Concatenate all images into a subject batch
    subject_face = np.stack(faces, axis=0)
    subject_left_eye = np.stack(left_eyes, axis=0)
    subject_right_eye = np.stack(right_eyes, axis=0)
    subject_facegrid = np.stack(subject_facegrid, axis=0)

    #Normalize images (i.e. map to 0..1 range, subtract mean, and scale to 64x64)
    subject_face = normalize(subject_face)
    subject_left_eye = normalize(subject_left_eye)
    subject_right_eye = normalize(subject_right_eye)
    subject_y = np.stack(y, axis=0)



    print(subject_face.shape, subject_left_eye.shape, subject_right_eye.shape,
          subject_facegrid.shape, subject_y.shape)

    return [subject_left_eye, subject_right_eye, subject_face,
            subject_facegrid, subject_y]

def validate_model(session, val_data, val_ops, batch_size=200):
    """ Validates the model stored in a session.
    Args:
        session: The session where the model is loaded.
        val_data: The validation data to use for evaluating the model.
        val_ops: The validation operations.
    Returns:
        The overall validation error for the model. """
    print ("Validating model...")

    eye_left, eye_right, face, face_mask, pred = val_ops
    y = tf.placeholder(tf.float32, [None, 2], name='pos')
    err = tf.reduce_mean(tf.sqrt(tf.reduce_sum(tf.squared_difference(pred, y), axis=1)))
    # Validate the model.
    # val_n_batches = val_data[0].shape[0] / batch_size + (val_data[0].shape[0] % batch_size != 0)
    # val_err = 0
    # for batch_val_data in next_batch(val_data, batch_size):
    val_batch_err, pred_out, y_out = session.run([err, pred, y], feed_dict={eye_left:  val_data[0], \
                                eye_right:  val_data[1], face:  val_data[2], \
                                face_mask:  val_data[3], y:  val_data[4]})
        # print(pred_out, y_out)
        # val_err += val_batch_err / val_n_batches
    return val_batch_err

def extract_validation_handles(session):
    """ Extracts the input and predict_op handles that we use for validation.
    Args:
        session: The session with the loaded graph.
    Returns:
        validation handles.
    """
    valid_nodes = tf.get_collection("validation_nodes")
    print(valid_nodes)
    if len(valid_nodes) != 5:
        raise Exception("ERROR: Expected 5 items in validation_nodes, got %d." % len(valid_nodes))
    return valid_nodes

def test(sess, load_data_dir, load_model_dir, batch_size):
    # _, val_data = load_data(load_data_dir)
    #
    # val_size = 10
    # val_data = [each[:val_size] for each in val_data]
    #
    # val_data = prepare_data(val_data)

    subject_nr = int(os.path.basename(load_data_dir).split(".")[0])
    val_data = load_subject_data(load_data_dir)

    # Load and validate the network.
    val_ops = extract_validation_handles(sess)
    error = validate_model(sess, val_data, val_ops, batch_size=batch_size)
    print ('Subject %d validation error: %f' % (subject_nr, error))
    return error

if __name__ == "__main__":
    # subject_dir = os.path.join(subject_base_dir, "00003")
    # load_data_dir = "/mnt/storage_local/data/gazecapture_alt/eye_tracker_train_and_val.npz"

    load_model_dir = "saved_model/best"
    batch_size = 8
    subject_base_dir = "/home/rafael/Documents/unicamp/ic/gazecapture/data/raw/*"
    subject_dirs = glob.glob(subject_base_dir)

    with tf.Session() as sess:
        print ("Loading model from file '%s'..." % load_model_dir)
        load(sess, load_model_dir, tags=[tf.saved_model.tag_constants.TRAINING])

        total_error = 0
        for sd in subject_dirs:
            total_error += test(sess, sd, load_model_dir, batch_size)
        total_error /= len(subject_dirs)

    print("Final total error = ", total_error)
