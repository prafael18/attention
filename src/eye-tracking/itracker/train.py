import os
import sys
import glob
import time
import shutil
import argparse
import subprocess

import numpy as np
import tensorflow as tf
from tensorflow.python import debug as tf_debug

import data
import network
from util import timef, clean_dirs

# TODO:
#
# Remove saving iterator state
# Add mean subtraction to images
# Understand itracker_solver.prototxt
# Run inference with pytorch trained model
# Check if older models had negative pred values

def main(tfr_base_dir, runs_base_dir, total_steps, steps_per_val,
        steps_per_chkpt, resume_training, debug, clean, flow):

    # Find a unique id for the newest run
    run_dirs = sorted(glob.glob(os.path.join(runs_base_dir, 'run-*')))
    current_run_id = 1
    if run_dirs:
        run_ids  = sorted([int(r.split('-')[-1]) for r in run_dirs])
        current_run_id = run_ids[-1] + 1

    base_export_dir = os.path.join(runs_base_dir, f'run-{current_run_id}', 'models')
    log_dir = os.path.join(runs_base_dir, f'run-{current_run_id}', 'logs')

    train_filenames = glob.glob(os.path.join(tfr_base_dir, "train", "*.tfr"))
    val_filenames = glob.glob(os.path.join(tfr_base_dir, "val", "*.tfr"))

    if len(train_filenames) == 0:
        print(f"No training data found with tfr_base_dir={tfr_base_dir}")
        return
    if len(val_filenames) == 0:
        print(f"No validation data found with tfr_base_dir={tfr_base_dir}")
        return

    # Clean model and logging directories before begginning training
    if clean:
        clean_dirs([base_export_dir, log_dir])

    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Write current git HEAD's SHA to log dir
    with open(os.path.join(log_dir, 'commit-sha.txt'), 'a') as f:
        sha = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip()
        f.write(sha.decode('ascii')+"\n")


    # Get train and val dataset objects
    augm_params_fn = os.path.join(log_dir, 'augm_params.json')
    train_dataset = data.get_dataset(
        filenames=train_filenames,
        batch_size=256,
        train=True,
        flow=flow,
        augmentation_factor=1
        # save_fn=augm_params_fn
    )
    val_dataset = data.get_dataset(
        filenames=val_filenames,
        batch_size=256,
        train=False,
        flow=flow,
        augmentation_factor=1
    )

    # Only allocate GPU memory as needed
    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True

    with tf.Session(config=config) as sess:
        # Enable CLI debugging
        if debug:
            sess = tf_debug.LocalCLIDebugWrapperSession(sess)

        # Initialize network object with model definition. Already initializes variables
        itracker = network.iTrackerNetwork(
            session=sess,
            train_dataset=train_dataset,
            val_dataset=val_dataset,
            base_export_dir=base_export_dir,
            log_dir=log_dir,
            resume_training=resume_training,
            flow=flow
        )

        # We want to train up to step 150,000, so we have to check at what step
        # we're at for every iteration. In this case, an infinite loop is more
        # appropriate since the number of epochs shouldn't be a limiting factor
        # for training time

        try:
            while True:

                # Initialize iterator with training data for one epoch
                sess.run(itracker.train_iterator_init)

                # Keep track of time per epoch
                epoch_start = time.time()

                # Initialize accumulated train variables to 0 before starting new epoch
                train_samples = 0
                train_batches_loss = []

                while True:
                    step = tf.train.get_global_step().eval()

                    # Adaptive learning rate with respect to training step
                    if step < total_steps//2:
                        feed_dict = {"learning_rate:0":1e-3}
                    elif step < total_steps:
                        feed_dict = {"learning_rate:0":1e-4}

                    try:
                        # Add training handle to feed dict
                        feed_dict["data/handle:0"] = itracker.train_handle

                        # Run training op and return loss along with batch size
                        _, train_loss, n = sess.run((itracker.train_op, itracker.loss, itracker.batch_size),
                                                     feed_dict=feed_dict)

                        # Update train loss data
                        train_samples += n
                        train_batches_loss.append(train_loss*n)

                        # Compute new step after running batch
                        step = tf.train.get_global_step().eval()

                        # Log a per batch train loss for every step
                        itracker.log_scalar('batch_train_loss', train_loss, step)

                        print(f"{itracker.elapsed_time(sess)} "\
                            f"Current batch training loss ({step}/{total_steps}): {train_loss:.3f}       \r",
                            end="")

                        # In case we finished training, run final routine and exit
                        if step == total_steps:
                            print(f"{itracker.elapsed_time(sess)} Finished training for {step} iterations.     ")
                            # Save the latest model with `training` tag
                            itracker.save(session=sess, training=True, overwrite=True)
                            # After finishing training, validate one last time and save
                            # model in case it got more accurate
                            itracker.validate(session=sess)
                            return

                        # Save model variables every `save_iterations`
                        step = tf.train.get_global_step().eval()
                        if step%steps_per_chkpt == 0:
                            itracker.save(session=sess, training=True, overwrite=True)

                        # Validate data after every `steps_per_val` steps and
                        # save model in case of best validation loss
                        if step%steps_per_val == 0:
                            itracker.validate(session=sess)

                    except tf.errors.OutOfRangeError:
                        # After finishing current epoch, we want to allow iterator reinitialization
                        resume_training = False

                        # Log a per epoch train loss (mean over all batches)
                        train_loss = np.sum(train_batches_loss)/train_samples
                        itracker.log_scalar('epoch_train_loss', train_loss, step)

                        epoch_time = time.time() - epoch_start

                        # Increment epoch counter
                        epoch = sess.run(itracker.epoch.assign_add(1))
                        print(f"{itracker.elapsed_time(sess)} "\
                            f"Completed {epoch} epochs of training "\
                            f"(time = {timef(epoch_time)}, loss={train_loss:.3f})        ")
                        break

                # itracker.validate(session=sess)

        except KeyboardInterrupt:
            step = tf.train.get_global_step().eval()
            print(f"\nTraining interrupted at step {step}.        ")
            itracker.save(session=sess, training=True, overwrite=True)
            print("Now exiting...")
            return


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--tfr_base_dir', action='store', required=True,
                        help='Base dataset directory where all subject dirs are located')
    parser.add_argument('-e', '--runs_base_dir', action='store', required=True,
                        help='Base directory to save models and logs for training run')
    parser.add_argument('-t', '--total_steps', action='store', type=int, required=False, default=15000,
                        help='Number of steps to train model')
    parser.add_argument('-v', '--steps_per_val', action='store', type=int, required=False, default=500,
                        help='Number of steps before computing loss on validation data')
    parser.add_argument('-c', '--steps_per_chkpt', action='store', type=int, required=False, default=1000,
                        help='Number of steps before saving checkpoint')
    parser.add_argument('-r', '--resume_training', action='store_true', required=False,
                        help='Flag to resume training from saved checkpoint in `export_dir`')
    parser.add_argument('--debug', action='store_true', required=False,
                        help='Attach CLI tfdbg to session')
    parser.add_argument('--clean', action='store_true', required=False,
                        help='Purge models base export dir and logging dir')
    parser.add_argument('--flow', action='store_true', required=False,
                        help='Train model using optical flow')

    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    main(**kwargs)
