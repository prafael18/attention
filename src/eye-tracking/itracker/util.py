import os
import glob
import json
import shutil

import scipy.io
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

def read_dict(filename):
    with open(filename, 'r') as f:
        return json.load(f)

def save_dict(dict, filename):
    with open(filename, 'w') as f:
        json.dump(dict, f, sort_keys=True, indent=2)

def timef(seconds):
  minutes, seconds = divmod(int(seconds), 60)
  hours, minutes = divmod(minutes, 60)
  days, hours = divmod(hours, 24)
  time_str = f"{hours:02}h:{minutes:02}m:{seconds:02}s"
  if days > 0:
      time_str = f"{days:02}d:{time_str}"
  return f"({time_str})"

def clean_dirs(directories):
    for dir in directories:
        files = glob.glob(os.path.join(dir, "*"))
        for f in files:
            if os.path.isdir(f):
                shutil.rmtree(f)
            else:
                os.remove(f)
    print(f"Cleaned all files and directories from {directories}")

def retrieve_var(name):
    vs = [v for v in tf.global_variables() if v.name == f"{name}:0"]
    return vs[0]

def load_mat(name, dtype=np.float32):
    mat = scipy.io.loadmat(name)
    mat = mat['image_mean']
    mat = mat.astype(dtype)
    return mat

def scale(x, min=0, max=255):
    # Scale an array to interval [min, max]
    m = (max - min)/(np.max(x) - np.min(x)) # slope of linear transformation
    return (x-np.min(x))*m + min

def clip(x, min=0, max=255):
    x[x < min] = min
    x[x > max] = max
    return x

def show_sample(features):

    # Remove batch dimension in case there is one
    features = tuple(map(
        lambda feat: np.squeeze(feat),
        features
    ))
    face, reye, leye, fgrid, dinfo = features

    # Add mean to image features
    face += mean_face
    reye += mean_reye
    leye += mean_leye

    face, reye, leye = tuple(map(
        lambda feat: np.clip(feat, 0, 255).astype(np.uint8),
        (face, reye, leye)
    ))
    fgrid = np.squeeze(fgrid)
    print(f"Dot info: x={dinfo[0]}, y={dinfo[1]}")
    plt.clf()
    ax1 = plt.subplot(231)
    ax1.imshow(face)
    ax2 = plt.subplot(232)
    ax2.imshow(reye)
    ax3 = plt.subplot(233)
    ax3.imshow(leye)
    ax4 = plt.subplot(235)
    ax4.imshow(fgrid, cmap='gray')
    plt.waitforbuttonpress()


# Load mean images into memory for data normalization
mean_face = load_mat('assets/mean_face_224.mat')
mean_reye = load_mat('assets/mean_right_224.mat')
mean_leye = load_mat('assets/mean_left_224.mat')
