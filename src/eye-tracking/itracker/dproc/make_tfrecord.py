import os
import glob
import argparse

import cv2
import numpy as np
import tensorflow as tf

import util
import subject

def clean_dir(tfr_base_dir):
    dirs = ['train', 'val', 'test']
    dirs = list(map(lambda set: os.path.join(tfr_base_dir, set), dirs))
    for d in dirs:
        fps = glob.glob(os.path.join(d, '*'))
        for f in fps:
            os.remove(f)

def write_tfrecord(samples, tfr_fp, split_name, file_id, flow):
    if flow:
        tfr_basename = f'{split_name}-optflow-{file_id}.tfr'
    else:
        tfr_basename = f'{split_name}-{file_id}.tfr'

    tfr_filename = os.path.join(tfr_fp, tfr_basename)
    writer = tf.python_io.TFRecordWriter(tfr_filename)

    for i in range(len(samples)):
        if flow:
            face, reye, leye, fgrid, dinfo, optflow = samples[i]
        else:
            face, reye, leye, fgrid, dinfo = samples[i]

        # Encode all images as pngs and convert arrays to byte strings
        # Here we assume all images are BGR so OpenCV can convert them to an RGB byte string
        encoded_face, encoded_reye, encoded_leye, encoded_fgrid = tuple(map(
            lambda feat: tf.compat.as_bytes(cv2.imencode('.png', feat)[1].tobytes()),
            (face, reye, leye, fgrid)
        ))

        feature = {
            'face' : util.byte_feature(encoded_face),
            'reye' : util.byte_feature(encoded_reye),
            'leye' : util.byte_feature(encoded_leye),
            'fgrid' : util.byte_feature(encoded_fgrid),
            'dinfo' : util.float_feature(dinfo)
        }

        if flow:
            feature['optflow'] = util.float_feature(optflow)

        example = tf.train.Example(features=tf.train.Features(feature=feature))
        writer.write(example.SerializeToString())
    writer.close()
    return

def parse_subjects(split_name, dataset_name, tfr_dir, subject_dirs, samples_per_file, flow):

    print(f"Parsing subjects for {split_name} set...")

    # Create a separate directory to store the tfrecords of corresponding dataset split
    tfr_fp = os.path.join(tfr_dir, split_name)
    os.makedirs(tfr_fp, exist_ok=True)

    # Calculate vars used to print progress
    nr_subjects = len(subject_dirs)
    subject_width = 1/nr_subjects

    sub_cnt, cnt, file_id, samples = 0, 1, 1, []
    for sd in subject_dirs:

        # Instantiate Subject object to process its frames
        if dataset_name == 'gazecapture':
            subj = subject.GazeCaptureSubject(sd, flow)
        else:
            subj = subject.MPIIGazeSubject(sd)

        # In case subject has no valid frames
        if not subj.valid.any():
            continue

        # For every `samples_per_file` samples, commit to tfrecord
        nr_subject_samples = np.sum(subj.valid)
        sub_sample_cnt = 0
        for sample in subj.subject_iterator():
            progress = ((sub_cnt/nr_subjects) + (sub_sample_cnt/nr_subject_samples)*subject_width)*100
            print(f'\tProgress ({sub_cnt+1}/{nr_subjects}): {progress:.2f}%   \r', end='')
            samples.append(sample)
            if (cnt+1)%samples_per_file == 0:
                write_tfrecord(samples, tfr_fp, split_name, file_id, flow)
                file_id += 1
                samples = []
            # Increment sample count and samples for this subject count
            cnt += 1
            sub_sample_cnt += 1
        # Increment subject count
        sub_cnt += 1

        if len(samples) > 0:
            write_tfrecord(samples, tfr_fp, split_name, file_id, flow)

    # Reset carriage return
    print("")


def main(raw_base_dir, tfr_base_dir, rm, file_size, flow):
    dirs = os.listdir(raw_base_dir)
    if 'raw' not in dirs:
        print(f"Specified base dir ({raw_base_dir}) is missing a `raw` directory.")
        return

    dataset_name = os.path.basename(raw_base_dir)

    if dataset_name == 'gazecapture':
        raw_dir = os.path.join(raw_base_dir, 'raw', ('[0-9]'*5))
    else:
        raw_dir = os.path.join(raw_base_dir, 'raw', 'p*')

    tfr_dir = os.path.join(tfr_base_dir, 'tfr')
    os.makedirs(tfr_dir, exist_ok=True)

    if rm:
        clean_dir(tfr_dir)
        print("Cleaned up base tfrecord directory...")

    subject_dirs = glob.glob(raw_dir)

    # Split subjects into train, val, and test sets
    if dataset_name == 'gazecapture':
        tr, va, te = [], [], []
        for sd in subject_dirs:
            info_fp = os.path.join(sd, 'info.json')
            info = util.get_json(info_fp)
            if info['Dataset'] == 'train':
                tr.append(sd)
            elif info['Dataset'] == 'val':
                va.append(sd)
            else:
                te.append(sd)
    else:
        tr = subject_dirs[:12]
        va = subject_dirs[12:14]
        te = subject_dirs[14:]

    print(f'Total of {len(subject_dirs)} subjects.\n'\
          f'  Train: {len(tr)}\n'\
          f'  Validation: {len(va)}\n'\
          f'  Test: {len(te)}')

    # One data sample has three compressed 224x224x3 images and one 25x25
    # facegrid as inputs and a label with two 32-bits floats.
    file_size = eval(file_size)
    sample_size = 3*(68000) + (150) + 2*4
    samples_per_file = int(file_size/sample_size)

    print(f'File size = {file_size/10**9:.2f} GB')
    print(f'Sample size = {sample_size/10**3:.2f} KB')
    print(f'Samples per file = {samples_per_file}')

    parse_subjects("val", dataset_name, tfr_dir, va, samples_per_file, flow)
    parse_subjects("test", dataset_name, tfr_dir, te, samples_per_file, flow)
    parse_subjects("train", dataset_name, tfr_dir, tr, samples_per_file, flow)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--raw_base_dir', action='store', required=True,
                        help='Base dataset directory where all subject dirs are located')
    parser.add_argument('-o', '--tfr_base_dir', action='store', required=True,
                        help='Base dataset directory where all tfrecords should be stored')
    parser.add_argument('-s', '--file_size', action='store', default='2**30',
                        help='Size of each tfrecord file')
    parser.add_argument('-r', '--rm', action='store_true', required=False,
                        help='Flag indicating wether or not to clean tfrecord directory')
    parser.add_argument('-f', '--flow', action='store_true', required=False,
                        help='If true, the tfrecords generated include optical flow data')

    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    main(**kwargs)
