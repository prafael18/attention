import os
import glob
import argparse

import cv2
import numpy as np
import tensorflow as tf

from util import show_sample

def parse_image(feature_name, example, flags):
    encoded_byte_string = example.features.feature[feature_name].bytes_list.value[0]
    encoded_array = np.fromstring(encoded_byte_string, dtype=np.uint8)
    image = cv2.imdecode(encoded_array, flags)
    if flags == cv2.IMREAD_COLOR:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    return image

def parse_float(feature_name, example):
    return tuple(example.features.feature[feature_name].float_list.value)

def show_tfrecords(base_dir, split, count, show, mean, flow):

    if mean:
        mean_faces = []
        mean_reyes = []
        mean_leyes = []

    tfr_dir = os.path.join(base_dir, 'tfr', split, '*')
    dataset_name = os.path.basename(base_dir)
    tfr_fps = glob.glob(tfr_dir)
    total_nr_records = 0
    for tfp in tfr_fps:
        record_iterator = tf.python_io.tf_record_iterator(path=tfp)
        nr_records = 0
        for string_record in record_iterator:
            if count:
                nr_records += 1
            if show or mean or flow:
                example = tf.train.Example()
                example.ParseFromString(string_record)

                face = parse_image('face', example, cv2.IMREAD_COLOR)
                reye = parse_image('reye', example, cv2.IMREAD_COLOR)
                leye = parse_image('leye', example, cv2.IMREAD_COLOR)
                fgrid = parse_image('fgrid', example, cv2.IMREAD_GRAYSCALE)
                dinfo = parse_float('dinfo', example)
                features = face, reye, leye, fgrid, dinfo
                if flow:
                    optflow = parse_float('optflow', example)
                    features = features + (optflow,)
                    print(f"Optical flow is {optflow}")
                if show:
                    show_sample(features, flow)
                if mean:
                    mean_faces.append(face)
                    mean_reyes.append(reye)
                    mean_leyes.append(leye)

        if count:
            print(f"Total number of records in file {tfp} is {nr_records}")
            total_nr_records += nr_records

    if count:
        print(f"Total number of records in directory {tfr_dir} is {total_nr_records}")
    if mean:
        mean_face = np.mean(np.stack(mean_faces), axis=0)
        mean_reye = np.mean(np.stack(mean_reyes), axis=0)
        mean_leye = np.mean(np.stack(mean_leyes), axis=0)
        np.save(f'mean_face_{dataset_name}_{split}.npy', mean_face)
        np.save(f'mean_reye_{dataset_name}_{split}.npy', mean_reye)
        np.save(f'mean_leye_{dataset_name}_{split}.npy', mean_leye)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--base_dir', action='store', required=True,
                                    help='Base dataset directory')
    parser.add_argument('--train', action='store_const', const='train', dest='split',
                                    help='Flag indicating whether we want to display the training set tfrecords')
    parser.add_argument('--val', action='store_const', const='val', dest='split',
                                    help='Flag indicating whether we want to display the validation set tfrecords')
    parser.add_argument('--test', action='store_const', const='test', dest='split',
                                    help='Flag indicating whether we want to display the test set tfrecords')
    parser.add_argument('--show', action='store_true',
                                    help='If flag is present, uses matplotlib to plot each sample')
    parser.add_argument('--count', action='store_true',
                                    help='Count number of tfrecords per file')
    parser.add_argument('--mean', action='store_true',
                                    help='Compute mean images for entire dataset')
    parser.add_argument('--flow', action='store_true',
                                    help='Read optical flow from tfrecords')
    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    show_tfrecords(**kwargs)
