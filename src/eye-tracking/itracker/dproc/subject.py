# Standard libs
import os
import math
from collections import deque

# Third-party libs
import cv2
import dlib
import scipy.io
import numpy as np

# Local libs
import util

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"(x={self.x}, y={self.y})"

class MPIIGazeSubject:
    # Get shape predictor in order to crop eyes
    face_detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("../assets/shape_predictor_68_face_landmarks.dat")

    def __init__(self, subject_dir):
        self.subject_dir = subject_dir

        # Get screen info
        screen_fn = os.path.join(subject_dir, 'Calibration', 'screenSize.mat')
        screen_mat = scipy.io.loadmat(screen_fn)
        self.screen_scale_h = screen_mat['height_mm']/screen_mat['height_pixel']
        self.screen_scale_w = screen_mat['width_mm']/screen_mat['width_pixel']

        # Collect frame filepaths and ground truth prediction
        self.dinfo = []
        self.frame_fps = []
        id = os.path.basename(subject_dir)
        with open(os.path.join(subject_dir, f'{id}.txt'), 'r') as f:
            for line in f.readlines():
                tokens = line.split(' ')
                self.frame_fps.append(os.path.join(subject_dir, tokens[0]))
                self.dinfo.append((float(tokens[1]), float(tokens[2])))

        # Array with valid frame indices (only used for consistency)
        self.valid = np.ones(len(self.frame_fps))

    def subject_iterator(self):
        for i in range(len(self.frame_fps)):
            frame = cv2.imread(self.frame_fps[i], cv2.IMREAD_COLOR)

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = MPIIGazeSubject.face_detector(gray, 1)

            if not faces:
                print(f'No face found for {self.frame_fps[i]}')
                continue
            else:
                fc = faces[0]

            # Get face crop
            face_pt1 = Point(fc.left(), fc.top())
            face_pt2 = Point(fc.right(), fc.bottom())
            face = frame[face_pt1.y: face_pt2.y, face_pt1.x: face_pt2.x]

            # Get facegrid considering a square frame
            frame_d = max(frame.shape[0], frame.shape[1])
            facegrid = self._get_facegrid(frame.shape[:2], (25, 25), face_pt1, face_pt2)

            kps =  MPIIGazeSubject.predictor(frame, fc)

            reye_pt1, reye_pt2 = self._get_bounding_square(kps, range(36, 42))
            reye = frame[reye_pt1.y: reye_pt2.y, reye_pt1.x: reye_pt2.x]

            leye_pt1, leye_pt2 = self._get_bounding_square(kps, range(42, 48))
            leye = frame[leye_pt1.y: leye_pt2.y, leye_pt1.x: leye_pt2.x]

            if not face.any() or not facegrid.any() or not reye.any() or not leye.any():
                continue

            crops = tuple(map(
                lambda crop: cv2.resize(crop, dsize=(224, 224), interpolation=cv2.INTER_CUBIC),
                (face, reye, leye)
            ))

            dinfo_x = self.dinfo[i][0]*self.screen_scale_w
            dinfo_y = self.dinfo[i][1]*self.screen_scale_h
            dinfo = dinfo_x, dinfo_y

            sample = crops + (facegrid, dinfo)
            yield sample

    def _get_facegrid(self, frame_shape, grid_shape, face_pt1, face_pt2):
        frame_height, frame_width = frame_shape
        grid_height, grid_width = grid_shape

        scale_x = grid_width/frame_width
        scale_y = grid_height/frame_height

        # Compute face params in frame coordinates
        face_x = face_pt1.x
        face_y = face_pt1.y
        face_height = face_pt2.y - face_pt1.y
        face_width = face_pt2.x - face_pt1.x

        # Transform face in frame coordinates to grid coordinates
        grid_w = round(face_width * scale_x)
        grid_h = round(face_height * scale_y)
        grid_size = max(grid_w, grid_h)
        grid_pt1 = Point(round(face_x * scale_x), round(face_y * scale_y))

        facegrid = np.zeros(grid_shape)
        facegrid[grid_pt1.y:grid_pt1.y + grid_size, grid_pt1.x:grid_pt1.x + grid_size] = 1

        return facegrid

    def _get_bounding_square(self, keypoints, indices):
        pt1 = Point(float('inf'), float('inf'))
        pt2 = Point(float('-inf'), float('-inf'))

        for i in indices:
            x, y = keypoints.part(i).x, keypoints.part(i).y

            if x < pt1.x:
                pt1.x = x
            if x > pt2.x:
                pt2.x = x
            if y < pt1.y:
                pt1.y = y
            if y > pt2.y:
                pt2.y = y

        width = pt2.x - pt1.x
        height = pt2.y - pt1.y

        ratio = width/height
        if ratio < 0:
            target_height = height
            target_width = height
        else:
            target_height = width
            target_width = width

        padding_width = math.floor((target_width-width)/2)
        padding_height = math.floor((target_height-height)/2)

        pt1.x -= padding_width
        pt1.y -= padding_height
        pt2.x = pt1.x + target_width
        pt2.y = pt1.y + target_height

        return pt1, pt2

class GazeCaptureSubject:
    def __init__(self, subject_dir, flow):
        self.subject_dir = subject_dir
        self.flow = flow

        get_json = lambda name: util.get_json(os.path.join(subject_dir, name))
        self.face_json  = get_json('appleFace.json')
        self.reye_json  = get_json('appleRightEye.json')
        self.leye_json  = get_json('appleLeftEye.json')
        self.fgrid_json = get_json('faceGrid.json')
        self.frame_json = get_json('frames.json')
        self.dinfo_json = get_json('dotInfo.json')

        self.faces = deque([])
        self.reyes = deque([])
        self.leyes = deque([])
        self.ids = deque([])

        fa = np.array(self.face_json['IsValid'])
        re = np.array(self.reye_json['IsValid'])
        le = np.array(self.leye_json['IsValid'])
        self.valid = (fa == 1) & (re == 1) & (le == 1)

    def subject_iterator(self, flow=False):
        for i in range(len(self.valid)):
            if not self.valid[i]:
                continue
            frame_fp = os.path.join(self.subject_dir, 'frames', self.frame_json[i])
            frame_id = int(self.frame_json[i].split('.')[0])
            frame = cv2.imread(frame_fp, cv2.IMREAD_COLOR)

            face_crop = util.face_crop(frame, self.face_json, i)
            reye_crop = util.eye_crop(frame, self.face_json, self.reye_json, i)
            leye_crop = util.eye_crop(frame, self.face_json, self.leye_json, i)

            if self.flow:
                # Check if there was an abrupt change in the image or orientation, reset the list
                if len(self.faces) > 0:
                    if len(self.faces) == 3:
                        index = 1
                    else:
                        index = 0
                    if self.faces[index].shape != face_crop.shape or \
                       self.reyes[index].shape != reye_crop.shape or \
                       self.leyes[index].shape != leye_crop.shape or \
                       util.cc(self.faces[index], face_crop) < 0.8:
                        self.faces.clear()
                        self.reyes.clear()
                        self.leyes.clear()
                        self.ids.clear()

                # Add new features to queue
                self.faces.append(face_crop)
                self.reyes.append(reye_crop)
                self.leyes.append(leye_crop)
                self.ids.append(frame_id)

                # Keep queue at a maximum size of 3
                if len(self.faces) > 3:
                    self.faces.popleft()
                    self.reyes.popleft()
                    self.leyes.popleft()
                    self.ids.popleft()

                # Only yield when we have 3 samples to compute the optical flow
                if len(self.faces) == 3:

                    # Compute optical flow features
                    dt = np.array([self.ids[-1] - self.ids[0]])
                    face_flow = util.optical_flow(self.faces[0], self.faces[-1])
                    reye_flow = util.optical_flow(self.reyes[0], self.reyes[-1])
                    leye_flow = util.optical_flow(self.leyes[0], self.leyes[-1])

                    if face_flow is not None and reye_flow is not None and leye_flow is not None:
                        optical_flow = np.concatenate([face_flow, reye_flow, leye_flow, dt])
                    else:
                        print("One of the flow computations returned None")
                        continue

                    fgrid = util.get_fgrid(self.fgrid_json, i)
                    dinfo = util.get_dinfo(self.dinfo_json, i)

                    crops = (self.faces[-1], self.reyes[-1], self.leyes[-1])
                    crops = tuple(map(lambda crop: cv2.resize(crop, dsize=(224, 224), interpolation=cv2.INTER_CUBIC), crops))
                    sample = crops + (fgrid, dinfo)
                    yield sample + (optical_flow,)
            else:
                crops = (face_crop, reye_crop, leye_crop)
                crops = tuple(map(lambda crop: cv2.resize(crop, dsize=(224, 224), interpolation=cv2.INTER_CUBIC), crops))
                sample = crops + (fgrid, dinfo)
                util.show_sample(sample)
                yield sample
