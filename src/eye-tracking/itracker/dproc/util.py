import json

import cv2
import scipy.io
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# Define epsilon constant
EPS = np.finfo(np.float32).eps

# Params for ShiTomasi corner detection
feature_params = dict(maxCorners = 10,
                      qualityLevel = 0.1,
                      minDistance = 7,
                      blockSize = 5)

# Parameters for Lucas Kanade optical flow
lk_params = dict(winSize  = (15,15),
                 maxLevel = 3,
                 criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

def show_faces(faces):
    fig, axes = plt.subplots(nrows=1, ncols=len(faces))

    if type(axes) != np.ndarray:
        axes = np.array([axes])

    for i, ax in enumerate(axes):
        ax.imshow(faces[i])
    fig.waitforbuttonpress()
    plt.close(fig)

def optical_flow(old, new, show=False):

    new_gray = cv2.cvtColor(new, cv2.COLOR_BGR2GRAY)
    old_gray = cv2.cvtColor(old, cv2.COLOR_BGR2GRAY)

    p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
    if p0 is not None:
        p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, new_gray, p0, None, **lk_params)
    else:
        return None

    # Keep only good points
    good_new = p1[st==1]
    good_old = p0[st==1]

    # Compute the mean optical flow
    if st.any():
        flow = np.mean(good_new - good_old, axis=0)
    else:
        # No valid points found
        flow = np.array([0, 0])

    if show:
        print(f"Computed optflow as {flow}")
        for i, (old_p, new_p) in enumerate(zip(good_old, good_new)):
            old = cv2.arrowedLine(old, tuple(old_p), tuple(new_p), color=(0, 0, 255), thickness=1)

        fig, axes = plt.subplots(nrows=1, ncols=2)
        axes[0].imshow(old)
        axes[1].imshow(new)
        fig.waitforbuttonpress()
        plt.close(fig)

    return flow

def cc(img1, img2):
    p = img1.astype(np.float32)
    q = img2.astype(np.float32)

    dp = p - np.mean(p)
    dq = q - np.mean(q)

    cc = np.sum((dp)*(dq))/(EPS + np.sqrt(np.sum(dp*dp)*np.sum(dq*dq)))
    return cc

def load_mat(name, dtype=np.float32):
    mat = scipy.io.loadmat(name)
    mat = mat['image_mean']
    mat = mat.astype(dtype)
    return mat

def get_json(json_fp):
    with open(json_fp) as f:
        return json.loads(f.read())

def face_crop(frame, json_obj, i):
    """Returns regular crop of the original frame.
    """
    x, y = int(json_obj['X'][i]), int(json_obj['Y'][i])
    w, h = int(json_obj['W'][i]), int(json_obj['H'][i])
    x, y = max(0, x), max(0, y)
    return frame[y:y+h, x:x+w]

def eye_crop(frame, face_json, eye_json, i):
    """This function returns the eye crop, but we also need the face_json because the eye
    coordinates are given relative to the top-left corner of the face crop.
    """
    fx, fy = int(face_json['X'][i]), int(face_json['Y'][i])
    x, y = int(eye_json['X'][i]), int(eye_json['Y'][i])
    w, h = int(eye_json['W'][i]), int(eye_json['H'][i])
    x += fx
    y += fy
    x, y = max(0, x), max(0, y)
    return frame[y:y+h, x:x+w]

def get_fgrid(json_obj, i):
    x, y = int(json_obj['X'][i]), int(json_obj['Y'][i])
    w, h = int(json_obj['W'][i]), int(json_obj['H'][i])
    fgrid = np.zeros((25, 25))
    fgrid[y:y+h, x:x+w] = 1
    return fgrid

def get_dinfo(json_obj, i):
    return np.array([json_obj['XCam'][i], json_obj['YCam'][i]])

def byte_feature(byte_string):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[byte_string]))

def float_feature(floats):
    return tf.train.Feature(float_list=tf.train.FloatList(value=list(floats)))

def draw_centered_arrow(img, vector):
    # Image center coordinates as (x,y)
    center = img.shape[1]//2, img.shape[0]//2
    offset = int(center[1]+vector[1]), int(center[0]+vector[0])
    img = cv2.arrowedLine(img, center, offset, color=(0, 0, 255), thickness=1)
    return img

def show_sample(features, flow=False):
    features = tuple(map(lambda feat: np.squeeze(feat), features))
    if flow:
        face, reye, leye, fgrid, dinfo, optflow = features
        face = draw_centered_arrow(face, optflow[:2])
        reye = draw_centered_arrow(reye, optflow[2:4])
        leye = draw_centered_arrow(leye, optflow[4:6])
        print(f"Distance between optflow frames is {optflow[6]}")

    else:
        face, reye, leye, fgrid, dinfo = features
    print(f"Dot info: x={dinfo[0]}, y={dinfo[1]}")
    plt.subplot(231)
    plt.imshow(face)
    plt.subplot(232)
    plt.imshow(reye)
    plt.subplot(233)
    plt.imshow(leye)
    plt.subplot(235)
    plt.imshow(fgrid)
    plt.waitforbuttonpress()
