import flask
import camera

app = flask.Flask(__name__)

@app.route('/')
def index():
    """Video streaming home page."""
    return flask.render_template('index.html')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""

    device = 'laptop'
    ipcamera_url = 0
    model_dir = '../runs/mpii-runs/run-1/models/best/007000'
    orientation = 1
    # calibration = {
    #     'points': 13,
    #     'display_time': 1,
    #     'relax_time': 0.5,
    #     'random': False
    # }
    calibration = None

    cam = camera.Camera(
        device=device,
        orientation=orientation,
        model_dir=model_dir,
        calibration=calibration,
        ipcamera_url=ipcamera_url
    )

    return flask.Response(gen(cam),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='192.168.1.109', threaded=True)
