import sys
import math
import queue
import threading

import cv2
import dlib
import numpy as np
import tensorflow as tf

sys.path.insert(0, '../')
from util import load_mat

# Dlib positions
#  ("mouth", [48, 68)),
#	("right_eyebrow", [17, 22)),
#	("left_eyebrow", [22, 27)),
#	("right_eye", [36, 42)),
#	("left_eye", [42, 48)),
#	("nose", [27, 35)),
#	("jaw", [0, 17))

screen_config = {
    'desktop': {
        'screen_H': 1080,
        'screen_W': 2560,
        'device_screen_H_mm': 282.5,
        'device_screen_W_mm': 663.0,
        'device_screen_Y_mm': 41.0,
        'device_screen_X_mm': 363.0
    },
    'laptop': {
        'screen_H': 1080,
        'screen_W': 1920,
        'device_screen_H_mm': 193.5,
        'device_screen_W_mm': 344.0,
        'device_screen_Y_mm': 10.5,
        'device_screen_X_mm': 171.0
    },
    'phone': {
        # 'screen_H': 2560,
        # 'screen_W': 1440,
        'screen_H': 640,
        'screen_W': 360,
        'device_screen_H_mm': 63.2,
        'device_screen_W_mm': 113.0,
        'device_screen_Y_mm': 8.2,
        'device_screen_X_mm': 49.5
    }
}

# Load mean images into memory for data normalization
mean_face = load_mat('../assets/mean_face_224.mat')
mean_reye = load_mat('../assets/mean_right_224.mat')
mean_leye = load_mat('../assets/mean_left_224.mat')

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"(x={self.x}, y={self.y})"


def update(cap, queue, run_event):
    while cap.isOpened() and run_event.is_set():
        ret, frame = cap.read()
        if ret:
            # Remove next-to-last frame before adding latest frame
            if not queue.empty():
                queue.get()
            queue.put(frame)
        else:
            continue
    cap.release()
    print("Video capture object closed")

def cam2screen(pred, device='desktop', orientation=1):
    global screen_config

    # Orientation:
    # 1: portrait
    # 2: portrait, upside down (iPad only)
    # 3: landscape, with home button on the right
    # 4: landscape, with home button on the left

    config = screen_config[device]

    X_curr, Y_curr = pred[0,0]*10, pred[0,1]*10

    screen_H, screen_W = config['screen_H'], config['screen_W']
    dH, dW = config['device_screen_H_mm'], config['device_screen_W_mm']
    dY, dX = config['device_screen_Y_mm'], config['device_screen_X_mm']

    if orientation == 1:
        Y_curr = -Y_curr - dY
        X_curr = X_curr + dX
    elif orientation == 2:
        Y_curr = -Y_curr + dY + dH
        X_curr = X_curr - dX + dW
    elif orientation == 3:
        Y_curr = -Y_curr - dX + dW
        X_curr = X_curr - dY
    elif orientation == 4:
        Y_curr = -Y_curr + dX
        X_curr = X_curr + dY + dH
    else:
        print(f"Specified orientation {orientation} hasn't been implemented")
        raise NotImplementedError

    X_curr = X_curr * (screen_W/dW)
    Y_curr = Y_curr * (screen_H/dH)

    return X_curr, Y_curr

def get_facegrid(frame_shape, grid_shape, face_pt1, face_pt2):
    frame_height, frame_width = frame_shape
    grid_height, grid_width = grid_shape

    scale_x = grid_width/frame_width
    scale_y = grid_height/frame_height

    # Compute face params in frame coordinates
    face_x = face_pt1.x
    face_y = face_pt1.y
    face_height = face_pt2.y - face_pt1.y
    face_width = face_pt2.x - face_pt1.x

    # Transform face in frame coordinates to grid coordinates
    grid_w = round(face_width * scale_x)
    grid_h = round(face_height * scale_y)
    grid_pt1 = Point(round(face_x * scale_x), round(face_y * scale_y))

    facegrid = np.zeros(grid_shape)
    facegrid[grid_pt1.y:grid_pt1.y + grid_h, grid_pt1.x:grid_pt1.x + grid_w] = 1

    return facegrid

def get_bounding_square(keypoints, indices):
    pt1 = Point(float('inf'), float('inf'))
    pt2 = Point(float('-inf'), float('-inf'))

    for i in indices:
        x, y = keypoints.part(i).x, keypoints.part(i).y

        if x < pt1.x:
            pt1.x = x
        if x > pt2.x:
            pt2.x = x
        if y < pt1.y:
            pt1.y = y
        if y > pt2.y:
            pt2.y = y

    width = pt2.x - pt1.x
    height = pt2.y - pt1.y

    ratio = width/height
    if ratio < 0:
        target_height = height
        target_width = height
    else:
        target_height = width
        target_width = width

    padding_width = math.floor((target_width-width)/2)
    padding_height = math.floor((target_height-height)/2)

    pt1.x -= padding_width
    pt1.y -= padding_height
    pt2.x = pt1.x + target_width
    pt2.y = pt1.y + target_height

    return pt1, pt2

def main(sess, device, ipcamera_url=None, show=False):
    global screen_config
    deviceH, deviceW = screen_config[device]['screen_H'], screen_config[device]['screen_W']

    # Save background image
    background = cv2.imread('../assets/background_phone.jpg')
    background = cv2.resize(background, dsize=(deviceW, deviceH))

    # Recover tensors from graph
    g = tf.get_default_graph()
    pred = g.get_tensor_by_name("predictions:0")
    face_t = g.get_tensor_by_name("face:0")
    reye_t = g.get_tensor_by_name("reye:0")
    leye_t = g.get_tensor_by_name("leye:0")
    fgrid_t = g.get_tensor_by_name("fgrid:0")

    if ipcamera_url:
        cap = cv2.VideoCapture(ipcamera_url)
    else:
        cap = cv2.VideoCapture(0)

    cap_buffer = queue.Queue(1)


    # Create an event to signal a keyboard interrupt to threads
    run_event = threading.Event()
    run_event.set()

    t = threading.Thread(target=update, args=(cap, cap_buffer, run_event))
    t.daemon = True
    t.start()

    face_detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("../assets/shape_predictor_68_face_landmarks.dat")

    try:
        while True:
            if not cap_buffer.empty():
                frame = cap_buffer.get()
                if ipcamera_url:
                    print(frame.shape)
                    # frame = cv2.resize(frame, dsize=(0,0), fx=1/3, fy=1/3)
                    frame = np.transpose(frame, axes=(1,0,2))
                    frame = frame[::-1, ...]
                    frame = frame.copy()
            else:
                continue

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # frame = cv2.flip(frame,180)
            frame_copy = frame.copy()

            faces = face_detector(gray, 1)
            for face in faces:

                # Get face crop
                face_pt1 = Point(face.left(), face.top())
                face_pt2 = Point(face.right(), face.bottom())
                face_crop = frame_copy[face_pt1.y: face_pt2.y, face_pt1.x: face_pt2.x]

                # Get facegrid considering a square frame
                frame_d = min(frame.shape[0], frame.shape[1])
                facegrid = get_facegrid((frame.shape[:2]), (25, 25), face_pt1, face_pt2)

                kps =  predictor(frame, face)

                reye_pt1, reye_pt2 = get_bounding_square(kps, range(36, 42))
                reye = frame_copy[reye_pt1.y: reye_pt2.y, reye_pt1.x: reye_pt2.x]

                leye_pt1, leye_pt2 = get_bounding_square(kps, range(42, 48))
                leye = frame_copy[leye_pt1.y: leye_pt2.y, leye_pt1.x: leye_pt2.x]

                if not face_crop.any() or not facegrid.any() or not reye.any() or not leye.any():
                    continue

                crops = tuple(map(
                    lambda crop: cv2.resize(crop, dsize=(224, 224), interpolation=cv2.INTER_CUBIC),
                    (face_crop, reye, leye)
                ))

                face_crop, reye, leye, fgrid = tuple(map(
                    lambda feat: np.expand_dims(feat, axis=0).astype(np.float32),
                    crops + (facegrid,)
                ))

                # BGR -> RGB before subtracting mean
                face_crop = face_crop[..., ::-1]
                reye = reye[..., ::-1]
                leye = leye[..., ::-1]

                face_crop -= mean_face
                reye -= mean_reye
                leye -= mean_leye

                pred_np = sess.run(
                    fetches=pred,
                    feed_dict= {
                        face_t: face_crop,
                        reye_t: reye,
                        leye_t: leye,
                        fgrid_t: fgrid
                    }
                )

                x, y = cam2screen(pred_np, device=device, orientation=1)

                # Clip coordinate values
                x = min(deviceW-1, max(0, x))
                y = min(deviceH-1, max(0, y))

                # bg = np.zeros((1080, 1920, 3))
                bg = background.copy()
                cv2.circle(
                    img=bg,
                    center=(int(x), int(y)),
                    radius=10,
                    color=(0, 0, 255),
                    thickness=-1
                )
                cv2.imshow('Fixation', bg)
                cv2.waitKey(1)

                print(x,y)

                if show:
                    cv2.imshow('Facegrid', facegrid*255)

                    face_crop, reye, leye = crops

                    # Show face crop and draw bounding square on frame
                    cv2.imshow('Face', face_crop)
                    cv2.rectangle(
                        img=frame,
                        pt1=(face.left(), face.top()),
                        pt2=(face.right(), face.bottom()),
                        color=(255, 255, 0),
                        thickness=1
                    )

                    # Show right eye crop and draw bounding square on frame
                    cv2.imshow('Right Eye', reye)
                    cv2.rectangle(
                        img=frame,
                        pt1=(reye_pt1.x, reye_pt1.y),
                        pt2=(reye_pt2.x, reye_pt2.y),
                        color=(0, 0, 255),
                        thickness=1
                    )

                    # Show left eye crop and draw bounding square on frame
                    cv2.imshow('Left Eye', leye)
                    cv2.rectangle(
                        img=frame,
                        pt1=(leye_pt1.x, leye_pt1.y),
                        pt2=(leye_pt2.x, leye_pt2.y),
                        color=(255, 0, 0),
                        thickness=1
                    )

                    cv2.imshow('Frame', frame)
                    cv2.waitKey(30)

    except KeyboardInterrupt:
        cv2.destroyAllWindows()
        print("Attempting to close threads")
        run_event.clear()
        t.join()
        print("Finished closing threads")



if __name__ == "__main__":

    show = True
    device = 'phone'
    ipcamera_url = 'http://192.168.1.128:1234/video'
    model_dir = '../runs/run-10/models/best/083000'

    with tf.Session() as sess:
        # Load SavedModel
        tf.saved_model.load(
            sess=sess,
            tags=[tf.saved_model.tag_constants.SERVING],
            export_dir=model_dir)
        print(f"Successfully loaded model from {model_dir}")

        main(sess, device=device, ipcamera_url=ipcamera_url, show=show)
