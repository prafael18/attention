import cv2
import numpy as np

cap = cv2.VideoCapture('http://192.168.1.129:1234/video')

while True:
    ret, frame = cap.read()
    frame = cv2.resize(frame, dsize=(0,0), fx=1/3, fy=1/3)
    frame = np.transpose(frame, axes=(1,0,2))
    frame = frame[::-1, ...]

    print(frame.shape)
    if ret:
        cv2.imshow("IP Webcam", frame)
        if cv2.waitKey(1) == ord('q'):
            break
