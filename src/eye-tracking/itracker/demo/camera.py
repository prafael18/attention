import time
import math
import queue
import random
import threading

import cv2
import dlib
import scipy.io
import numpy as np
import tensorflow as tf
from sklearn.svm import SVR
from sklearn.multioutput import MultiOutputRegressor

def is_array_in_list(arr, arr_list):
    return next((True for elem in arr_list if arr is elem), False)

def load_mat(name, dtype=np.float32):
    mat = scipy.io.loadmat(name)
    mat = mat['image_mean']
    mat = mat.astype(dtype)
    return mat

# Load mean images into memory for data normalization
# mean_face = np.load('../assets/mean_face_mpiifacegaze_train.npy')
# mean_reye = np.load('../assets/mean_reye_mpiifacegaze_train.npy')
# mean_leye = np.load('../assets/mean_leye_mpiifacegaze_train.npy')

mean_face = load_mat('../assets/mean_face_224.mat')
mean_reye = load_mat('../assets/mean_reye_224.mat')
mean_leye = load_mat('../assets/mean_leye_224.mat')

screen_config = {
    'desktop': {
        'screen_H': 1080,
        'screen_W': 2560,
        'device_screen_H_mm': 282.5,
        'device_screen_W_mm': 663.0,
        'device_screen_Y_mm': 41.0,
        'device_screen_X_mm': 363.0
    },
    'laptop': {
        # 'screen_H': 1080,
        # 'screen_W': 1920,
        'screen_H': 990,
        'screen_W': 1760,
        'device_screen_H_mm': 193.5,
        'device_screen_W_mm': 344.0,
        'device_screen_Y_mm': 10.5,
        'device_screen_X_mm': 171.0
    },
    'phone': {
        # 'screen_H': 2560,
        # 'screen_W': 1440,
        'screen_H': 640,
        'screen_W': 360,
        'device_screen_H_mm': 63.2,
        'device_screen_W_mm': 113.0,
        'device_screen_Y_mm': 8.2,
        'device_screen_X_mm': 49.5
    }
}

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"(x={self.x}, y={self.y})"

class CameraInput(object):
    def __init__(self, cap, run_event):
        self._run_event = run_event
        self._cap = cap
        self._cap_buffer = queue.Queue(1)
        self._thread = threading.Thread(target=self._update)
        self._thread.start()

    def _update(self):
        while self._cap.isOpened() and self._run_event.is_set():
            ret, frame =  self._cap.read()
            if ret:
                # Remove next-to-last frame before adding latest frame
                if not self._cap_buffer.empty():
                    self._cap_buffer.get()
                self._cap_buffer.put(frame)
            else:
                continue
        self._cap.release()
        print("Video capture object for camera input is closed")

    @property
    def thread(self):
        return self._thread

    @property
    def frame(self):
        if not self._cap_buffer.empty():
            return self._cap_buffer.get()

class CameraOutput(object):
    def __init__(self, sess, camera_input, face_detector, predictor, run_event, ipcamera_url):
        self._sess = sess
        self._camera_input = camera_input
        self._face_detector = face_detector
        self._predictor = predictor
        self._run_event = run_event
        self._ipcamera_url = ipcamera_url

        # Recover tensors from graph
        g = tf.get_default_graph()
        self._feat_vec = g.get_tensor_by_name("all/fc1/Relu:0")
        self._pred = g.get_tensor_by_name("predictions:0")
        self._face_t = g.get_tensor_by_name("face:0")
        self._reye_t = g.get_tensor_by_name("reye:0")
        self._leye_t = g.get_tensor_by_name("leye:0")
        self._fgrid_t = g.get_tensor_by_name("fgrid:0")

        self._face = None
        self._reye = None
        self._leye = None
        self._facegrid = None
        self._frame = None

        # Value of feature vec is None while we don't have any predictions
        self._feat_vec_np = None
        self._pred_coords = np.array([[0,0]])

        self._thread = threading.Thread(target=self._update)
        self._thread.start()

    def _update(self):
        while self._run_event.is_set():
            # Get latest input frame
            frame = self._camera_input.frame

            # If cap buffer is empty, continue...
            if frame is None:
                continue

            # In case we're receiving feed from ip camera, pre-process frame
            if self._ipcamera_url:
                frame = np.transpose(frame, axes=(1,0,2))
                frame = frame[::-1, ...]
                frame = frame.copy()

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = self._face_detector(gray, 1)

            for fc in faces:
                # Get face crop
                face_pt1 = Point(fc.left(), fc.top())
                face_pt2 = Point(fc.right(), fc.bottom())
                face = frame[face_pt1.y: face_pt2.y, face_pt1.x: face_pt2.x]

                # Get facegrid considering a square frame
                frame_d = min(frame.shape[0], frame.shape[1])
                facegrid = self._get_facegrid((frame.shape[:2]), (25, 25), face_pt1, face_pt2)

                kps =  self._predictor(frame, fc)

                reye_pt1, reye_pt2 = self._get_bounding_square(kps, range(36, 42))
                reye = frame[reye_pt1.y: reye_pt2.y, reye_pt1.x: reye_pt2.x]

                leye_pt1, leye_pt2 = self._get_bounding_square(kps, range(42, 48))
                leye = frame[leye_pt1.y: leye_pt2.y, leye_pt1.x: leye_pt2.x]

                if not face.any() or not facegrid.any() or not reye.any() or not leye.any():
                    continue

                crops = tuple(map(
                    lambda crop: cv2.resize(crop, dsize=(224, 224), interpolation=cv2.INTER_CUBIC),
                    (face, reye, leye)
                ))

                # Update crops for visualization in parent object
                self._face = crops[0]
                self._reye = crops[1]
                self._leye = crops[2]
                self._facegrid = facegrid
                self._frame = frame

                face, reye, leye, fgrid = tuple(map(
                    lambda feat: np.expand_dims(feat, axis=0).astype(np.float32),
                    crops + (facegrid,)
                ))

                # BGR -> RGB before subtracting mean
                face = face[..., ::-1]
                reye = reye[..., ::-1]
                leye = leye[..., ::-1]

                face -= mean_face
                reye -= mean_reye
                leye -= mean_leye

                pred_np, feat_vec_np = self._sess.run(
                    fetches=[self._pred, self._feat_vec],
                    feed_dict = {
                        self._face_t: face,
                        self._reye_t: reye,
                        self._leye_t: leye,
                        self._fgrid_t: fgrid
                    }
                )

                self._feat_vec_np = feat_vec_np
                self._pred_coords = pred_np

    @property
    def feat_vec(self):
        return self._feat_vec_np

    @property
    def face(self):
        return self._face

    @property
    def reye(self):
        return self._reye

    @property
    def leye(self):
        return self._leye

    @property
    def facegrid(self):
        return self._facegrid

    @property
    def frame(self):
        return self._frame

    @property
    def thread(self):
        return self._thread

    @property
    def pred_coords(self):
        return self._pred_coords

    def _get_bounding_square(self, keypoints, indices):
        pt1 = Point(float('inf'), float('inf'))
        pt2 = Point(float('-inf'), float('-inf'))

        for i in indices:
            x, y = keypoints.part(i).x, keypoints.part(i).y

            if x < pt1.x:
                pt1.x = x
            if x > pt2.x:
                pt2.x = x
            if y < pt1.y:
                pt1.y = y
            if y > pt2.y:
                pt2.y = y

        width = pt2.x - pt1.x
        height = pt2.y - pt1.y

        ratio = width/height
        if ratio < 0:
            target_height = height
            target_width = height
        else:
            target_height = width
            target_width = width

        padding_width = math.floor((target_width-width)/2)
        padding_height = math.floor((target_height-height)/2)

        pt1.x -= padding_width
        pt1.y -= padding_height
        pt2.x = pt1.x + target_width
        pt2.y = pt1.y + target_height

        return pt1, pt2

    def _get_facegrid(self, frame_shape, grid_shape, face_pt1, face_pt2):
        frame_height, frame_width = frame_shape
        grid_height, grid_width = grid_shape

        scale_x = grid_width/frame_width
        scale_y = grid_height/frame_height

        # Compute face params in frame coordinates
        face_x = face_pt1.x
        face_y = face_pt1.y
        face_height = face_pt2.y - face_pt1.y
        face_width = face_pt2.x - face_pt1.x

        # Transform face in frame coordinates to grid coordinates
        grid_w = round(face_width * scale_x)
        grid_h = round(face_height * scale_y)
        grid_pt1 = Point(round(face_x * scale_x), round(face_y * scale_y))

        facegrid = np.zeros(grid_shape)
        facegrid[grid_pt1.y:grid_pt1.y + grid_h, grid_pt1.x:grid_pt1.x + grid_w] = 1

        return facegrid

class Camera(object):
    def __init__(self, device, orientation, model_dir, calibration=None, ipcamera_url=None, show=None):
        global screen_config

        self._sess = tf.Session()
        self._face_detector = dlib.get_frontal_face_detector()
        self._predictor = dlib.shape_predictor("../assets/shape_predictor_68_face_landmarks.dat")
        self._device = device
        self._orientation = orientation
        self._model_dir = model_dir
        self._calibration = calibration
        self._run_event = threading.Event()
        self._run_event.set()

        sc = screen_config[device]
        self._deviceH = sc['screen_H']
        self._deviceW = sc['screen_W']
        self._dH, self._dW = sc['device_screen_H_mm'], sc['device_screen_W_mm']
        self._dY, self._dX = sc['device_screen_Y_mm'], sc['device_screen_X_mm']

        tf.saved_model.load(
            sess=self._sess,
            tags=[tf.saved_model.tag_constants.SERVING],
            export_dir=model_dir)
        print(f"Successfully loaded model from {model_dir}")

        if ipcamera_url:
            self._cap = cv2.VideoCapture(ipcamera_url)
        else:
            self._cap = cv2.VideoCapture(0)

        self._camera_input = CameraInput(
            cap=self._cap,
            run_event=self._run_event
        )
        self._camera_output = CameraOutput(
            sess=self._sess,
            camera_input=self._camera_input,
            face_detector=self._face_detector,
            predictor=self._predictor,
            run_event=self._run_event,
            ipcamera_url=ipcamera_url
        )


        # Only read background image once
        self._background = cv2.imread('../assets/background.jpg')
        self._background = cv2.resize(self._background, dsize=(self._deviceW, self._deviceH))

        # By default, we don't return calibration frames
        if calibration is None:
            self._is_calibrating = False
        else:
            self._is_calibrating = True
            self._calibration_frame = np.zeros((self._deviceH, self._deviceW, 3), dtype=np.uint8)
            self._calibration_thread = threading.Thread(target=self._update)
            self._calibration_thread.start()
            self._classifier = None

    def _update(self):
        mean = False
        print("Starting calibration routine")

        self._is_calibrating = True
        svm_data = {}

        zeros = np.zeros((self._deviceH, self._deviceW, 3), dtype=np.uint8)
        self._calibration_frame = zeros

        # Give the application time to start up    mat = mat.astype(dtype)

        start = time.time()
        while time.time() - start < 4:
            continue

        points = []
        if self._calibration['random']:
            for _ in range(self._calibration['points']):
                x = np.random.randint(0, self._deviceW)
                y = np.random.randint(0, self._deviceH)
                points.append((x,y))
        else:
            # If calibration isn't random, ignore number of points and calibrate
            # for the entire screen
            points = []
            xs = np.linspace(50, self._deviceW-50, 5)
            ys = np.linspace(50, self._deviceH-50, 5)
            for i in range(0, 25, 2):
                p = int(xs[i//5]), int(ys[i%5])
                points.append(p)
            # random.shuffle(points)

        xs, ys = [], []

        for x, y in points:
            calibration_frame = zeros.copy()
            cv2.circle(
                img=calibration_frame,
                center=(x, y),
                radius=5,
                color=(0, 0, 255),
                thickness=-1
            )

            self._calibration_frame = calibration_frame
            start = time.time()
            feat_vecs = {}
            time_thresh  = 0.6*self._calibration['display_time']
            while time.time() - start < self._calibration['display_time']:
                elapsed_time = time.time() - start
                if elapsed_time > time_thresh:
                    cur_feat_vec = self._camera_output.feat_vec
                    if cur_feat_vec is None:
                        continue
                    if id(cur_feat_vec) not in feat_vecs:
                        feat_vecs[id(cur_feat_vec)] = cur_feat_vec

            print(f'Feat vec length for ({x}, {y}):', len(feat_vecs))

            feat_vecs = list(map(np.squeeze, feat_vecs.values()))

            if mean:
                ys.append((x,y))
                xs.append(np.mean(feat_vecs, axis=0))
            else:
                ys.extend([(x,y)]*len(feat_vecs))
                xs.extend(feat_vecs)

            self._calibration_frame = zeros
            start = time.time()
            while time.time() - start < self._calibration['relax_time']:
                continue

        print("Finished gathering calibration points.")
        x_tr = np.stack(xs)
        y_tr = np.stack(ys)
        print(x_tr.shape)
        print(y_tr.shape)

        # self._classifier = MultiOutputRegressor(SVR(kernel='linear'))
        # self._classifier = MultiOutputRegressor(SVR(kernel='linear', C=0.0215, epsilon=0.464))
        self._classifier = MultiOutputRegressor(SVR(kernel='rbf', gamma='scale', C=1e3))
        self._classifier.fit(x_tr, y_tr)

        print(y_tr)
        print(self._classifier.predict(x_tr))

        print("Finished training SVR classifier")
        self._is_calibrating = False

    @property
    def camera_output(self):
        return self._camera_output

    def get_frame(self):
        # This function must return the latest frame
        if self._is_calibrating:
            next_frame =  self._calibration_frame
        else:
            if self._calibration is None:
                coords = self._camera_output.pred_coords
                x, y = self._cam2screen(coords)
            else:
                coords = self._classifier.predict(self._camera_output.feat_vec)
                x, y = (coords[0,0], coords[0,1])
            print(x, y)

            # Clip coordinate values
            x = int(min(self._deviceW-1, max(0, x)))
            y = int(min(self._deviceH-1, max(0, y)))
            coords = (x, y)

            next_frame = self._background.copy()
            cv2.circle(
                img=next_frame,
                center=coords,
                radius=10,
                color=(0, 0, 255),
                thickness=-1
            )

        # return next_frame
        # enc_frame = cv2.imencode('.jpg', next_frame)[1].tobytes()
        return next_frame

    def _cam2screen(self, pred):
        # Orientation:
        # 1: portrait
        # 2: portrait, upside down (iPad only)
        # 3: landscape, with home button on the right
        # 4: landscape, with home button on the left

        X_curr, Y_curr = pred[0,0]*10, pred[0,1]*10

        # if self._orientation == 1:
        #     Y_curr = -Y_curr - self._dY
        #     X_curr = X_curr + self._dX
        # elif self._orientation == 2:
        #     Y_curr = -Y_curr + self._dY + self._dH
        #     X_curr = X_curr - self._dX + self._dW
        # elif self._orientation == 3:
        #     Y_curr = -Y_curr - self._dX + self._dW
        #     X_curr = X_curr - self._dY
        # elif self._orientation == 4:
        #     Y_curr = -Y_curr + self._dX
        #     X_curr = X_curr + self._dY + self._dH
        # else:
        #     print(f"Specified orientation {orientation} hasn't been implemented")
        #     raise NotImplementedError

        X_curr = X_curr * (self._deviceW/self._dW)
        Y_curr = Y_curr * (self._deviceH/self._dH)

        return X_curr, Y_curr

    def close(self):
        print("Entering camera object destructor")

        # Close tensorflow session
        self._sess.close()
        print("Closed tf session")

        # Terminate all child threads
        self._run_event.clear()
        self._camera_output.thread.join()
        self._camera_input.thread.join()
        print("Threads terminated")

        # Destory all OpenCV windows
        cv2.destroyAllWindows()
        print("Closed all OpenCV windows")


if __name__ == "__main__":
    show = True
    device = 'laptop'
    orientation = 1
    ipcamera_url = 0
    # model_dir = '../runs/mpii-runs/run-1/models/best/007000'
    model_dir = '../runs/run-9/models/best/083000'
    calibration = {
        'display_time': 1,
        'relax_time': 0.5,
        'random': False
    }
    # calibration = None
    camera = Camera(device, orientation, model_dir, calibration=calibration, ipcamera_url=ipcamera_url)

    try:
        while True:
            bg = camera.get_frame()
            frame = camera.camera_output.frame
            face = camera.camera_output.face
            reye = camera.camera_output.reye
            leye = camera.camera_output.leye
            facegrid = camera.camera_output.facegrid

            if bg is not None and frame is not None:
                cv2.imshow("Background", bg)
                cv2.imshow('Facegrid', facegrid*255)

                # Show face crop and draw bounding square on frame
                cv2.imshow('Face', face)
                # cv2.rectangle(
                #     img=frame,
                #     pt1=(face.left(), face.top()),
                #     pt2=(face.right(), face.bottom()),
                #     color=(255, 255, 0),
                #     thickness=1
                # )

                # Show right eye crop and draw bounding square on frame
                cv2.imshow('Right Eye', reye)
                # cv2.rectangle(
                #     img=frame,
                #     pt1=(reye_pt1.x, reye_pt1.y),
                #     pt2=(reye_pt2.x, reye_pt2.y),
                #     color=(0, 0, 255),
                #     thickness=1
                # )

                # Show left eye crop and draw bounding square on frame
                cv2.imshow('Left Eye', leye)
                # cv2.rectangle(
                #     img=frame,
                #     pt1=(leye_pt1.x, leye_pt1.y),
                #     pt2=(leye_pt2.x, leye_pt2.y),
                #     color=(255, 0, 0),
                #     thickness=1
                # )

                cv2.imshow('Frame', frame)
                cv2.waitKey(30)

    except KeyboardInterrupt:
        camera.close()
