import math
import random

import cv2
import numpy as np
import tensorflow as tf

from util import load_mat, save_dict, read_dict

augm_params = {}
augm_params_key = None

# Load mean images into memory for data normalization
mean_face = load_mat('assets/mean_face_224.mat')
mean_reye = load_mat('assets/mean_right_224.mat')
mean_leye = load_mat('assets/mean_left_224.mat')

def salt_and_pepper(features, random_fn=None, sp=None, pp=None, load=None):

    def tf_salt_and_pepper(img, sp, pp):
        sp_mask = tf.random.uniform(shape=img.shape[:-1]) < sp
        sp_mask = tf.broadcast_to(tf.expand_dims(sp_mask, axis=2), img.shape)
        img = tf.where(
            condition=sp_mask,
            x=tf.ones_like(img)*255,
            y=img
        )
        pp_mask = tf.random.uniform(shape=img.shape[:-1]) < pp
        pp_mask = tf.broadcast_to(tf.expand_dims(pp_mask, axis=2), img.shape)
        img = tf.where(
            condition=pp_mask,
            x=tf.zeros_like(img),
            y=img
        )
        return img

    if load:
        sp = augm_params[augm_params_key]['salt_and_pepper']['sp']
        pp = augm_params[augm_params_key]['salt_and_pepper']['pp']
    else:
        sp = random_fn(*sp)
        pp = random_fn(*pp)
        augm_params[augm_params_key]['salt_and_pepper'] = {'sp': sp, 'pp': pp}

    face, reye, leye = tuple(map(
        lambda feat: tf_salt_and_pepper(feat, sp, pp),
        features[:3]
    ))
    return face, reye, leye, features[3]


def gaussian_noise(features, random_fn=None, mean=None, sigma=None, load=None):
    if load:
        mean = augm_params[augm_params_key]['gaussian_noise']['mean']
        sigma = augm_params[augm_params_key]['gaussian_noise']['sigma']
    else:
        mean = random_fn(*mean)
        sigma = random_fn(*sigma)
        augm_params[augm_params_key]['gaussian_noise'] = {'mean': mean, 'sigma': sigma}

    face, reye, leye = tuple(map(
        lambda feat: feat + tf.random.normal(feat.shape, mean=mean, stddev=sigma),
        features[:3]
    ))
    return face, reye, leye, features[3]

def blur(features, random_fn=None, ksize=None, sigma=None, load=None):
    # Define blur function using opencv methods
    def cv_blur(img, ksize, sigma):
        kernel = cv2.getGaussianKernel(ksize=ksize, sigma=sigma)
        blur_img = cv2.sepFilter2D(img, ddepth=-1, kernelX=kernel, kernelY=kernel)
        return blur_img

    if load:
        ksize = int(augm_params[augm_params_key]['blur']['ksize'])
        sigma = augm_params[augm_params_key]['blur']['sigma']
    else:
        ksize = random_fn(*ksize)
        sigma = random_fn(*sigma)

        # Cast kernel size to nearest odd integer (only works for non tf random_fn)
        ksize = (math.floor(ksize) // 2) * 2 + 1

        augm_params[augm_params_key]['blur'] = {'ksize': ksize, 'sigma': sigma}

    ksize = tf.constant(ksize, dtype=tf.uint8)
    sigma = tf.constant(sigma, dtype=tf.float32)

    face, reye, leye = tuple(map(
        lambda feat: tf.py_func(func=cv_blur, inp=[feat, ksize, sigma], Tout=tf.float32),
        features[:3]
    ))
    return face, reye, leye, features[3]

def flip(features, load=None):
    if not load:
        augm_params[augm_params_key]['flip'] = {}
    face, reye, leye, fgrid =  tuple(map(
        lambda feat: tf.image.flip_left_right(feat),
        features
    ))
    return face, leye, reye, fgrid

def adjust_brightness(features, random_fn=None, delta=None, load=None):
    if load:
        delta = augm_params[augm_params_key]['brightness']['delta']
    else:
        delta = random_fn(*delta)
        augm_params[augm_params_key]['brightness'] = {'delta': delta}
    face, reye, leye = tuple(map(
        lambda feat: tf.clip_by_value(tf.math.add(feat, delta), 0, 255),
        features[:3]
    ))
    return face, reye, leye, features[3]

def adjust_contrast(features, random_fn=None, contrast=None, load=None):
    if load:
        contrast = augm_params[augm_params_key]['contrast']['contrast']
    else:
        contrast = random_fn(*contrast)
        augm_params[augm_params_key]['contrast'] = {'contrast': contrast}
    face, reye, leye = tuple(map(
        lambda feat: tf.image.adjust_contrast(feat, contrast),
        features[:3]
    ))
    return face, reye, leye, features[3]

def adjust_hue(features, random_fn=None, delta=None, load=None):
    if load:
        delta = augm_params[augm_params_key]['hue']['delta']
    else:
        delta = random_fn(*delta)
        augm_params[augm_params_key]['hue'] = {'delta': delta}
    face, reye, leye = tuple(map(
        lambda feat: tf.image.adjust_hue(feat, delta),
        features[:3]
    ))
    return face, reye, leye, features[3]

def adjust_saturation(features, random_fn=None, saturation_factor=None, load=None):
    if load:
        saturation_factor = augm_params[augm_params_key]['saturation']['saturation_factor']
    else:
        saturation_factor = random_fn(*saturation_factor)
        augm_params[augm_params_key]['saturation'] = {'saturation_factor': saturation_factor}
    face, reye, leye = tuple(map(
        lambda feat: tf.image.adjust_saturation(feat, saturation_factor),
        features[:3]
    ))
    return face, reye, leye, features[3]

def rotate(features, random_fn=None, angle=None, load=None):
    if load:
        angle = augm_params[augm_params_key]['rotation']['angle']
    else:
        angle = random_fn(*angle)
        augm_params[augm_params_key]['rotation'] = {'angle': angle}
    return tuple(map(
        lambda feat: tf.contrib.image.rotate(feat, angle),
        features
    ))

def subtract_mean_fn(features, labels, flow=False):
    # Unpack features
    if flow:
        face, reye, leye, fgrid, optflow = features
    else:
        face, reye, leye, fgrid = features

    # Subtract mean images
    face -= mean_face
    reye -= mean_reye
    leye -= mean_leye

    if flow:
        return (face, reye, leye, fgrid, optflow), labels
    return (face, reye, leye, fgrid), labels

def aug_fn(example, augmentation_dict, load=None, flow=False):
    global augm_params, augm_params_key

    # Unpack example
    if flow:
        (face, reye, leye, fgrid, optflow), dinfo = example
    else:
        (face, reye, leye, fgrid), dinfo = example

    # Make fgrid three-dimensional
    fgrid = tf.reshape(fgrid, (25, 25, 1))
    feats = face, reye, leye, fgrid

    # We don't modify the label or optical flow despite transformations
    aug_dinfo = tf.broadcast_to(dinfo, (len(augmentation_dict) + 1, 2))
    if flow:
        aug_optflow = tf.broadcast_to(optflow, (len(augmentation_dict) + 1, 7))

    # Create a list of lists for each feature with the original sample in it
    feats_lists = list(map(lambda f: [f], (face, reye, leye, fgrid)))

    # Append augmented features to individual feature lists
    for k in augmentation_dict.keys():
        augm_params_key = k
        if not load:
            augm_params[k] = {}
        aug_feats = feats
        for fn, kwargs in augmentation_dict[k]:
            if load:
                kwargs['load'] = True
            aug_feats = fn(aug_feats, **kwargs)
        for i in range(len(feats_lists)):
            feats_lists[i].append(aug_feats[i])

    # Stack augmented frames along axis 0 to create a `mini-batch` of augmented data
    face, reye, leye, fgrid = tuple(map(
        lambda feat_l: tf.stack(feat_l, axis=0),
        feats_lists
    ))

    fgrid = tf.squeeze(fgrid)

    if flow:
        return (face, reye, leye, fgrid, aug_optflow), aug_dinfo
    return (face, reye, leye, fgrid), aug_dinfo

# Helper function to parse tfrecord sample
def parse_fn(example_proto, flow=False):
    global mean_face, mean_reye, mean_leye

    feature_description = {
        'face'  : tf.FixedLenFeature([], tf.string, default_value=''),
        'reye'  : tf.FixedLenFeature([], tf.string, default_value=''),
        'leye'  : tf.FixedLenFeature([], tf.string, default_value=''),
        'fgrid' : tf.FixedLenFeature([], tf.string, default_value=''),
        'dinfo' : tf.FixedLenFeature([2], tf.float32, default_value=[0.0, 0.0])
    }

    if flow:
        feature_description['optflow'] = tf.FixedLenFeature([7], tf.float32, default_value=[0.0]*7)

    features_dict = tf.parse_single_example(example_proto, feature_description)

    # Ensure features are in cm (if using mpiifacegaze, we should divide by 10)
    labels = features_dict['dinfo']

    # Decode uint8 png arrays to original image
    face, reye, leye, fgrid = tuple(map(
        lambda feat_id: tf.image.decode_png(features_dict[feat_id], channels=0, dtype=tf.uint8),
        ('face', 'reye', 'leye', 'fgrid')
    ))

    # Set tensor shapes
    fgrid = tf.reshape(fgrid, (25, 25))
    face.set_shape((224, 224, 3))
    reye.set_shape((224, 224, 3))
    leye.set_shape((224, 224, 3))


    # Cast tensors to floats
    features = tuple(map(
        lambda feat: tf.cast(feat, tf.float32),
        (face, reye, leye, fgrid)
    ))

    # Append optical flow to feature tuple
    if flow:
        features = features + (features_dict['optflow'],)

    return features, labels

def get_dataset(filenames, batch_size, train=False, augmentation_factor=1, seed=None, save_fn=None, load_fn=None, flow=False):
    global augm_params

    # If we are testing, we need to sample from one file at a time to keep samples in order
    if train:
        nr_files = len(filenames)
    else:
        nr_files = 1
    block_length = max(1, int(batch_size/nr_files))

    # Use interleave and prefetch to read many files concurrently. We use nr_files buffer size to
    # guarantee uniformity when shuffling filenames. The cycle_length guarantees we're sampling from
    # all files simultaneously and `block_length` tells us how many samples to get from
    # each file before cycling again through files
    filenames = tf.data.Dataset.from_tensor_slices(filenames)
    if train:
        filenames = filenames.shuffle(buffer_size=nr_files)
    dataset = filenames.interleave(lambda f: tf.data.TFRecordDataset(f).prefetch(10*block_length),
                               cycle_length=nr_files,
                               block_length=block_length)
                               # cycle_length=1)

    # Parallelize map with 4 threads (approx. number of cpu cores). Shuffle buffer
    # size doesn't have to be too big since we already sample from randomly shuffled files
    dataset = dataset.map(
        lambda exproto: parse_fn(exproto, flow=flow),
        num_parallel_calls=4
    )

    if augmentation_factor > 1:
        # If seed is provided, the intended behavior is to apply a random list of transformations
        # that is fixed at graph creation time and randomly alter the transformation's parameters
        # for every augmentation (currently, there is a bug where different augmentations use the same
        # parameter despite different random tensors with different seeds being used).
        # In case seed isn't provided, we randomly choose a list of transformations with their respective
        # parameters at graph creation time and use these for every sample throughout training
        # In both cases, we want the data pipeline to be easily reproducible, meaning that given the same
        # set of parameters we can get the same augmentations in the same order

        # If we want to dynamically change transformation parameters, use tf.random,
        # otherwise set them only when creating graph and using python's random module
        if seed:
            random.seed(seed)
            tf.random.set_random_seed(seed)
            random_fn = lambda min, max: tf.random.uniform([], min, max)
        else:
            random_fn = lambda min, max: random.uniform(min, max)

        # List of transformation functions with respective paramters' min and max values
        transformations = {
            'brightness': {'fn': adjust_brightness, 'kwargs': {'delta': (10, 100)}},
            'contrast': {'fn': adjust_contrast, 'kwargs': {'contrast': (0.5, 1.0)}},
            'hue': {'fn': adjust_hue, 'kwargs': {'delta': (0.1, 0.35)}},
            'saturation': {'fn': adjust_saturation, 'kwargs': {'saturation_factor': (0.25, 1.0)}},
            'blur': {'fn': blur, 'kwargs': {'ksize': (11, 13), 'sigma': (4, 15)}},
            'gaussian_noise': {'fn': gaussian_noise, 'kwargs': {'mean': (10, 15), 'sigma': (15, 25)}},
            # 'salt_and_pepper': {'fn': salt_and_pepper, 'kwargs': {'sp': (0, 0.5), 'pp': (0, 0.5)}},
            # 'flip': {'fn': flip, 'kwargs': {}},
            # 'rotation': {'fn': rotate, 'kwargs': {'angle': (-0.1, 0.1)}},
        }

        # Let's call an augmentation a list of tranformations with their respecitve parameters.
        # We want to randomly define `augmentation_factor` augmentations for each sample
        aug_dict = {}
        if load_fn:
            augm_params = read_dict(load_fn)
            for i in augm_params.keys():
                aug_dict[i] = []
                for fn_name in augm_params[i].keys():
                    fn = transformations[fn_name]['fn']
                    aug_dict[i].append((fn, {}))
        else:
            transf_keys = list(transformations.keys())
            i = 0
            while i < augmentation_factor-1:
                k = transf_keys[i%len(transf_keys)]
                transf = transformations[k]
                fn = transf['fn']
                kwargs = {}
                for p in transf['kwargs'].keys():
                    kwargs[p] = transf['kwargs'][p]
                    kwargs['random_fn'] = random_fn
                aug_dict[i] = [(fn, kwargs)]
                i += 1

        # Apply `augmentation_factor` different sets of transformations to each sample
        # and return it as a batch
        dataset = dataset.map(
            lambda x, y: aug_fn((x,y), aug_dict, load_fn),
            num_parallel_calls=4
        )

        # Unbatch the augmented features so that they can be randomly sampled with shuffle
        dataset = dataset.apply(tf.data.experimental.unbatch())

        # Save augmentation params for future reference
        if save_fn:
            save_dict(augm_params, save_fn)

    # Subtract mean from all images after applying transformations
    dataset = dataset.map(
        lambda x, y: subtract_mean_fn(x, y, flow),
        num_parallel_calls=4
    )

    if train:
        dataset = dataset.shuffle(buffer_size=5*batch_size)

    dataset = dataset.batch(batch_size=batch_size)

    # Use prefetch to have at least one batch to overlap producer and consumer
    dataset = dataset.prefetch(10)

    return dataset
