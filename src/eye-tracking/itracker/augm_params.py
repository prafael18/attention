import os
import pdb
import math
import subprocess

import matplotlib
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from util import save_dict

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.logging.set_verbosity(tf.logging.FATAL)

# Remove tensorflow debugging info from stdout
input_base_dir = '/mnt/shared-storage/datasets/gazecapture_alt'
# input_base_dir = '/neuron2/disk1/panda/datasets/gazecapture_alt'
out_dir = 'augm_plots'
tmp_fn = 'tmp.json'
nr_tests = 100
og_error = None

transformations = {
    'flip': {'fn': None, 'kwargs': {}},
    'brightness': {'fn': None, 'kwargs': {'delta': (10, 100)}},
    'contrast': {'fn': None, 'kwargs': {'contrast': (0.5, 1.0)}},
    'hue': {'fn': None, 'kwargs': {'delta': (0.1, 0.35)}},
    'saturation': {'fn': None, 'kwargs': {'saturation_factor': (0.25, 1.0)}},
    'blur': {'fn':None, 'kwargs': {'ksize': (11, 13), 'sigma': (4, 15)}},
    'gaussian_noise': {'fn':None, 'kwargs': {'mean': (10, 15), 'sigma': (15, 25)}},
    'salt_and_pepper': {'fn':None, 'kwargs': {'sp': (0, 0.5), 'pp': (0, 0.5)}},
    'rotation': {'fn': None, 'kwargs': {'angle': (-1.04, 1.04)}},
}

def plot_error(param_values, error_list, tf_name, param_names=['param']):
    assert len(param_values) == len(error_list)

    fig = plt.figure(figsize=(10,10))

    if len(param_names) == 1:
        ax = fig.add_subplot(111)
        ax.scatter(x=param_values, y=error_list, label=f'{param_names[0]}')
        ax.set_xlim(np.min(param_values)-0.1, np.max(param_values)+0.1)
        ax.set_ylim(min(np.min(error_list), og_error)-0.1, max(np.max(error_list), og_error)+0.1)
        xticks = ax.get_xticks()
        ax.plot(xticks, [og_error]*len(xticks), color='r', label='null')
        ax.set_title(f'Errors for {tf_name}')
        ax.set_ylabel('Error')
        ax.set_xlabel(f'{param_names[0]}')
        ax.legend()
    elif len(param_names) == 2:
        ax = Axes3D(fig)
        xs, ys = zip(*param_values)

        # Transform X, Y and Z to 2d arrays
        X = np.unique(xs)
        Y = np.unique(ys)
        X, Y = np.meshgrid(X, Y, indexing='ij')
        Z = np.reshape(error_list, X.shape)

        # Make surface plot with errors and add colorbar
        surf = ax.plot_surface(X=X, Y=Y, Z=Z, cmap=matplotlib.cm.coolwarm)
        fig.colorbar(surf, shrink=0.3, aspect=10)

        # Set axes limits
        ax.set_xlim(np.min(xs), np.max(xs))
        ax.set_ylim(np.min(ys), np.max(ys))
        ax.set_zlim(min(np.min(error_list), og_error), max(np.max(error_list), og_error))

        # Plot error with no transformations as a wireframe
        X = ax.get_xticks()
        Y = ax.get_yticks()
        X, Y = np.meshgrid(X, Y)
        Z = np.broadcast_to(og_error, X.shape)
        ax.plot_wireframe(X=X, Y=Y, Z=Z, color=(0.3, 0.3, 1.0))

        ax.set_title(f'Errors for {tf_name}')
        ax.set_zlabel('Error')
        ax.set_xlabel(f'{param_names[0]}')
        ax.set_ylabel(f'{param_names[1]}')
    else:
        raise NotImplementedError

    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    fig.savefig(os.path.join(out_dir, f'{tf_name}.png'))

def compute_error(input_base_dir, augm_factor, tf_dict=None, tmp_fn=None):
    if tf_dict:
        save_dict(tf_dict, tmp_fn)
    args = [
        "python3", "infer.py",
        "-i", input_base_dir,
        "-o", "test",
        "--run", "9",
        "--split", "val",
        "--augm_factor", str(augm_factor),
        "--best",
        "--tfr",
    ]
    if tmp_fn:
        args += ["--load", tmp_fn]
    cp = subprocess.run(
        args,
        stdout=subprocess.PIPE
    )
    error_str = cp.stdout.decode("utf-8").strip().split("\n")[-1]
    error = float(error_str.split(':')[-1])
    return error

def params2args(params):
    # Params is a list of lists and we want to return a list of tuples with all
    # possible combinations of params
    nr_params = len(params)

    # Create string for the arg tuple
    args_str = '[('
    for i in range(nr_params):
        args_str += f'x{i},'
    args_str += ')'

    for i in range(nr_params):
        args_str += f' for x{i} in {repr(params[i].tolist())}'
    args_str += ']'

    return eval(args_str)

og_error = compute_error(input_base_dir, 1)
print(f'No transformation error: {og_error}')

try:
    for k in transformations.keys():
        error_list = []
        kwargs = transformations[k]['kwargs']
        if kwargs:
            # Create a list of tuples with the arguments for each call to the transformation
            arg_ks = list(kwargs.keys())
            nr_args = len(arg_ks)
            params = []
            for arg_k in arg_ks:
                pmin, pmax = kwargs[arg_k]
                k_params = np.linspace(pmin, pmax, nr_tests ** (1/nr_args))
                if arg_k == 'ksize':
                    k_params = np.unique((np.floor(k_params) // 2) * 2 + 1)
                params.append(k_params)
            args = params2args(params)

            print(args)

            f = open("plotting_data.txt", "w")

            # Compute mean error across hyperparamter space
            for i in range(len(args)):
                tf_dict = {"0":{k: {}}}
                for j in range(len(arg_ks)):
                    tf_dict["0"][k][arg_ks[j]] = args[i][j]
                error_list.append(compute_error(input_base_dir, 2, tf_dict, tmp_fn))

                error_str = f"Completed {i}/{len(args)} tests for {k}. "\
                            f"Current error is {np.mean(error_list):.3f}     "
                f.write(error_str)

            f.close()

            # Plot the mean error as a function of the parameters
            plot_error(args, error_list, k, arg_ks)

        else:
            tf_dict = {"0": {k: {}}}
            error_list.append(compute_error(input_base_dir, 2, tf_dict, tmp_fn))
            plot_error([1], error_list, k)
finally:
    if os.path.exists(tmp_fn):
        os.remove(tmp_fn)
