import os
import glob
import json
import argparse
import subprocess

import cv2
import matplotlib
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.svm import SVR
from sklearn.decomposition import PCA
from sklearn.multioutput import MultiOutputRegressor
from mpl_toolkits.mplot3d import Axes3D

import dproc.util as util

# Load mean images into memory for data normalization
mean_face = util.load_mat('assets/mean_face_224.mat')
mean_reye = util.load_mat('assets/mean_right_224.mat')
mean_leye = util.load_mat('assets/mean_left_224.mat')

class Subject:
    def __init__(self, subject_dir, nr_points):
        self.subject_dir = subject_dir
        self.nr_points = nr_points

        get_json = lambda name: util.get_json(os.path.join(subject_dir, name))
        self.face_json  = get_json('appleFace.json')
        self.reye_json  = get_json('appleRightEye.json')
        self.leye_json  = get_json('appleLeftEye.json')
        self.fgrid_json = get_json('faceGrid.json')
        self.frame_json = get_json('frames.json')
        self.dinfo_json = get_json('dotInfo.json')
        self.screen_json = get_json('screen.json')

        fa = np.array(self.face_json['IsValid'])
        re = np.array(self.reye_json['IsValid'])
        le = np.array(self.leye_json['IsValid'])
        self.valid = (fa == 1) & (re == 1) & (le == 1)

    def _get_sample(self, i):
        frame_fp = os.path.join(self.subject_dir, 'frames', self.frame_json[i])
        frame = cv2.imread(frame_fp, cv2.IMREAD_COLOR)
        frame = frame[..., ::-1]

        face_crop = util.face_crop(frame, self.face_json, i)
        reye_crop = util.eye_crop(frame, self.face_json, self.reye_json, i)
        leye_crop = util.eye_crop(frame, self.face_json, self.leye_json, i)
        fgrid = util.get_fgrid(self.fgrid_json, i)
        dinfo = util.get_dinfo(self.dinfo_json, i)

        # Resize crops to input size
        crops = (face_crop, reye_crop, leye_crop)
        crops = tuple(map(lambda crop: cv2.resize(crop, dsize=(224, 224), interpolation=cv2.INTER_CUBIC), crops))

        # Subtracte mean images from each crop
        crops = tuple(map(lambda crop: crop.astype(np.float32), crops))
        face, reye, leye = crops
        face -= mean_face
        reye -= mean_reye
        leye -= mean_leye

        # Pack features
        sample = (face, reye, leye, fgrid, dinfo)

        # Add batch dimension to features
        sample = tuple(map(lambda feat: np.expand_dims(feat, axis=0), sample))

        return sample

    def next_sample(self, mode='calibration'):
        total_nr_frames = len(self.valid)

        i = 0
        cur_orientation = 0
        cur_point = 0
        while i < total_nr_frames:

            # Check if frame is valid
            if not self.valid[i]:
                i += 1
                continue

            # Establish whether we sould return a sample or not depending on mode
            if cur_orientation != self.screen_json['Orientation'][i]:
                cur_orientation = self.screen_json['Orientation'][i]
                cur_point = -1

            if cur_point != self.dinfo_json['DotNum'][i]:
                cur_point += 1

            if mode == 'calibration':
                cond = cur_point < self.nr_points
            else:
                cond = cur_point >= self.nr_points

            # Yield sample if its point number is valid
            if cond:
                yield self._get_sample(i), cur_point, cur_orientation

            # Update frame index
            i += 1

def main(input_base_dir, output_base_dir, runs_base_dir, split, nr_points, run, relu, mean, kernel, C, epsilon, best):
    raw_dir = os.path.join(input_base_dir, 'raw')

    if not os.path.isdir(raw_dir):
        print(f"No raw directory inside specified input_dir ({input_base_dir})")
        return

    if best:
        model_type = 'best'
        tags = [tf.saved_model.tag_constants.SERVING]
    else:
        model_type = 'latest'
        tags = [tf.saved_model.tag_constants.TRAINING]

    output_dir = os.path.join(output_base_dir, f'run-{run}')
    output_fn_basename = f'loss-{split}-{kernel}-p{nr_points}'
    if relu:
        output_fn_basename += '-relu.txt'
    else:
        output_fn_basename += '-biasadd.txt'

    output_fn = os.path.join(output_dir, output_fn_basename)

    model_dir = os.path.join(runs_base_dir, f'run-{run}', 'models', model_type, '*')
    model_dir = sorted(glob.glob(model_dir))[-1]

    # Make output directory in case it doesn't exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # Get a list of all subjects
    subject_dirs = glob.glob(os.path.join(raw_dir, "*"))

    # Parse only the subjects from the specified split
    split_subjects = []
    for sd in subject_dirs:
        if not os.path.isdir(sd):
            continue
        info_fp = os.path.join(sd, 'info.json')
        info = util.get_json(info_fp)
        if info['Dataset'] == split:
            split_subjects.append(sd)

    with tf.Session() as sess:
        # Load SavedModel
        tf.saved_model.load(
            sess=sess,
            tags=tags,
            export_dir=model_dir)
        print(f"Successfully loaded model from {model_dir}")

        # Recover pred and loss tensors from graph
        g = tf.get_default_graph()
        batch_size_t = g.get_tensor_by_name("batch_length:0")
        preds_t = g.get_tensor_by_name("predictions:0")
        if relu:
            feat_vec_t = g.get_tensor_by_name("all/fc1/Relu:0")
        else:
            feat_vec_t = g.get_tensor_by_name("all/fc1/BiasAdd:0")

        total_error = []
        total_samples = 0
        for sd in split_subjects:

            subject = Subject(sd, nr_points)

            feat_vec_list, dinfo_list = [], []
            feat_vec_point, dinfo_point = [], []
            cur_point = 0
            for sample, point, ori in subject.next_sample(mode='calibration'):
                # Compute feature vector for given inputs
                feat_vec_np = get_feat_vec(sess, feat_vec_t, sample)
                if mean:
                    if cur_point == point:
                        feat_vec_point.append(feat_vec_np)
                        dinfo_point.append(sample[-1])
                    else:
                        cur_point = point
                        feat_vec_list.append(np.mean(feat_vec_point, axis=0))
                        dinfo_list.append(np.mean(dinfo_point, axis=0))
                        feat_vec_point = [feat_vec_np]
                        dinfo_point = [sample[-1]]
                else:
                    # Add calibration features and labels to a list
                    feat_vec_list.append(feat_vec_np)
                    dinfo_list.append(sample[-1])

            if mean:
                feat_vec_list.append(np.mean(feat_vec_point, axis=0))
                dinfo_list.append(np.mean(dinfo_point, axis=0))

            x_tr = np.concatenate(feat_vec_list, axis=0)
            y_tr = np.concatenate(dinfo_list, axis=0)

            print(x_tr.shape, y_tr.shape)

            classifier = MultiOutputRegressor(SVR(kernel=kernel, C=C, epsilon=epsilon))
            classifier.fit(x_tr, y_tr)

            preds, labels = [], []
            for sample, point, ori in subject.next_sample(mode='inference'):
                feat_vec_np = get_feat_vec(sess, feat_vec_t, sample)
                preds.append(classifier.predict(feat_vec_np))
                labels.append(sample[-1])

            preds = np.concatenate(preds, axis=0)
            labels = np.concatenate(labels, axis=0)

            nr_samples = len(preds)
            squared_diff = (preds - labels)**2
            error = np.mean(np.sqrt(np.sum(squared_diff, axis=1)))

            print(f"Subject {int(os.path.basename(sd))} error is {error}")

            total_error.append(error*nr_samples)
            total_samples += nr_samples

        error = np.sum(total_error)/total_samples
        out_str = f"Model dir: {model_dir}\n"\
                  f"Dataset split: {split}\n"\
                  f"Total samples: {total_samples}\n"\
                  f"Calibrate with mean feat_vec: {mean}\n"\
                  f"Feature vector layer name: {feat_vec_t.name}\n"\
                  f"Calibration points: {nr_points}\n"\
                  f"SVM params: C = {C}, epsilon = {epsilon}\n"\
                  f"Mean error: {error}\n"\
                  "=========================================\n"
        with open(output_fn, "a") as f:
            f.write(out_str)
        print(out_str)

        return error

def get_feat_vec(sess, feat_vec_tensor, sample):
    # Create dictionary with features
    face, reye, leye, fgrid, dinfo = sample
    feed_dict = {
        "face:0": face,
        "reye:0": reye,
        "leye:0": leye,
        "fgrid:0": fgrid,
    }

    return sess.run(feat_vec_tensor, feed_dict=feed_dict)

def visualize_svm(x_tr, y_tr):
    markers = ['o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X']
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

    pca = PCA(n_components=2).fit(x_tr)
    pca_2d = pca.transform(x_tr)

    pca_y = PCA(n_components=1).fit(y_tr)
    pca_y_1d = pca_y.transform(y_tr)

    classifier = MultiOutputRegressor(SVR(kernel='rbf', gamma='auto')).fit(pca_2d, pca_y_1d)

    prev_point = np.array([0.0, 0.0])
    point_id = -1
    for i in range(len(y_tr)):
        if not (prev_point == y_tr[i]).all():
            prev_point = y_tr[i]
            point_id += 1
        marker = markers[point_id%len(markers)]
        color = colors[point_id%len(colors)]

        plt.scatter(pca_2d[i,0], pca_2d[i,1], c=color, marker=marker)

    x_min, x_max = pca_2d[:, 0].min() - 1, pca_2d[:,0].max() + 1
    y_min, y_max = pca_2d[:, 1].min() - 1, pca_2d[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, .01), np.arange(y_min, y_max, .01))
    Z = classifier.predict(np.c_[xx.ravel(),  yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.contour(xx, yy, Z)
    plt.show()

def compute_error(input_base_dir, run, split, augm_factor, tf_dict=None, tmp_fn=None):
    if tf_dict:
        save_dict(tf_dict, tmp_fn)
    args = [
        "python3", "infer.py",
        "-i", input_base_dir,
        "-o", "test",
        "--run", str(run),
        "--split", split,
        "--augm_factor", str(augm_factor),
        "--best",
        "--tfr",
    ]
    if tmp_fn:
        args += ["--load", tmp_fn]
    cp = subprocess.run(
        args,
        stdout=subprocess.PIPE
    )
    error_str = cp.stdout.decode("utf-8").strip().split("\n")[-1]
    error = float(error_str.split(':')[-1])
    return error

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_base_dir', action='store', required=True,
                        help='Base directory with input files for prediction. By default, we assume '\
                        '`input_dir` is a directory with subject dirs for inference. If it contains tfrecords '\
                        'we must specify that with the --tfr flag.')
    parser.add_argument('-o', '--output_base_dir', action='store', required=False, default='results',
                        help='By default, inference results are printed to stdout, but if `output_dir` '\
                        'is specified, results will be stored as csv files for predictions and text files for metrics')
    parser.add_argument('-e', '--runs_base_dir', action='store', required=False, default='runs',
                        help='Base directory to export and/or load saved model')
    parser.add_argument('--run', action='store', type=int, required=True,
                        help='Run id (int) to run inference on')
    parser.add_argument('--split', action='store', required=False, default='test',
                        help='What dataset split to use as inputs to compute metrics. Should be one of'\
                        ' val, test, or train')
    parser.add_argument('--nr_points', action='store', type=int, default=13,
                        help='Number of calibration points to use')
    parser.add_argument('--kernel', action='store', default='linear',
                        help='Kernel type to use when training SVR classifier')
    parser.add_argument('--num_sep', action='store', type=int, default=10,
                        help='Number of log-spaced samples to use when exploring hyperparameters')
    parser.add_argument('--relu', action='store_true',
                        help='If flag is present, use the feature vector after ReLU activation to train classifier')
    parser.add_argument('--mean', action='store_true',
                        help='If flag is present, train SVR with mean feature vectors for each point')
    parser.add_argument('--best', action='store_true',
                        help='Flag indicating whether to use best or latest model')


    kwargs = vars(parser.parse_args())
    og_error = compute_error(kwargs['input_base_dir'], kwargs['run'], kwargs['split'], 1)

    Cs = np.logspace(-3, 1, num=kwargs['num_sep'], base=10)
    es = np.logspace(-3, 1, num=kwargs['num_sep'], base=10)

    # Remove unecessary args for main
    kwargs.pop('num_sep')

    error_list = []
    for C in Cs:
        for epsilon in es:
            # Modify params in dict
            kwargs['C'] = C
            kwargs['epsilon'] = epsilon

            # Compute error for hyperparam combination
            error_list.append(main(**kwargs))

    # Plot error as a surface plot
    fig = plt.figure(figsize=(10,10))
    ax = Axes3D(fig)

    # Change x and y axis scale to logarithmic
    xticks = np.logspace(-3, 1, num=5, base=10)
    yticks = np.logspace(-3, 1, num=5, base=10)
    ax.set_xticks(np.log10(xticks))
    ax.set_xticklabels(xticks)
    ax.set_yticks(np.log10(yticks))
    ax.set_yticklabels(yticks)

    # Set z axis limits
    ax.set_zlim(min(np.min(error_list), og_error), max(np.max(error_list), og_error))

    # Make surface plot with errors and add colorbar
    X, Y =  np.meshgrid(np.log10(Cs), np.log10(es), indexing='ij')
    Z = np.reshape(error_list, X.shape)
    surf = ax.plot_surface(X=X, Y=Y, Z=Z, cmap=matplotlib.cm.coolwarm)

    fig.colorbar(surf, shrink=0.3, aspect=10)

    # Plot error with no transformations as a wireframe
    X = ax.get_xticks()
    Y = ax.get_yticks()
    X, Y = np.meshgrid(X, Y, indexing='ij')
    Z = np.broadcast_to(og_error, X.shape)
    ax.plot_wireframe(X=X, Y=Y, Z=Z, color=(0.3, 0.3, 1.0))

    # Set title and labels
    plot_title = f"{kwargs['split']}-{kwargs['kernel']}-{kwargs['nr_points']}p-run{kwargs['run']}"
    if kwargs['relu']:
        plot_title += '-relu'
    else:
        plot_title += '-biasadd'
    ax.set_title(plot_title)
    ax.set_zlabel('Error')
    ax.set_xlabel('C')
    ax.set_ylabel('epsilon')

    output_dir = os.path.join(kwargs['output_base_dir'], f"run-{kwargs['run']}")
    fig.savefig(os.path.join(output_dir, f'{plot_title}.png'))
