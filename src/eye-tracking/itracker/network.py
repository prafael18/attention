import os
import glob
import time
import shutil

import numpy as np
import tensorflow as tf

import util

class iTrackerNetwork:
    def __init__(self, session, train_dataset, val_dataset, base_export_dir, log_dir, resume_training, flow=False):

        # Wether or not the network should include optical flow
        self.flow = flow

        # Set regularization hyper param
        self.weight_decay = 5e-4

        # Set export dirs for best and latest models
        self.best_export_dir = os.path.join(base_export_dir, "best")
        self.latest_export_dir = os.path.join(base_export_dir, "latest")

        if resume_training:
            # Load model graph and variables
            saver = self.load(session, training=True)

            # When saving, we must use this auxiliary tf.Saver to prevent SavedModel error with saveable objects.
            self.saver = saver

            # Create summary writer object to log loss summaries and graph data
            self.summary_writer = tf.summary.FileWriter(log_dir, session.graph)

            # Restore variables
            self.global_step = util.retrieve_var("global_step")
            self.epoch = util.retrieve_var("epoch")
            self.best_val_loss = util.retrieve_var("best_val_loss")
            self.cur_train_loss = util.retrieve_var("current_train_loss")
            self.cur_train_samples = util.retrieve_var("current_train_samples")
            self.training_time = time.time()

            # Restore tensors and ops
            g = tf.get_default_graph()

            # Define model inputs and outputs for serving API
            self.inputs = {
                "face": g.get_tensor_by_name("face:0"),
                "reye": g.get_tensor_by_name("reye:0"),
                "leye": g.get_tensor_by_name("leye:0"),
                "fgrid": g.get_tensor_by_name("fgrid:0"),
            }
            self.outputs = {
                "predictions": g.get_tensor_by_name("predictions:0")
            }

            self.batch_size = g.get_tensor_by_name("batch_length:0")
            self.pred = g.get_tensor_by_name("predictions:0")
            self.loss = g.get_tensor_by_name("loss:0")
            self.error = g.get_tensor_by_name("euclidean_error:0")
            self.train_op = g.get_operation_by_name("train")

            self.train_iterator_init = g.get_operation_by_name("data/MakeIterator")
            self.val_iterator_init = g.get_operation_by_name("data/MakeIterator_1")
            self.train_handle = g.get_tensor_by_name("data/train_handle:0").eval(session=session)
            self.val_handle = g.get_tensor_by_name("data/val_handle:0").eval(session=session)

        else:
            # Create a feedable iterator
            with tf.name_scope("data"):
                handle = tf.placeholder(tf.string, shape=[], name="handle")
                iterator = tf.data.Iterator.from_string_handle(
                    handle, train_dataset.output_types, train_dataset.output_shapes)

                # Get initializable iterators from datasets
                train_iterator = train_dataset.make_initializable_iterator()
                val_iterator = val_dataset.make_initializable_iterator()

                # The `Iterator.string_handle()` method returns a tensor that can be evaluated
                # and used to feed the `handle` placeholder.
                train_handle = train_iterator.string_handle(name="train_handle")
                val_handle = val_iterator.string_handle(name="val_handle")

                # Get train and val init ops
                train_iterator_init = iterator.make_initializer(train_dataset, name="train_iterator_init")
                val_iterator_init = iterator.make_initializer(val_dataset, name="val_iterator_init")

                # Create saveable object from iterator. This allows us to continue from exact
                # batch when resuming training
                # saveable_iterator = tf.data.experimental.make_saveable_from_iterator(train_iterator)

                # Save the iterator state by adding it to the saveable objects collection.
                # tf.add_to_collection(tf.GraphKeys.SAVEABLE_OBJECTS, saveable_iterator)

                # Get symbolic tensors for input features and output labels
                features, labels = iterator.get_next()

            # Unpack features tuple
            if flow:
                face, reye, leye, fgrid, optflow = features
            else:
                face, reye, leye, fgrid = features

            # Rename input tensors for easy recovery
            face = tf.identity(face, name="face")
            reye = tf.identity(reye, name="reye")
            leye = tf.identity(leye, name="leye")
            fgrid = tf.identity(fgrid, name="fgrid")
            labels = tf.identity(labels, name="labels")

            # Define model inputs for tensorflow serving API
            inputs_dict = {
                "face": face,
                "reye": reye,
                "leye": leye,
                "fgrid": fgrid,
            }

            if flow:
                optflow = tf.identity(optflow, name="optflow")
                inputs_dict["optflow"] = optflow

            # Get batch size for each iteration. This is usefuly to calculate an accurate
            # validation loss, since the batch size may vary nearing the end of the dataset
            # in case we don't have a multiple of batch_size elements
            batch_size = tf.identity(tf.shape(face)[0], name="batch_length")

            # Use the same convolutional network to process face, reye, and leye, with
            # shared weights for the eyes
            with tf.variable_scope("eyes"):
                with tf.name_scope("reye"):
                    with tf.variable_scope("eye", reuse=tf.AUTO_REUSE):
                        reye = self.image_model(reye)
                with tf.name_scope("leye"):
                    with tf.variable_scope("eye", reuse=tf.AUTO_REUSE):
                        leye = self.image_model(leye)
                eyes = tf.concat([reye, leye], axis=1)
                eyes = tf.layers.dense(inputs=eyes,
                                        name="fc1",
                                        units=128,
                                        bias_initializer=tf.initializers.constant(1.0),
                                        kernel_initializer=tf.initializers.random_normal(stddev=0.001),
                                        activation=tf.nn.relu)

            # Layers for facegrid
            with tf.variable_scope("faces"):
                face = self.image_model(face)
                face = tf.layers.dense(inputs=face,
                                        name="fc1",
                                        units=128,
                                        bias_initializer=tf.initializers.constant(1.0),
                                        kernel_initializer=tf.initializers.random_normal(stddev=0.001),
                                        activation=tf.nn.relu)
                face = tf.layers.dense(inputs=face,
                                        name="fc2",
                                        units=64,
                                        bias_initializer=tf.initializers.constant(1.0),
                                        kernel_initializer=tf.initializers.random_normal(stddev=0.001),
                                        activation=tf.nn.relu)

            # Layers for facegrid
            with tf.variable_scope("fgrids"):
                fgrid = tf.layers.flatten(fgrid)
                fgrid = tf.layers.dense(inputs=fgrid,
                                        name="fc1",
                                        units=256,
                                        bias_initializer=tf.initializers.constant(1.0),
                                        kernel_initializer=tf.initializers.random_normal(stddev=0.001),
                                        activation=tf.nn.relu)
                fgrid = tf.layers.dense(inputs=fgrid,
                                        name="fc2",
                                        units=128,
                                        bias_initializer=tf.initializers.constant(1.0),
                                        kernel_initializer=tf.initializers.random_normal(stddev=0.001),
                                        activation=tf.nn.relu)

            # Join all input streams
            with tf.variable_scope("all"):
                all = tf.concat([eyes, face, fgrid], axis=1)
                all = tf.layers.dense(inputs=all,
                                        name="fc1",
                                        units=128,
                                        bias_initializer=tf.initializers.constant(1.0),
                                        kernel_initializer=tf.initializers.random_normal(stddev=0.001),
                                        activation=tf.nn.relu)
                if flow:
                    all = tf.concat([all, optflow], axis=1)
                predictions = tf.layers.dense(inputs=all,
                                        name="fc2",
                                        units=2,
                                        bias_initializer=tf.initializers.constant(0.0),
                                        kernel_initializer=tf.initializers.random_normal(stddev=0.001),
                                        activation=None)

            # Identity op so we can easily recover predictions tensor
            predictions = tf.identity(predictions, name="predictions")

            # Define model outputs for tensorflow serving API
            outputs_dict = {
                "predictions": predictions
            }

            # Define useful variables to keep track during training
            global_step = tf.get_variable("global_step", initializer=0, trainable=False)
            epoch = tf.Variable(0, name="epoch", trainable=False)
            best_val_loss = tf.Variable(float('inf'), name="best_val_loss", trainable=False)
            cur_train_loss = tf.Variable(0.0, name="current_train_loss", trainable=False)
            cur_train_samples = tf.Variable(0, name="current_train_samples", trainable=False)

            # Keep track of training start time
            training_time = tf.Variable(0.0, name="training_time", trainable=False)

            # Define model loss (for some reason, tf.losses doesn't allow name assignment)
            loss = tf.losses.mean_squared_error(labels=labels, predictions=predictions)
            loss += tf.losses.get_regularization_loss()
            loss = tf.identity(loss, name='loss')

            # Define euclidean error tensors. Needed for comparison with paper metrics.
            l2_norm = tf.norm(predictions-labels, ord='euclidean', axis=1)
            l2_norm = tf.reduce_mean(l2_norm, name="euclidean_error")

            # Since we want an adaptive learning rate, we'll use a placeholder tensor that we can pass when training
            learning_rate = tf.placeholder(tf.float32, shape=(), name="learning_rate")


            # Separate biases from kernels
            biases = [v for v in tf.trainable_variables() if 'bias' in v.name]
            kernels = [v for v in tf.trainable_variables() if 'kernel' in v.name]

            # We use different optimizers in order to apply a different learning rate
            # to bias and kernels
            bias_opt = tf.train.MomentumOptimizer(learning_rate=learning_rate*2,
                                                    momentum=0.9)
            kernel_opt = tf.train.MomentumOptimizer(learning_rate=learning_rate,
                                                   momentum=0.9)

            # Compute gradients for biases and kernels
            grads = tf.gradients(loss, biases + kernels)
            bias_grads = grads[:len(biases)]
            kernel_grads = grads[len(biases):]

            # Apply gradients to optimizers and include global step only in one of them
            bias_train_op = bias_opt.apply_gradients(zip(bias_grads, biases))
            kernel_train_op = kernel_opt.apply_gradients(zip(kernel_grads, kernels), global_step)

            # Create a final train op
            train_op = tf.group(bias_train_op, kernel_train_op, name="train")

            # Initialize variables
            init_op = tf.global_variables_initializer()
            session.run(init_op)

            # Set object properties
            self.saver = tf.train.Saver()
            self.train_iterator_init = train_iterator.initializer
            self.val_iterator_init = val_iterator.initializer
            self.train_handle = train_handle.eval(session=session)
            self.val_handle = val_handle.eval(session=session)
            self.summary_writer = tf.summary.FileWriter(log_dir, session.graph)
            # self.dropout_keep_prob = dropout_keep_prob
            self.inputs = inputs_dict
            self.outputs = outputs_dict
            self.global_step = global_step
            self.epoch = epoch
            self.batch_size = batch_size
            self.best_val_loss = best_val_loss
            self.pred = predictions
            self.loss = loss
            self.error = l2_norm
            self.train_op = train_op
            self.cur_train_loss = cur_train_loss
            self.cur_train_samples = cur_train_samples
            self.training_time = time.time()

    def save(self, session, training=True, overwrite=True):

        # Get current training step
        step = tf.train.get_global_step().eval(session=session)

        # If saved model is intended to be used for serving, we should create a
        # SignatureDef that complies with tensorflow's serving API.
        if training:
            base_export_dir = self.latest_export_dir
            tags = [tf.saved_model.tag_constants.TRAINING]
            signature_def_map = None
        else:
            base_export_dir = self.best_export_dir
            tags = [tf.saved_model.tag_constants.SERVING]
            signature_def_map = {
              tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
                  tf.saved_model.signature_def_utils.predict_signature_def(self.inputs, self.outputs)
            }


        # Create export dir with current step
        export_dir = os.path.join(base_export_dir, f"{step:06}")

        # Create a SavedModelBuilder object to save graph and variables
        builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
        builder.add_meta_graph_and_variables(session,
            tags=tags,
            signature_def_map=signature_def_map,
            saver=self.saver)
        builder.save()

        print(f"{self.elapsed_time(session)} Saved model to {export_dir}             ")

        # Get dir path with second highest index to overwrite. We overwrite after
        # saving to guarantee that we won't be left with an incomplete model if
        # there's a power failure.
        if overwrite:
            export_dirs = sorted(glob.glob(os.path.join(base_export_dir, "*")))
            if len(export_dirs) >= 2:
                newest_export_dir = export_dirs[-2]
                shutil.rmtree(newest_export_dir)

    def load(self, session, training):
        if training:
            base_export_dir = self.latest_export_dir
            tags = [tf.saved_model.tag_constants.TRAINING]
        else:
            base_export_dir = self.best_export_dir
            tags = [tf.saved_model.tag_constants.SERVING]

        export_dirs = sorted(glob.glob(os.path.join(base_export_dir, "*")))
        if export_dirs:
            export_dir = export_dirs[-1]
        else:
            print("No SavedModel available to restore.")
            exit(1)

        # TODO: Perhaps we need a different loading behavior for serving
        # (https://stackoverflow.com/questions/33759623/tensorflow-how-to-save-restore-a-model/47235448#47235448)

        # Load variables for default graph defined in current session
        meta_graph_def = tf.saved_model.load(session, tags, export_dir)
        saver = tf.train.import_meta_graph(meta_graph_def)
        saver.restore(session, f"{export_dir}/variables/variables")
        print(f"Loaded model and variables from {export_dir}")
        return saver

    def validate(self, session):
        session.run(self.val_iterator_init)
        val_batches_loss = []
        val_batches_error = []
        val_samples = 0
        while True:
            try:
                val_loss, val_error, n = session.run((self.loss, self.error, self.batch_size),
                    feed_dict={"data/handle:0": self.val_handle})
                val_batches_loss.append(val_loss*n)
                val_batches_error.append(val_error*n)
                val_samples += n
            except tf.errors.OutOfRangeError:
                # Compute mean validation loss and error across all samples
                val_loss = np.sum(val_batches_loss)/val_samples
                val_error = np.sum(val_batches_error)/val_samples
                # Log validation loss for tensorboard visualization
                step = self.global_step.eval(session=session)
                self.log_scalar('val_loss', val_loss, step)
                self.log_scalar('val_error', val_error, step)
                val_str = \
                    f"Current validation loss over {val_samples} samples is "\
                    f"{val_loss:.3f} and error is {val_error:.3f}"
                if val_loss < self.best_val_loss.eval():
                    val_str += " (new best)!"
                    # Update best_val_loss variable
                    assign_best_loss = self.best_val_loss.assign(val_loss)
                    session.run(assign_best_loss)
                    # Save model with best validation loss for serving
                    self.save(session=session, training=False, overwrite=True)
                val_str = f"{self.elapsed_time(session)} " + val_str
                print(val_str)

                return val_loss

    def log_scalar(self, tag, value, step):
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=value)])
        self.summary_writer.add_summary(summary, step)

    def elapsed_time(self, session):
        return util.timef(time.time() - self.training_time)

    def image_model(self, inputs):
        # In order to define what padding to use for each layer we used the pytorch
        # code as a reference along with the Conv2D function documentation at
        # https://pytorch.org/docs/stable/nn.html#torch.nn.Conv2d to calculate what
        # was the expected output shape for each layer.
        conv1 = tf.layers.conv2d(inputs=inputs,
                                name="conv1",
                                filters=96,
                                kernel_size=[11,11],
                                strides=[4,4],
                                padding="valid",
                                bias_initializer=tf.initializers.constant(0.0),
                                kernel_initializer=tf.initializers.random_normal(stddev=0.01),
                                kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay),
                                activation=tf.nn.relu)
        pool1 = tf.layers.max_pooling2d(inputs=conv1,
                                name="pool1",
                                pool_size=[3,3],
                                strides=[2,2],
                                padding="valid")
        norm1 = tf.nn.local_response_normalization(input=pool1,
                                name="norm1",
                                depth_radius=2, # Equivalent to n=5 in AlexNet paper
                                bias=1.0,
                                alpha=1e-4,
                                beta=0.75)
        conv2 = tf.layers.conv2d(inputs=norm1,
                                name="conv2",
                                filters=256,
                                kernel_size=[5,5],
                                strides=[1,1],
                                padding="same",
                                bias_initializer=tf.initializers.constant(1.0),
                                kernel_initializer=tf.initializers.random_normal(stddev=0.01),
                                kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay),
                                activation=tf.nn.relu)
        pool2 = tf.layers.max_pooling2d(inputs=conv2,
                                name="pool2",
                                pool_size=[3,3],
                                strides=[2,2],
                                padding="valid")
        norm2 = tf.nn.local_response_normalization(input=pool2,
                                name="norm2",
                                depth_radius=2, # Equivalent to n=5 in AlexNet paper
                                bias=1.0,
                                alpha=1e-4,
                                beta=0.75)
        conv3 = tf.layers.conv2d(inputs=norm2,
                                name="conv3",
                                filters=384,
                                kernel_size=[3,3],
                                strides=[1,1],
                                padding="same",
                                bias_initializer=tf.initializers.constant(0.0),
                                kernel_initializer=tf.initializers.random_normal(stddev=0.01),
                                kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay),
                                activation=tf.nn.relu)
        conv4 = tf.layers.conv2d(inputs=conv3,
                                name="conv4",
                                filters=64,
                                kernel_size=[1,1],
                                strides=[1,1],
                                padding="same",
                                bias_initializer=tf.initializers.constant(0.0),
                                kernel_initializer=tf.initializers.random_normal(stddev=0.01),
                                kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay),
                                activation=tf.nn.relu)
        flat = tf.layers.flatten(conv4, name="flatten")
        return flat
