import os
import glob
import time
import argparse

import numpy as np
import tensorflow as tf
from tensorflow.python import debug as tf_debug

import data
from util import timef, clean_dirs, show_sample

def main(input_base_dir, output_base_dir, runs_base_dir, split, run, augm_factor, tfr, best, debug, show, clean, load_fn):

    input_dir = os.path.join(input_base_dir, 'tfr', split)
    output_dir = os.path.join(output_base_dir, f'run-{run}')


    if augm_factor > 1:
        output_fn_basename = f"loss-{split}-augm{augm_factor}x"
    else:
        output_fn_basename = f"loss-{split}"

    if best:
        model_type = 'best'
        tags = [tf.saved_model.tag_constants.SERVING]
    else:
        model_type = 'latest'
        tags = [tf.saved_model.tag_constants.TRAINING]

    model_dir = os.path.join(runs_base_dir, f'run-{run}', 'models', model_type, '*')
    model_dir = sorted(glob.glob(model_dir))[-1]

    # Make output directory in case it doesn't exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # Clean output directoy if requested
    if clean:
        clean_dirs([output_dir])

    if not tfr:
        print("This option hasn't been implemented yet!")
        return

    input_filenames = glob.glob(os.path.join(input_dir, "*"))
    batch_size = 200

    if batch_size%augm_factor != 0:
        print("Batch size must be divisible by the augmenation factor")
        return

    dataset = data.get_dataset(
        filenames=input_filenames,
        batch_size=batch_size,
        train=False,
        augmentation_factor=augm_factor,
        load_fn=load_fn,
        save_fn=os.path.join(output_dir, output_fn_basename + ".json")
    )
    iterator = dataset.make_one_shot_iterator()
    features, labels = iterator.get_next()
    face, reye, leye, fgrid = features

    with tf.Session() as sess:

        # Enable CLI debugging
        if debug:
            sess = tf_debug.LocalCLIDebugWrapperSession(sess)

        # Load SavedModel
        tf.saved_model.load(
            sess=sess,
            tags=tags,
            export_dir=model_dir)
        print(f"Successfully loaded model from {model_dir}")

        # Recover pred and loss tensors from graph
        g = tf.get_default_graph()
        preds = g.get_tensor_by_name("predictions:0")
        batch_size = g.get_tensor_by_name("batch_length:0")

        if augm_factor > 1:

            # Get the mean value between augmented predictions
            augm_preds = tf.reshape(preds, shape=[-1, augm_factor, 2])
            augm_preds = tf.reduce_mean(augm_preds, axis=1)

            # Get mean labels for augmented predictions
            augm_labels = tf.reshape(labels, shape=[-1, augm_factor, 2])
            augm_labels = tf.reduce_mean(augm_labels, axis=1)

            print(augm_labels, augm_preds)

            loss = tf.reduce_mean(tf.math.squared_difference(augm_preds, augm_labels))
            euclidean_error = tf.reduce_mean(tf.norm(augm_preds-augm_labels, ord='euclidean', axis=1))
        else:
            loss = tf.reduce_mean(tf.math.squared_difference(preds, labels))
            euclidean_error = tf.reduce_mean(tf.norm(preds-labels, ord='euclidean', axis=1))

        evaluate_start = time.time()
        test_batches_loss = []
        test_batches_error = []
        test_samples = 0
        while True:
            try:
                # Evaluate dataset iterator tensors
                feed_dict = sess.run(
                        {
                            "face:0": face,
                            "reye:0": reye,
                            "leye:0": leye,
                            "fgrid:0": fgrid,
                            "labels:0": labels
                        })

                # Display one sample from the batch with matplotlib
                if show:
                    for i in range(feed_dict['face:0'].shape[0]):
                        features = tuple(map(
                            lambda feat: feed_dict[f"{feat}:0"][i],
                            ('face', 'reye', 'leye', 'fgrid', 'labels')
                        ))
                        show_sample(features)

                # Compute pred and loss tensors
                preds_np, n = sess.run(
                    fetches=[preds, batch_size],
                    feed_dict=feed_dict
                )

                loss_np, error_np = sess.run(
                    fetches=[loss, euclidean_error],
                    feed_dict={
                        preds: preds_np,
                        labels: feed_dict['labels:0']
                    }
                )

                # We count the number of actual samples, not the augmented number
                n = n//augm_factor

                test_batches_loss.append(loss_np*n)
                test_batches_error.append(error_np*n)
                test_samples += n
                cur_test_loss = np.sum(test_batches_loss)/test_samples
                elapsed_time = time.time() - evaluate_start
                print(f"{timef(elapsed_time)} Current loss is {cur_test_loss:.3f} "\
                      f"({test_samples} samples)     \r", end="")
            except tf.errors.OutOfRangeError:
                elapsed_time = time.time() - evaluate_start
                test_loss = np.sum(test_batches_loss)/test_samples
                test_error = np.sum(test_batches_error)/test_samples
                test_out_str = \
                    f"Evaluation time: {timef(elapsed_time)}" + " "*30 + "\n"\
                    f"Number of samples: {test_samples}\n"\
                    f"Input directory: {input_dir}\n"\
                    f"Model directory: {model_dir}\n"\
                    f"Average loss: {test_loss}\n"\
                    f"Average euclidean_error: {test_error}\n"


                if output_dir:
                    with open(os.path.join(output_dir, output_fn_basename + ".txt"), "w") as f:
                        f.write(test_out_str)

                print(test_out_str, end="")
                break

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_base_dir', action='store', required=True,
                        help='Base directory with input files for prediction. By default, we assume '\
                        '`input_dir` is a directory with subject dirs for inference. If it contains tfrecords '\
                        'we must specify that with the --tfr flag.')
    parser.add_argument('-o', '--output_base_dir', action='store', required=False, default='results',
                        help='By default, inference results are printed to stdout, but if `output_dir` '\
                        'is specified, results will be stored as csv files for predictions and text files for metrics')
    parser.add_argument('-e', '--runs_base_dir', action='store', required=False, default='runs',
                        help='Base directory to export and/or load saved model')
    parser.add_argument('--run', action='store', type=int, required=True,
                        help='Run id (int) to run inference on')
    parser.add_argument('--split', action='store', required=False, default='test',
                        help='What dataset split to use as inputs to compute metrics. Should be one of'\
                        ' val, test, or train')
    parser.add_argument('--augm_factor', action='store', type=int, required=False, default=1,
                        help='How many times to increase dataset by.')
    parser.add_argument('--best', action='store_true',
                        help='Flag indicating whether to use best or latest model')
    parser.add_argument('--tfr', action='store_true', required=False,
                        help='Flag indicating that `input_dir` has tfrecords for inference')
    parser.add_argument('--debug', action='store_true', required=False,
                        help='Flag to enable CLI debugging with tfdbg')
    parser.add_argument('--show', action='store_true', required=False,
                        help='Flag to display one input sample per batch with matplotlib')
    parser.add_argument('--clean', action='store_true', required=False,
                        help='Remove files from `output_dir` to write new ones')
    parser.add_argument('--load_fn', action='store', required=False,
                        help='JSON file with dictionary of transformations to use for inference.')
    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    main(**kwargs)
