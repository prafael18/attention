import tensorflow as tf
from tensorflow.python.framework import tensor_shape

# Parse example protobuf into feature dict
def parse_tfr(example_proto):
    feature_description = {
        'images': tf.VarLenFeature(tf.string),
        'maps': tf.VarLenFeature(tf.string),
        'fixations': tf.VarLenFeature(tf.string),
        'width'  : tf.FixedLenFeature([], tf.int64, default_value=0),
        'height'  : tf.FixedLenFeature([], tf.int64, default_value=0),
        'nr_frames'  : tf.FixedLenFeature([], tf.int64, default_value=0),
    }

    features = tf.parse_single_example(example_proto, feature_description)
    return features

# Filter sequences that don't have enough frames to sample from
def filter_fn(features, seq_length):
    return tf.cond(
        pred=features['nr_frames'] >= seq_length,
        true_fn=lambda: True,
        false_fn=lambda: False
    )

# Helper function to parse features into feedable data
def parse_feat(features, seq_length, feature_shape, label_shape, train):
    # Unpack shape parameters
    feature_height, feature_width = feature_shape

    nr_frames = features['nr_frames']
    width = tf.cast(features['width'], dtype=tf.int32)
    height = tf.cast(features['height'], dtype=tf.int32)


    if label_shape is None:
        label_height, label_width = height, width
    else:
        label_height, label_width = label_shape[0], label_shape[1]

    # Convert tensors to dense
    features = tuple(map(
        lambda key: tf.sparse.to_dense(features[key], default_value=''),
        ('images', 'fixations', 'maps')
    ))

    # Sample randomly chosen seq_length frames from each feature.
    # We add 1 to maxval since maxval isn't sampled from
    start = 0
    if train:
        start = tf.random.uniform((), minval=0, maxval=nr_frames-seq_length+1, dtype=tf.int64)
    features = tuple(map(
        lambda feat: feat[start:start+seq_length],
        features
    ))

    # For each feature type, we use tf.map_fn to iterate over rows decoding pngs
    images, fixations, maps = tuple(map(
        lambda params: tf.map_fn(
            lambda frame:
                tf.image.resize_image_with_pad(
                    image=tf.image.decode_png(frame, channels=0, dtype=tf.uint8),
                    target_height=params[1],
                    target_width=params[2],
                    method=tf.image.ResizeMethod.BILINEAR
                ),
            params[0],  # Feature can be image, fixation or map
            dtype=tf.float32
        ),
        zip(
            features,
            [feature_height, label_height, label_height],
            [feature_width, label_width, label_width]
        ),
    ))

    # Use same image pre processing of VGG-16 since we will initialize weights
    # with pre-trained network
    # ACLNet's pre-processing incorrectly bases itself on https://github.com/fchollet/deep-learning-models/blob/master/imagenet_utils.py,
    # where they subtract the mean BGR values from an RGB image before converting to a BGR image
    # We opted to pre-process data according to the official VGG-16 repo
    # (https://gist.github.com/ksimonyan/3785162f95cd2d5fee77#file-readme-md)
    images = images[..., ::-1]                                     # RGB -> BGR
    images = tf.math.subtract(images, [103.939, 116.779, 123.680]) # Mean subtraction from each channel

    # Casting to bool ensures that any value different than 0 will have value 1 when
    # cast back to float
    fixations = tf.cast(fixations, dtype=tf.bool)
    fixations = tf.cast(fixations, dtype=tf.float32)

    # Scale between [0, 1]
    maps = tf.math.divide(maps, 255)

    # We need to set the shapes of seq_length and label height and width as undefined
    # in order for both static and dynamic iterators to have the same output shape
    images = tf.reshape(images, [seq_length, feature_height, feature_width, 3])
    fixations = tf.reshape(fixations, [seq_length, label_height, label_width, 1])
    maps = tf.reshape(maps, [seq_length, label_height, label_width, 1])

    return images, fixations, maps

def get_dataset(filenames, batch_size, seq_length, feature_shape, label_shape, train=False):
    nr_files = len(filenames)
    block_length = tf.maximum(tf.constant(1, dtype=tf.int64), tf.cast(batch_size/nr_files, tf.int64))

    # Use interleave and prefetch to read many files concurrently. We use nr_files buffer size to
    # guarantee uniformity when shuffling filenames. The cycle_length guarantees we're sampling from
    # all files simultaneously and `block_length` tells us how many samples to get from
    # each file before getting cycling again through files
    filenames = tf.data.Dataset.from_tensor_slices(filenames)
    if train:
        filenames = filenames.shuffle(buffer_size=nr_files)
    dataset = filenames.interleave(lambda f: tf.data.TFRecordDataset(f).prefetch(block_length),
                               cycle_length=nr_files,
                               block_length=block_length)

    # Parse tfrecords into feature dictionaries
    dataset = dataset.map(
        map_func=parse_tfr,
        num_parallel_calls=4
    )

    # Filter examples that don't have enough frames to sample from
    dataset = dataset.filter(
        predicate=lambda features: filter_fn(features, seq_length)
    )

    # Parallelize map with 4 threads (approx. number of cpu cores). Shuffle buffer
    # size doesn't have to be too big since we already sample from randomly shuffled files
    dataset = dataset.map(
        lambda features: parse_feat(features, seq_length, feature_shape, label_shape, train),
        num_parallel_calls=4
    )
    if train:
        dataset = dataset.shuffle(buffer_size=5*batch_size)
    dataset = dataset.batch(batch_size=batch_size)

    # Use prefetch to have at least one batch to overlap producer and consumer
    dataset = dataset.prefetch(15)

    return dataset
