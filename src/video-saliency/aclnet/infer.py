import os
import glob
import math
import argparse

import cv2
import scipy
import numpy as np
import tensorflow as tf
from scipy.misc import imshow, imresize
from tensorflow.python import debug as tf_debug

import util
import data
import network

# Expects a 3-D tensor of shape (height, width, channels)
def proc_predictions(image, target_shape):
    height, width = image.shape[:2]
    target_height, target_width = target_shape[:2]

    # Normalize predictions first
    image = (image/np.max(image))*255

    # Resize image keeping its aspect ratio
    ratio = min(target_height/height, target_width/width)
    res_image = cv2.resize(image, dsize=(0, 0), fx=ratio, fy=ratio, interpolation=cv2.INTER_LINEAR)


    if len(target_shape) != len(res_image.shape):
        res_image = res_image[..., np.newaxis]

    res_height, res_width = res_image.shape[:2]

    # Padding dimensions are an upper bound for how much padding we need.
    # We add extra padding and then crop a target_shape image from the final result
    padding_height = max(0, math.floor((target_height - res_height)/2))
    padding_width = max(0, math.floor((target_width - res_width)/2))

    padded_img = np.zeros(target_shape)
    padded_img[padding_height:padding_height+res_height, padding_width:padding_width+res_width] = res_image

    img = scipy.ndimage.filters.gaussian_filter(padded_img, sigma=7)
    img = (img/np.max(img))*255

    return img

def main(input_base_dir, output_base_dir, dataset, split, model_base_dir, run, best, static, test_metrics, show, debug, clean):
    dataset_name = dataset
    input_dir = os.path.join(input_base_dir, dataset, 'tfr', split)

    if best:
        model_type = 'best'
        tags = [tf.saved_model.tag_constants.SERVING]
    else:
        model_type = 'latest'
        tags = [tf.saved_model.tag_constants.TRAINING]

    # Get model dir with highest index
    model_dir = os.path.join(model_base_dir, f'run-{run}', 'models', model_type, '*')
    model_dir = sorted(glob.glob(model_dir))[-1]

    if output_base_dir:
        output_dir = os.path.join(output_base_dir, f'run-{run}')

    # Set label shape according to params. None means use original image size
    label_shape = None
    if not test_metrics:
        if static:
            label_shape = (64, 80)
        else:
            label_shape = (128, 160)

    # Make output directory in case it doesn't exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    filenames = glob.glob(os.path.join(input_dir, '*.tfr'))
    dataset = data.get_dataset(filenames,
        batch_size=2,
        seq_length=5,
        feature_shape=(256, 320),
        label_shape=label_shape,
        train=False
    )
    iterator = dataset.make_one_shot_iterator()
    images, fixations, maps = iterator.get_next()

    with tf.Session() as sess:
        # Enable CLI debugging
        if debug:
            sess = tf_debug.LocalCLIDebugWrapperSession(sess)

        tf.saved_model.loader.load(sess, tags, model_dir)
        print(f"Finished loading model and variables from {model_dir}")
        g = tf.get_default_graph()

        if static:
            pred = g.get_tensor_by_name("attention/static_pred:0")
            type = 'static'
        else:
            pred = g.get_tensor_by_name("inference/dynamic_pred:0")
            type = 'dynamic'

        batch_size = g.get_tensor_by_name("batch_length:0")
        time_steps = g.get_tensor_by_name("time_steps:0")

        val_metrics = {
            'losses': [],
            'kl': [],
            'cc': [],
            'nss': [],
            'samples': 0
        }
        while True:
            try:

                images_np, fixations_np, maps_np = sess.run([images, fixations, maps])

                pred_np, n, t = \
                    sess.run(
                        fetches=[pred, batch_size, time_steps],
                        feed_dict={'images:0': images_np}
                    )

                if test_metrics:
                    new_pred = np.zeros(fixations_np.shape)
                    target_shape = fixations_np.shape[2:]
                    for i in range(pred_np.shape[0]):
                        for j in range(pred_np.shape[1]):
                            new_pred[i,j] = proc_predictions(pred_np[i,j], target_shape)
                    pred_np = new_pred

                pred_t = tf.placeholder(tf.float32, shape=pred_np.shape)
                fixation_t = tf.placeholder(tf.float32, shape=fixations_np.shape)
                map_t = tf.placeholder(tf.float32, shape=maps_np.shape)

                kl = util.kl_divergence(map_t, pred_t)
                cc = util.cc(map_t, pred_t)
                nss = util.nss(fixation_t, pred_t)
                loss = 10*kl - cc - nss

                val_loss, val_kl, val_cc, val_nss = \
                    sess.run(
                        fetches=[loss, kl, cc, nss],
                        feed_dict={
                            pred_t: pred_np,
                            fixation_t: fixations_np,
                            map_t: maps_np
                        }
                    )

                # Keep track of validation metrics
                val_metrics['losses'].append(val_loss*n)
                val_metrics['kl'].append(val_kl*n)
                val_metrics['cc'].append(val_cc*n)
                val_metrics['nss'].append(val_nss*n)
                val_metrics['samples'] += n*t

                print(f"Processed {val_metrics['samples']} samples    \r", end="")

                if show:
                    for i in range(pred_np.shape[0]):
                        for j in range(pred_np.shape[1]):
                            util.show_sample(
                                features=(images_np[i,j], fixations_np[i,j], maps_np[i,j]),
                                pred=pred_np[i,j]
                            )

            except tf.errors.OutOfRangeError:
                # Compute mean score over validation split
                loss = np.sum(val_metrics['losses'])/val_metrics['samples']
                kl = np.sum(val_metrics['kl'])/val_metrics['samples']
                cc = np.sum(val_metrics['cc'])/val_metrics['samples']
                nss = np.sum(val_metrics['nss'])/val_metrics['samples']

                out_str = \
                    f"Number of samples: {val_metrics['samples']}" + " "*30 + "\n"\
                    f"Input directory: {input_dir}\n"\
                    f"Model directory: {model_dir}\n"\
                    f"Test metrics: {test_metrics}\n"\
                    f"Static: {static}\n"\
                    f"Average loss: {loss}\n"\
                    f"Average KL-divergence: {kl}\n"\
                    f"Average CC: {cc}\n"\
                    f"Average NSS: {nss}\n"

                if output_dir:
                    if test_metrics:
                        metric_type = 'tm'
                    else:
                        metric_type = 'vm'
                    with open(os.path.join(output_dir, f'metrics-{dataset_name}-{split}-{metric_type}.txt'), 'w') as f:
                        f.write(out_str)

                print(out_str)
                break


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_base_dir', action='store', required=True,
                        help='Directory where datasets are located')
    parser.add_argument('-o', '--output_base_dir', action='store', required=False,
                        help='Base directory to store inference metric results')
    parser.add_argument('-d', '--dataset', action='store', required=True, default='dhf1k',
                        help="Dataset name to run inference on. Can be one of `dhf1k`, `ucf` or `hollywood`")
    parser.add_argument('-s', '--split', action='store', required=True, default='val',
                        help='What split of the dataset to run inference on')
    parser.add_argument('--run', action='store', type=int, required=True,
                        help='What training run should run inference for')
    parser.add_argument('--best', action='store_true', required=False,
                        help='Whether to evaluate on the best model or latest model of specified run')
    parser.add_argument('--static', action='store_true', required=False,
                        help='Whether to run inference for attention module predictions or final dynamic predictions')
    parser.add_argument('--test_metrics', action='store_true', required=False,
                        help='Flag to apply post processing to predictions before computing metrics')
    parser.add_argument('-m', '--model_base_dir', action='store', required=False, default='models',
                        help='Base directory to export and/or load saved model')
    parser.add_argument('--show', action='store_true', required=False,
                        help='Flag to display predictions as they are made')
    parser.add_argument('--debug', action='store_true', required=False,
                        help='Attach CLI tfdbg to session')
    parser.add_argument('--clean', action='store_true', required=False,
                        help='Purge models base export dir and logging dir')

    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    main(**kwargs)
