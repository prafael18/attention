import os
import glob
import shutil
import urllib.request

import numpy as np
import tensorflow as tf
from tensorflow.python.ops import rnn_cell_impl

import rnn
import data
import logger
from util import dependent_vars, time_distributed, kl_divergence, nss, cc

vgg16_weights_url = 'https://gitlab.com/prafael18/prafael18.gitlab.io/raw/master/assets/vgg16_conv_weights.npz?inline=false'
vgg16_weights_fn = 'vgg16_conv_weights.npz'

class ACLNet(object):
    def __init__(self, sess, model_dir, log_dir, dynamic_train_filenames, dynamic_val_filenames, static_train_filenames, static_val_filenames, train_config):
        # Start training from dynamic loss
        self._static = False

        # Keep some args
        self._sess = sess
        self._train_config = train_config
        self._model_dir = model_dir

        # Create a feedable iterator
        with tf.name_scope("data"):
            # Create placeholders for datasets
            self._seq_length = tf.placeholder(tf.int64, shape=[], name="seq_length")
            self._label_shape = tf.placeholder(tf.int32, shape=[2], name="label_shape")
            # Create dataset objects.
            dynamic_train_dataset = data.get_dataset(
                filenames=dynamic_train_filenames,
                batch_size=self._train_config['dynamic_batch_size'],
                seq_length=self._seq_length,
                feature_shape=self._train_config['feature_shape'],
                label_shape=self._label_shape,
                train=True
            )
            dynamic_val_dataset = data.get_dataset(
                filenames=dynamic_val_filenames,
                batch_size=self._train_config['dynamic_batch_size'],
                seq_length=self._seq_length,
                feature_shape=self._train_config['feature_shape'],
                label_shape=self._label_shape,
            )
            static_train_dataset = data.get_dataset(
                filenames=static_train_filenames,
                batch_size=self._train_config['static_batch_size'],
                seq_length=self._seq_length,
                feature_shape=self._train_config['feature_shape'],
                label_shape=self._label_shape,
                train=True
            )
            static_val_dataset = data.get_dataset(
                filenames=static_val_filenames,
                batch_size=self._train_config['static_batch_size'],
                seq_length=self._seq_length,
                feature_shape=self._train_config['feature_shape'],
                label_shape=self._label_shape
            )

            self._handle = tf.placeholder(tf.string, shape=[], name="handle")
            iterator = tf.data.Iterator.from_string_handle(
                self._handle, dynamic_train_dataset.output_types, dynamic_train_dataset.output_shapes)

            # Get initializable iterators from datasets
            dynamic_train_iterator = dynamic_train_dataset.make_initializable_iterator()
            dynamic_val_iterator = dynamic_val_dataset.make_initializable_iterator()
            static_train_iterator = static_train_dataset.make_initializable_iterator()
            static_val_iterator = static_val_dataset.make_initializable_iterator()

            # Set init ops as attributes for easy recovery afterwards
            self._dynamic_train_iterator_init = dynamic_train_iterator.initializer
            self._dynamic_val_iterator_init = dynamic_val_iterator.initializer
            self._static_train_iterator_init = static_train_iterator.initializer
            self._static_val_iterator_init = static_val_iterator.initializer

            # The `Iterator.string_handle()` method returns a tensor that can be evaluated
            # and used to feed the `handle` placeholder.
            self._dynamic_train_handle = sess.run(dynamic_train_iterator.string_handle(name="dynamic_train_handle"))
            self._dynamic_val_handle = sess.run(dynamic_val_iterator.string_handle(name="dynamic_val_handle"))
            self._static_train_handle = sess.run(static_train_iterator.string_handle(name="static_train_handle"))
            self._static_val_handle = sess.run(static_val_iterator.string_handle(name="static_val_handle"))

        # Get symbolic tensors for input features and output labels
        images, fixations, maps = iterator.get_next()
        self._images = tf.identity(images, name="images")
        self._fixations = tf.identity(fixations, name="fixations")
        self._maps = tf.identity(maps, name="maps")

        # Keep track of batch size for metric computation
        input_shape = tf.shape(self._images)
        self._batch_size = tf.identity(input_shape[0], name="batch_length")
        self._time_steps = tf.identity(input_shape[1], name="time_steps")

        with tf.variable_scope("vgg16"):
            weights = self._load_vgg_weights()
            vgg = self._vgg_16(self._images, weights)
        with tf.variable_scope("attention"):
            res, att = self._attention(vgg)
            att /= tf.reduce_max(att, axis=[2,3,4], keepdims=True)
            self._static_pred = tf.identity(att, name='static_pred')
        with tf.variable_scope("conv_lstm"):
            res = tf.reshape(res, (-1, self._train_config["dynamic_seq_length"]) + tuple(res.shape[2:]))
            lstm_inputs = tf.unstack(res, axis=1)
            self._dropout_keep_prob = tf.placeholder_with_default(1.0, shape=[], name="dropout_keep_prob")
            lstm_cell = rnn.Conv2DLSTMCell(
                input_shape = res.shape[2:],
                filters=256,
                kernel_size=[3,3],
                strides=[1,1],
                padding='same',
                input_dropout_keep_prob=self._dropout_keep_prob,
                name='conv_lstm_cell'
            )
            lstm_out, final_state = tf.nn.static_rnn(lstm_cell, lstm_inputs, dtype=tf.float32, scope='static_rnn')
            lstm_out = tf.stack(lstm_out, axis=1)
        with tf.variable_scope("inference"):
            pred = self._inference(lstm_out)
            pred /= tf.reduce_max(pred, axis=[2,3,4], keepdims=True)
            self._dynamic_pred = tf.identity(pred, name='dynamic_pred')

        # Create optimizer object
        self._global_step = tf.get_variable("global_step", initializer=0, trainable=False)
        self._learning_rate = tf.placeholder(dtype=tf.float32, shape=[], name="learning_rate")

        with tf.name_scope("static"):
            with tf.name_scope("metrics"):
                self._static_kl = kl_divergence(self._maps, self._static_pred)
                self._static_cc = cc(self._maps, self._static_pred)
                self._static_nss = nss(self._fixations, self._static_pred)
            self._static_loss = tf.add_n(
                inputs=[
                    self._train_config['kl_mult']*self._static_kl,
                    -self._train_config['cc_mult']*self._static_cc,
                    -self._train_config['nss_mult']*self._static_nss
                ],
                name="loss"
            )
            static_opt = tf.train.AdamOptimizer(learning_rate=self._learning_rate, name="adam_optimizer")
            static_vars = dependent_vars(self._static_loss.op)
            static_gradients = tf.gradients(self._static_loss, static_vars)
            self._static_train_op = static_opt.apply_gradients(
                grads_and_vars=zip(static_gradients, static_vars),
                global_step=self._global_step,
                name="train"
            )

        with tf.name_scope("dynamic"):
            with tf.name_scope("metrics"):
                self._dynamic_kl = kl_divergence(self._maps, self._dynamic_pred)
                self._dynamic_cc = cc(self._maps, self._dynamic_pred)
                self._dynamic_nss = nss(self._fixations, self._dynamic_pred)
            self._dynamic_loss = tf.add_n(
                inputs=[
                    self._train_config['kl_mult']*self._dynamic_kl,
                    -self._train_config['cc_mult']*self._dynamic_cc,
                    -self._train_config['nss_mult']*self._dynamic_nss
                ],
                name="loss"
            )
            dynamic_opt = tf.train.AdamOptimizer(learning_rate=self._learning_rate, name="adam_optimizer")
            dynamic_vars = dependent_vars(self._dynamic_loss.op)
            dynamic_gradients = tf.gradients(self._dynamic_loss, dynamic_vars)
            self._dynamic_train_op = dynamic_opt.apply_gradients(
                grads_and_vars=zip(dynamic_gradients, dynamic_vars),
                global_step=self._global_step,
                name="train"
            )

        self._nr_params = int(np.sum([np.prod(v.shape) for v in tf.trainable_variables()]))
        self._logger = logger.Logger(log_dir, sess.graph)

        # Init best validation loss
        self._best_val_loss_var = tf.Variable(np.inf, dtype=tf.float32, name="best_val_loss")
        self._best_val_loss_value = tf.placeholder(dtype=tf.float32, shape=[], name="best_val_loss/value")
        self._best_val_loss_assign = self._best_val_loss_var.assign(self._best_val_loss_value, name="best_val_loss/assign")
        self._best_val_loss = None

        # Init best validation loss
        self._epoch_var = tf.Variable(0, dtype=tf.int32, name="epoch")
        self._epoch_add = self._epoch_var.assign_add(1, name="epoch/assign_add")
        self._epoch = None

    @time_distributed
    def _inference(self, inputs):
        conv = self.conv2d(inputs,
            filters=1,
            kernel_size=[1,1],
            strides=[1,1],
            padding='same',
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            activation=tf.nn.sigmoid,
            name='conv')
        out = self.upsampling2d(conv,
            scale=4,
            name='upsampling')
        return out

    @time_distributed
    def _attention(self, inputs):
        maxpool1 = self.maxpool2d(inputs,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool1')
        conv1_1 = self.conv2d(maxpool1,
            filters=64,
            kernel_size=[1,1],
            strides=[1,1],
            padding='same',
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name='conv1_1')
        conv1_2 = self.conv2d(conv1_1,
            filters=128,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name='conv1_2')
        maxpool2 = self.maxpool2d(conv1_2,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool2')
        conv2_1 = self.conv2d(maxpool2,
            filters=64,
            kernel_size=[1,1],
            strides=[1,1],
            padding='same',
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name='conv2_1')
        conv2_2 = self.conv2d(conv2_1,
            filters=128,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name='conv2_2')
        conv2_3 = self.conv2d(conv2_2,
            filters=1,
            kernel_size=[1,1],
            strides=[1,1],
            padding='same',
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            activation=tf.nn.sigmoid,
            name='conv2_3')
        up1 = self.upsampling2d(conv2_3,
            scale=4,
            name='upsampling')
        att = self.upsampling2d(up1,
            scale=2,
            name='upsampling')
        mask = tf.broadcast_to(up1,
            shape=[tf.shape(up1)[0], up1.shape[1], up1.shape[2], 512],
            name='broadcast')
        out = tf.add(inputs, inputs*mask)       # Residual layer
        return out, att

    @time_distributed
    def _vgg_16(self, inputs, weights):
        conv1_1 = self.conv2d(inputs,
            filters=64,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv1_1_k'],
            bias_initializer=weights['conv1_1_b'],
            name='conv1_1')
        conv1_2 = self.conv2d(conv1_1,
            filters=64,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv1_2_k'],
            bias_initializer=weights['conv1_2_b'],
            activation=tf.nn.relu,
            name='conv1_2')
        maxpool1 = self.maxpool2d(conv1_2,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool1')
        conv2_1 = self.conv2d(maxpool1,
            filters=128,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv2_1_k'],
            bias_initializer=weights['conv2_1_b'],
            name='conv2_1')
        conv2_2 = self.conv2d(conv2_1,
            filters=128,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv2_2_k'],
            bias_initializer=weights['conv2_2_b'],
            activation=tf.nn.relu,
            name='conv2_2')
        maxpool2 = self.maxpool2d(conv2_2,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool2')
        conv3_1 = self.conv2d(maxpool2,
            filters=256,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv3_1_k'],
            bias_initializer=weights['conv3_1_b'],
            name='conv3_1')
        conv3_2 = self.conv2d(conv3_1,
            filters=256,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv3_2_k'],
            bias_initializer=weights['conv3_2_b'],
            name='conv3_2')
        conv3_3 = self.conv2d(conv3_2,
            filters=256,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv3_3_k'],
            bias_initializer=weights['conv3_3_b'],
            name='conv3_3')
        maxpool3 = self.maxpool2d(conv3_3,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool3')
        conv4_1 = self.conv2d(maxpool3,
            filters=512,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv4_1_k'],
            bias_initializer=weights['conv4_1_b'],
            name='conv4_1')
        conv4_2 = self.conv2d(conv4_1,
            filters=512,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv4_2_k'],
            bias_initializer=weights['conv4_2_b'],
            name='conv4_2')
        conv4_3 = self.conv2d(conv4_2,
            filters=512,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv4_3_k'],
            bias_initializer=weights['conv4_3_b'],
            name='conv4_3')
        maxpool4 = self.maxpool2d(conv4_3,  #According to the paper, this maxpool layer was removed altogether
            ksize=[2,2],
            strides=[1,1],
            padding='same',
            name='pool4')
        conv5_1 = self.conv2d(maxpool4,
            filters=512,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv5_1_k'],
            bias_initializer=weights['conv5_1_b'],
            name='conv5_1')
        conv5_2 = self.conv2d(conv5_1,
            filters=512,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv5_2_k'],
            bias_initializer=weights['conv5_2_b'],
            name='conv5_2')
        conv5_3 = self.conv2d(conv5_2,
            filters=512,
            kernel_size=[3,3],
            strides=[1,1],
            padding='same',
            kernel_initializer=weights['conv5_3_k'],
            bias_initializer=weights['conv5_3_b'],
            name='conv5_3')
        return conv5_3

    def upsampling2d(self, inputs, scale, name=None):
        return tf.image.resize_nearest_neighbor(inputs,
            size=[inputs.shape[1]*scale, inputs.shape[2]*scale],
            name=name)

    def maxpool2d(self, inputs, ksize, strides, padding, name=None):
        return tf.nn.max_pool(
            value=inputs,
            ksize=[1, ksize[0], ksize[1], 1],
            strides=[1, strides[0], strides[1], 1],
            padding=padding.upper(),
            name=name)

    def conv2d(self, inputs, filters, kernel_size,
               strides, padding,
               kernel_initializer, bias_initializer,
               kernel_regularizer=None, bias_regularizer=None,
               activation=tf.nn.relu, name=None):

        # Set kernel shape to None if initializer is a constant due to tf ValueError
        if type(kernel_initializer) is np.ndarray:
            kernel_shape = None
        else:
            kernel_shape = (kernel_size[0], kernel_size[1], inputs.shape[-1], filters)

        if type(bias_initializer) is np.ndarray:
            bias_shape = None
        else:
            bias_shape = (filters,)

        with tf.variable_scope(name):
            kernel = tf.get_variable(
                name="kernel",
                shape=kernel_shape,
                dtype=tf.float32,
                initializer=kernel_initializer,
                regularizer=kernel_regularizer,
                trainable=True,
            )
            bias = tf.get_variable(
                name="bias",
                shape=bias_shape,
                dtype=tf.float32,
                initializer=bias_initializer,
                regularizer=bias_regularizer,
                trainable=True
            )
            conv = tf.nn.conv2d(inputs, kernel, [1, strides[0], strides[1], 1], padding.upper())
            out = activation(tf.nn.bias_add(conv, bias))
            return out

    def save(self, epoch, training, overwrite=True):
        if training:
            base_export_dir = os.path.join(self._model_dir, 'latest')
            tags = [tf.saved_model.tag_constants.TRAINING]
        else:
            base_export_dir = os.path.join(self._model_dir, 'best')
            tags = [tf.saved_model.tag_constants.SERVING]

        export_dir = os.path.join(base_export_dir, f'{epoch:02d}')

        builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
        builder.add_meta_graph_and_variables(self._sess, tags=tags)
        builder.save()

        # Get dir path with second highest index to overwrite. We overwrite after
        # saving to guarantee that we won't be left with an incomplete model if
        # there's a power failure. When saving test model, we always overwrite
        if overwrite or not training:
            export_dirs = sorted(glob.glob(os.path.join(base_export_dir, "*")))
            if len(export_dirs) >= 2:
                newest_export_dir = export_dirs[-2]
                shutil.rmtree(newest_export_dir)

        print(f"Saved model to {export_dir}" + " "*30)

    def load(self, training):
        if training:
            base_export_dir = os.path.join(self._model_dir, 'latest')
            tags = [tf.saved_model.tag_constants.TRAINING]
        else:
            base_export_dir = os.path.join(self._model_dir, 'best')
            tags = [tf.saved_model.tag_constants.SERVING]

        export_dirs = sorted(glob.glob(os.path.join(base_export_dir, "*")))
        if export_dirs:
            export_dir = export_dirs[-1]
        else:
            print("No SavedModel available to restore.")
            raise ValueError

        saver = tf.train.Saver()
        saver.restore(self._sess, f"{export_dir}/variables/variables")
        print(f"Loaded variables from {export_dir}")

    def _load_vgg_weights(self):
        global vgg16_weights_url, vgg16_weights_fn

        assets_dir = os.path.join(os.getcwd(), 'assets')
        if not os.path.isdir(assets_dir):
            os.makedirs(assets_dir)
        if vgg16_weights_fn not in os.listdir(assets_dir):
            response = urllib.request.urlopen(vgg16_weights_url)
            print(f"Downloading VGG-16 weights from {vgg16_weights_url}...")
            data = response.read()
            print(f"Finished downloading VGG-16 weights {' '*20}")
            with open(os.path.join(assets_dir, vgg16_weights_fn), 'wb') as f:
                f.write(data)
        weights = np.load(os.path.join(assets_dir, vgg16_weights_fn))
        return weights

    def log_scalar(self, tag, value, step):
        self._logger.log_scalar(tag, value, step)

    def log_images(self, tag, images, step):
        self._logger.log_images(tag, images, step)

    def initialize_iterator(self, static, train=False):
        if static:
            if train:
                iterator_init = self._static_train_iterator_init
            else:
                iterator_init = self._static_val_iterator_init
            time_steps = self._train_config['static_seq_length']
            label_shape = self._train_config['static_label_shape']
        else:
            if train:
                iterator_init = self._dynamic_train_iterator_init
            else:
                iterator_init = self._dynamic_val_iterator_init
            time_steps = self._train_config['dynamic_seq_length']
            label_shape = self._train_config['dynamic_label_shape']


        self._sess.run(iterator_init,
            feed_dict={
                self._seq_length: time_steps,
                self._label_shape: label_shape
            })

    @property
    def static(self):
        # This property determines what mode we'll run the network (static or dynamic)
        return self._static

    @property
    def epoch(self):
        if self._epoch:
            return self._epoch
        self._epoch = self._sess.run(self._epoch_var)
        return self._epoch

    @epoch.setter
    def epoch(self, value):
        # Ignore value and increment by one
        self._epoch = self._sess.run(self._epoch_add)

    @static.setter
    def static(self, val):
        self._static = val

    @property
    def dropout_keep_prob(self):
        return self._dropout_keep_prob

    @property
    def handle(self):
        return self._handle

    @property
    def train_iterator(self):
        if self._static:
            return self._static_train_iterator
        return self._dynamic_train_iterator

    @property
    def val_iterator(self):
        if self._static:
            return self._static_val_iterator
        return self._dynamic_val_iterator

    @property
    def train_handle(self):
        if self._static:
            return self._static_train_handle
        return self._dynamic_train_handle

    @property
    def val_handle(self):
        if self._static:
            return self._static_val_handle
        return self._dynamic_val_handle

    @property
    def best_val_loss(self):
        if self._best_val_loss:
            return self._best_val_loss
        self._best_val_loss = self._sess.run(self._best_val_loss_var)
        return self._best_val_loss

    @best_val_loss.setter
    def best_val_loss(self, value):
        self._best_val_loss = value
        self._sess.run(
            fetches=self._best_val_loss_assign,
            feed_dict={self._best_val_loss_value: self._best_val_loss}
        )

    @property
    def images(self):
        return self._images

    @property
    def fixations(self):
        return self._fixations

    @property
    def maps(self):
        return self._maps

    @property
    def batch_size(self):
        return self._batch_size

    @property
    def time_steps(self):
        return self._time_steps

    @property
    def train_op(self):
        if self._static:
            return self._static_train_op
        return self._dynamic_train_op

    @property
    def nr_params(self):
        return self._nr_params

    @property
    def global_step(self):
        return self._global_step

    @property
    def learning_rate(self):
        return self._learning_rate

    @property
    def cc(self):
        if self._static:
            return self._static_cc
        return self._dynamic_cc

    @property
    def kl_divergence(self):
        if self._static:
            return self._static_kl
        return self._dynamic_kl

    @property
    def nss(self):
        if self._static:
            return self._static_nss
        return self._dynamic_nss

    @property
    def loss(self):
        if self._static:
            return self._static_loss
        return self._dynamic_loss

    @property
    def pred(self):
        if self._static:
            return self._static_pred
        return self._dynamic_pred
