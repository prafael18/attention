import os
import glob
import argparse
import subprocess

import numpy as np
import tensorflow as tf
from tensorflow.python import debug as tf_debug

import util
import network

train_config = {
    'feature_shape': (256, 320),
    'static_batch_size': 20,
    'static_seq_length': 1,
    'static_label_shape': (64, 80),
    'dynamic_batch_size': 2,
    'dynamic_seq_length': 5,
    'dynamic_label_shape': (128, 160),
    # 'learning_rate': [1e-4, 1e-4, 1e-4, 1e-5, 1e-5, 1e-5, 1e-6, 1e-6, 1e-7, 1e-7],
    'learning_rate': [1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-5, 1e-5, 1e-5, 1e-5, 1e-5],
    'kl_mult': 10,
    'nss_mult': 1,
    'cc_mult': 2,
    'dropout_keep_prob': 0.6
}

def main(base_dir, model_dir, log_dir, total_epochs, epochs_per_val,
         epochs_per_chkpt, datasets, resume_training, debug, clean):

    assert len(datasets) > 0  # At least one dataset was specified
    assert total_epochs == len(train_config['learning_rate'])

    # Clean directories only in case we're not resuming training
    if clean and not resume_training:
        util.clean_dirs([log_dir, model_dir])

    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Write current git HEAD's SHA to log dir
    with open(os.path.join(log_dir, 'commit-sha.txt'), 'a') as f:
        sha = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip()
        f.write(sha.decode('ascii')+"\n")

    static_base_dir = os.path.join(base_dir, 'salicon', 'tfr')
    static_train_filenames = glob.glob(os.path.join(static_base_dir, 'train', '*'))
    static_val_filenames = glob.glob(os.path.join(static_base_dir, 'val', '*'))

    dynamic_train_filenames, dynamic_val_filenames = [], []
    for ds in datasets:
        dynamic_base_dir = os.path.join(base_dir, ds, 'tfr')
        dynamic_train_filenames += glob.glob(os.path.join(dynamic_base_dir, 'train', '*'))
        dynamic_val_filenames += glob.glob(os.path.join(dynamic_base_dir, 'val', '*'))

    if len(dynamic_train_filenames) == 0 or len(dynamic_val_filenames) == 0:
        print(f"No dynamic training or validation data found in {dynamic_base_dir}")
        return
    if len(static_train_filenames) == 0 or len(static_val_filenames) == 0:
        print(f"No static training or validation data found in {static_base_dir}")
        return

    with tf.Session() as sess:
        # Enable CLI debugging
        if debug:
            sess = tf_debug.LocalCLIDebugWrapperSession(sess)

        # Instantiate model
        net = network.ACLNet(sess, model_dir, log_dir, dynamic_train_filenames, dynamic_val_filenames, static_train_filenames,
         static_val_filenames, train_config)

        print(f"Instantiated ACLNet with {net.nr_params} parameters")

        # Initialize variables
        if resume_training:
            net.load(training=True)
        else:
            sess.run(tf.global_variables_initializer())

        # Get initial global step
        step = sess.run(net.global_step)

        while net.epoch < total_epochs:

            # Initialize static and dynamic training iterators
            net.initialize_iterator(net.static, train=True)
            net.initialize_iterator(not net.static, train=True)

            train_metrics = {
                'losses': [],
                'kl': [],
                'cc': [],
                'nss': [],
                'samples': 0
            }

            # We need to keep track of what iterator finished first
            finished_iter = -1

            while True:
                try:
                    if step%2 == 0:
                        net.static = False
                    else:
                        net.static = True

                    feed_dict = {
                        net.learning_rate: train_config['learning_rate'][net.epoch],
                        net.handle: net.train_handle,
                        net.dropout_keep_prob: train_config['dropout_keep_prob']
                    }
                    _, loss, kl, cc, nss, n, t, features = sess.run(
                        [net.train_op, net.loss, net.kl_divergence, net.cc, net.nss, net.batch_size, net.time_steps,
                        (net.images, net.fixations, net.maps, net.pred)],
                        feed_dict=feed_dict
                    )

                    # Keep track of validation metrics
                    train_metrics['losses'].append(loss*n)
                    train_metrics['kl'].append(kl*n)
                    train_metrics['cc'].append(cc*n)
                    train_metrics['nss'].append(nss*n)
                    train_metrics['samples'] += n*t

                    step = sess.run(net.global_step)

                    # Log metrics and loss
                    if net.static:
                        net.log_scalar('train/batch/static/loss', loss, step)
                        net.log_scalar('train/batch/static/kl', kl, step)
                        net.log_scalar('train/batch/static/cc', cc, step)
                        net.log_scalar('train/batch/static/nss', nss, step)
                    else:
                        net.log_scalar('train/batch/dynamic/loss', loss, step)
                        net.log_scalar('train/batch/dynamic/kl', kl, step)
                        net.log_scalar('train/batch/dynamic/cc', cc, step)
                        net.log_scalar('train/batch/dynamic/nss', nss, step)

                    print(f"\rStep {step}: loss={loss:.3f}, kl={kl:.3f}, cc={cc:.3f}, nss={nss:.3f}" + " "*10, end=" ")

                except tf.errors.OutOfRangeError:
                    # We only declare an epoch finished once both iterators have been fully iterated
                    # at least once. We keep track of what iterator finished first and keep reinitializing it
                    # in case the other one hasn't finished yet.
                    if finished_iter == -1 or finished_iter == int(net.static):
                        finished_iter = int(net.static)
                        net.initialize_iterator(net.static, train=True)
                        continue

                    # Increment epoch counter
                    net.epoch += 1

                    # Log one train video or image, depending on what iterator finished last
                    images, fixations, maps, pred = features
                    images += [103.939, 116.779, 123.680]    # Mean addition
                    images = images[..., ::-1]
                    net.log_images('train/inputs', images[0], step)
                    net.log_images('train/fixations', fixations[0]*255, step)
                    net.log_images('train/maps', maps[0]*255, step)
                    net.log_images('train/pred', pred[0]*255, step)

                    # Compute mean scores over train split
                    loss = np.sum(train_metrics['losses'])/train_metrics['samples']
                    kl = np.sum(train_metrics['kl'])/train_metrics['samples']
                    cc = np.sum(train_metrics['cc'])/train_metrics['samples']
                    nss = np.sum(train_metrics['nss'])/train_metrics['samples']

                    net.log_scalar('train/epoch/loss', loss, step)
                    net.log_scalar('train/epoch/kl', kl, step)
                    net.log_scalar('train/epoch/cc', cc, step)
                    net.log_scalar('train/epoch/nss', nss, step)

                    print(f"\rFinished epoch {net.epoch}: "
                          f"loss={loss:.3f}, kl={kl:.3f}, cc={cc:.3f}, nss={nss:.3f}")
                    break

            if (net.epoch)%epochs_per_chkpt == 0:
                net.save(net.epoch, training=True)

            if (net.epoch)%epochs_per_val == 0:
                # Initialize training iterator
                net.static = False
                net.initialize_iterator(net.static, train=False)

                feed_dict = {
                    net.handle: net.val_handle
                }
                val_metrics = {
                    'losses': [],
                    'kl': [],
                    'cc': [],
                    'nss': [],
                    'samples': 0
                }
                while True:
                    try:
                        loss, kl, cc, nss, n, t, features = sess.run(
                            [net.loss, net.kl_divergence, net.cc, net.nss, net.batch_size, net.time_steps,
                            (net.images, net.fixations, net.maps, net.pred)],
                            feed_dict=feed_dict
                        )
                        # Keep track of validation metrics
                        val_metrics['losses'].append(loss*n)
                        val_metrics['kl'].append(kl*n)
                        val_metrics['cc'].append(cc*n)
                        val_metrics['nss'].append(nss*n)
                        val_metrics['samples'] += n*t

                    except tf.errors.OutOfRangeError:
                        images, fixations, maps, pred = features
                        images += [103.939, 116.779, 123.680]    # Mean addition
                        images = images[..., ::-1]
                        if net.epoch == 1:
                            net.log_images('val/inputs', images[0], step)
                            net.log_images('val/fixations', fixations[0]*255, step)
                            net.log_images('val/maps', maps[0]*255, step)
                        net.log_images('val/pred', (pred[0]/np.max(pred[0]))*255, step)

                        # Compute mean score over validation split
                        loss = np.sum(val_metrics['losses'])/val_metrics['samples']
                        kl = np.sum(val_metrics['kl'])/val_metrics['samples']
                        cc = np.sum(val_metrics['cc'])/val_metrics['samples']
                        nss = np.sum(val_metrics['nss'])/val_metrics['samples']

                        val_str = f"Finished validating: "\
                                  f"loss={loss:.3f}, kl={kl:.3f}, cc={cc:.3f}, nss={nss:.3f}"

                        # Log validation scores
                        net.log_scalar('val/loss', loss, step)
                        net.log_scalar('val/kl', kl, step)
                        net.log_scalar('val/cc', cc, step)
                        net.log_scalar('val/nss', nss, step)

                        if loss < net.best_val_loss:
                            net.best_val_loss = loss
                            net.save(net.epoch, training=False)
                            val_str += " (new_best)!"

                        print(val_str)
                        break


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--base_dir', action='store', required=True,
                        help='Base dataset directory where all datasets are located')
    parser.add_argument('-m', '--model_dir', action='store', required=False, default='models',
                        help='Base directory to export and/or load saved model')
    parser.add_argument('-l', '--log_dir', action='store', required=False, default='logs',
                        help='Base directory to log training progress')
    parser.add_argument('-t', '--total_epochs', action='store', type=int, required=False, default=10,
                        help='Number of steps to train model')
    parser.add_argument('-v', '--epochs_per_val', action='store', type=int, required=False, default=1,
                        help='Number of steps before computing loss on validation data')
    parser.add_argument('-c', '--epochs_per_chkpt', action='store', type=int, required=False, default=1,
                        help='Number of steps before saving checkpoint')
    parser.add_argument('-r', '--resume_training', action='store_true', required=False,
                        help='Flag to resume training from saved checkpoint in `export_dir`')
    parser.add_argument('--debug', action='store_true', required=False,
                        help='Attach CLI tfdbg to session')
    parser.add_argument('--clean', action='store_true', required=False,
                        help='Purge models base export dir and logging dir')
    parser.add_argument('--dhf1k', dest='datasets', action='append_const', const='dhf1k',
                        help='Add dhf1k videos to dynamic training set')
    parser.add_argument('--ucf', dest='datasets', action='append_const', const='ucf',
                        help='Add ucf videos to dynamic training set')
    parser.add_argument('--hollywood', dest='datasets', action='append_const', const='hollywood',
                        help='Add hollywood videos to dynamic training set')

    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    net = main(**kwargs)
