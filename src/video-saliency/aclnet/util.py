import os
import glob
import shutil
import collections

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# Epsilon fuzz factor
epsilon = 1e-7
# epsilon = 2.2204e-16

def dependent_vars(starting_op):
    op_to_var = {var.op: var for var in tf.trainable_variables()}

    dependent_vars = []

    queue = collections.deque()
    queue.append(starting_op)

    visited = set([starting_op])

    while queue:
      op = queue.popleft()
      try:
        dependent_vars.append(op_to_var[op])
      except KeyError:
        # `op` is not a variable, so search its inputs (if any).
        for op_input in op.inputs:
          if op_input.op not in visited:
            queue.append(op_input.op)
            visited.add(op_input.op)
    return dependent_vars

# Wrapper to apply 2D operations across time-series dimension
def time_distributed(func):
    def wrapper_time_distributed(*args, **kwargs):
        inputs = args[1]  # arg[0] is the object self
        seq_length = tf.shape(inputs)[1]
        inputs = tf.reshape(inputs, (-1,) + tuple(inputs.shape[2:]))
        outs = func(args[0], inputs, *(args[2:]), **kwargs)
        if type(outs) is tuple:
            outs_list = [tf.reshape(out, (-1, seq_length) + tuple(out.shape[1:])) for out in outs]
            outs = tuple(outs_list)
        else:
            outs = tf.reshape(outs, (-1, seq_length) + tuple(outs.shape[1:]))
        return outs
    return wrapper_time_distributed

def reduce_std(input_tensor, axis=None, keepdims=False, name=None):
    name = name if name else "reduce_std"
    with tf.name_scope(name):
        means = tf.reduce_mean(input_tensor, axis=axis, keepdims=True)
        squared_deviations = tf.math.square(input_tensor - means)
        variance = tf.reduce_mean(squared_deviations, axis=axis, keepdims=keepdims)
        return tf.math.sqrt(variance)

# Saliency metrics
def kl_divergence(labels, predictions):
    # ACLNet's kl_div normalizes predictions between 0 and 1 at the beginning and multiplies
    # final loss for each frame by a mask that is 1 only when the max value of the label frame
    # is greater than 0.1

    with tf.name_scope("kl_div"):
        # One of the `problems` of using this metric is that the predicted maps won't learn the scale of the
        # true fixations, since they are normalized to sum to 1.

        # Axes to use with reduce ops
        axes = [2,3,4]
        # Normalize maps to sum to 1
        labels_sum = tf.reduce_sum(labels, axis=axes, keepdims=True)
        labels_denom = tf.where(
            condition=tf.equal(labels_sum, 0),
            x=tf.ones_like(labels_sum),
            y=labels_sum
        )
        labels = labels/labels_denom

        preds_sum = tf.reduce_sum(predictions, axis=axes, keepdims=True)
        preds_denom = tf.where(
            condition=tf.equal(preds_sum, 0),
            x=tf.ones_like(preds_sum),
            y=preds_sum
        )
        preds = predictions/preds_denom

        # Compute kl div for each frame
        kl = labels*tf.log(epsilon + labels/(epsilon + preds))
        kl = tf.reduce_sum(kl, axis=axes)
        # Add scores acrosse time_seq dimension
        kl = tf.reduce_sum(kl, axis=1)
        # Compute final score for batch
        kl = tf.reduce_mean(kl, name="value")
        return kl

def nss(labels, predictions):
    # They use a negative NSS for the loss, but NSS is a similarity metric so its loss should
    # be negative
    with tf.name_scope("nss"):
        # Axes to use with reduce ops
        axes = [2,3,4]
        # Normalize predictions with 0 mean and standard deviation 1
        map = predictions - tf.reduce_mean(predictions, axis=axes, keepdims=True)
        map = map/(epsilon + reduce_std(predictions, axis=axes, keepdims=True))
        # Cast to logical array and then back to numerical type in order to perform sum
        labels = tf.cast(labels, dtype=tf.bool)
        labels = tf.cast(labels, dtype=tf.float32)
        # Compute nss scores for each frame
        nss = tf.reduce_sum(map*labels, axis=axes)/(epsilon + tf.reduce_sum(labels, axis=axes))
        # Add scores acrosse time_seq dimension
        nss = tf.reduce_sum(nss, axis=1)
        # Compute final score for batch
        nss = tf.reduce_mean(nss, name="value")
        return nss

def cc(labels, predictions):
    # CC uses the same kind of [0,1] normalization for predictions and the boolean mask for the loss but
    # strangely, they use a sum to 1 normalization instead of mean subtraction and dividiosn by standard deviation
    with tf.name_scope("cc"):
        # Axes to use with reduce ops
        axes = [2,3,4]
        # Normalize maps to have 0 mean and stdev 1
        _labels = labels - tf.reduce_mean(labels, axis=axes, keepdims=True)
        labels = _labels/(epsilon + reduce_std(labels, axis=axes, keepdims=True))
        preds = predictions - tf.reduce_mean(predictions, axis=axes, keepdims=True)
        preds = preds/(epsilon + reduce_std(predictions, axis=axes, keepdims=True))
        # Subtract mean (is it really necessary if images already have 0 mean?)
        l = labels - tf.reduce_mean(labels, axis=axes, keepdims=True)
        p = preds - tf.reduce_mean(preds, axis=axes, keepdims=True)
        # Compute metric score for each frame
        cc = tf.reduce_sum(l*p, axis=axes)
        cc = cc/(epsilon + tf.math.sqrt(tf.reduce_sum(l*l, axis=axes)*tf.reduce_sum(p*p, axis=axes)))
        # Add scores acrosse time_seq dimension
        cc = tf.reduce_sum(cc, axis=1)
        # Compute final score for batch
        cc = tf.reduce_mean(cc, name="value")
        return cc

def show_sample(features, pred):
    image, fixation, map = features

    # Pre-process network outputs
    image += [103.939, 116.779, 123.680]    # Mean addition
    image = image[..., ::-1]                # BGR -> RGB
    image = image.astype(np.uint8)

    map = np.squeeze(map)                   # Remove channel dim
    pred = np.squeeze(pred)                 # Remove channel dim
    fixation = np.squeeze(fixation)         # Remove channel dim

    fig = plt.figure(num='Features', figsize=(10,10))
    fig.clear()
    axs = fig.subplots(nrows=2, ncols=3)
    axs[0,0].imshow(pred, cmap='gray')
    axs[0,1].imshow(map, cmap='gray')
    axs[0,2].imshow(fixation, cmap='gray')
    # gs = axs[1,0].get_subplotspec().get_gridspec()
    axs[1,0].remove()
    axs[1,2].remove()
    # axbig = fig.add_subplot(gs[1,:])
    # axbig.imshow(image)
    axs[1,1].imshow(image)
    return fig.waitforbuttonpress()

def clean_dirs(directories):
    for dir in directories:
        files = glob.glob(os.path.join(dir, "*"))
        for f in files:
            if os.path.isdir(f):
                shutil.rmtree(f)
            else:
                os.remove(f)
    print(f"Cleaned all files and directories from {directories}")
