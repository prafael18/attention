import numbers

import tensorflow as tf

from tensorflow.python.ops import rnn_cell_impl
from tensorflow.python.ops import init_ops
from tensorflow.python.keras import activations
from tensorflow.python.keras import initializers
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.utils import tf_utils

from tensorflow.python.framework import tensor_shape

class Conv2DLSTMCell(rnn_cell_impl.RNNCell):

    def __init__(self,
        input_shape,    # Input shape excluding batch size
        filters,
        kernel_size,
        strides=[1,1],
        padding='same',
        activation='tanh',
        recurrent_activation='sigmoid',
        use_bias=True,
        kernel_initializer='glorot_uniform',
        recurrent_initializer='orthogonal',
        bias_initializer='zeros',
        kernel_regularizer=None,
        recurrent_regularizer=None,
        bias_regularizer=None,
        unit_forget_bias=True,
        input_dropout_keep_prob=1.0,
        recurrent_dropout_keep_prob=1.0,
        output_dropout_keep_prob=1.0,
        name='convlstm_cell'):

        super(Conv2DLSTMCell, self).__init__(name=name)
        self._input_shape = tuple(input_shape)
        self._filters = filters
        self._kernel_size = tuple(kernel_size)
        self._strides = [1, strides[0], strides[1], 1]
        self._padding = padding.upper()

        self._activation = activations.get(activation)
        self._recurrent_activation = activations.get(recurrent_activation)
        self._use_bias = use_bias

        self._kernel_initializer = initializers.get(kernel_initializer)
        self._recurrent_initializer = initializers.get(recurrent_initializer)
        self._bias_initializer = initializers.get(bias_initializer)
        self._unit_forget_bias = unit_forget_bias

        self._kernel_regularizer = regularizers.get(kernel_regularizer)
        self._recurrent_regularizer = regularizers.get(recurrent_regularizer)
        self._bias_regularizer = regularizers.get(bias_regularizer)

        # Define custom bias initializer in case unit forget bias flag is set
        if self._unit_forget_bias:
            class UnitForgetBias(init_ops.Initializer):
                def __init__(self, dtype=tf.float32):
                    self.dtype = dtype

                def __call__(self, shape, dtype=None, partition_info=None):
                    if dtype is None:
                        dtype = self.dtype

                    assert shape[0] == filters*4

                    i_b = initializers.get(bias_initializer)(filters)
                    f_b = tf.ones(filters)
                    r_b = initializers.get(bias_initializer)(filters*2)

                    return tf.concat([i_b, f_b, r_b], axis=0)

            self._bias_initializer = UnitForgetBias

        state_size = tensor_shape.TensorShape(self._input_shape[:-1] + (self._filters,))
        self._state_size = rnn_cell_impl.LSTMStateTuple(state_size, state_size)
        self._output_size = state_size

        # We chose to implement a variational inference dropout that uses the same mask
        # at every time step for inputs, recurrent activations and outputs. For efficiency,
        # we chose to use a tied weights implementation, which is reported to have a
        # slightly worse performance than an untied weights implementation (https://arxiv.org/pdf/1512.05287.pdf)
        self._input_dropout_keep_prob = input_dropout_keep_prob
        self._recurrent_dropout_keep_prob = recurrent_dropout_keep_prob
        self._output_dropout_keep_prob = output_dropout_keep_prob

        self._input_dropout_mask = None
        self._recurrent_dropout_mask = None
        self._output_dropout_mask = None

        kernel_shape = self._kernel_size + (self._input_shape[-1], self._filters*4)
        recurrent_kernel_shape = self._kernel_size + (self._filters, self._filters*4)
        bias_shape = (self._filters*4,)

        self._kernel = self.add_weight(
            name="kernel",
            shape=kernel_shape,
            dtype=tf.float32,
            initializer=self._kernel_initializer,
            regularizer=self._kernel_regularizer,
            trainable=True
        )
        self._recurrent_kernel = self.add_weight(
            name="recurrent_kernel",
            shape=recurrent_kernel_shape,
            dtype=tf.float32,
            initializer=self._recurrent_initializer,
            regularizer=self._recurrent_regularizer,
            trainable=True
        )
        if self._use_bias:
            self._bias = self.add_weight(
                name="bias",
                shape=bias_shape,
                dtype=tf.float32,
                initializer=self._bias_initializer,
                regularizer=self._bias_regularizer,
                trainable=True
            )

    @property
    def output_size(self):
        return self._output_size

    @property
    def state_size(self):
        return self._state_size

    def _get_dropout_mask(self, input, keep_prob, name="dropout_mask"):
        # In case we want to keep all activations
        if isinstance(keep_prob, numbers.Real) and keep_prob == 1.0:
            return None

        with tf.name_scope(name):
            input_shape = tf.shape(input, name="shape")
            mask = tf.random.uniform(shape=input_shape, minval=0, maxval=1.0)
            mask += keep_prob
            mask = tf.math.floor(mask)
        return mask

    def call(self, inputs, state, scope=None):
        cell_prev, hidden_prev = state

        # On the first call we instantiate the dropout mask
        if self._input_dropout_mask is None:
            self._input_dropout_mask = self._get_dropout_mask(
                input=inputs,
                keep_prob=self._input_dropout_keep_prob,
                name="input_dropout_mask"
            )

        if self._input_dropout_mask is not None:
            inputs /= self._input_dropout_keep_prob
            inputs *= self._input_dropout_mask


        if self._recurrent_dropout_mask is None:
            self._recurrent_dropout_mask = self._get_dropout_mask(
                input=hidden_prev,
                keep_prob=self._recurrent_dropout_keep_prob,
                name="recurrent_dropout_mask"
            )

        if self._recurrent_dropout_mask is not None:
            hidden_prev /= self._recurrent_dropout_keep_prob
            hidden_prev *= self._recurrent_dropout_mask

        x = tf.nn.conv2d(inputs, self._kernel, self._strides, self._padding, name='input_conv')
        h = tf.nn.conv2d(hidden_prev, self._recurrent_kernel, self._strides, self._padding, name='recurrent_conv')
        gates = tf.math.add(x, h)

        if self._use_bias:
            gates = tf.nn.bias_add(gates, self._bias)

        # i = input_gate, f = forget_gate, j = new_cell_state, o = output_gate
        i, f, c, o = tf.split(
            value=gates,
            num_or_size_splits=4,
            axis=-1
        )

        i_g = tf.identity(self._recurrent_activation(i), name='input_gate')
        f_g = tf.identity(self._recurrent_activation(f), name='forget_gate')
        c_g = tf.identity(self._activation(c), name='cell_gate')
        o_g = tf.identity(self._recurrent_activation(o), name='output_gate')

        cell = tf.math.add(
            x=f_g*cell_prev,
            y=i_g*c_g,
            name='cell_state')
        hidden = tf.math.multiply(
            x=o_g,
            y=self._activation(cell),
            name='hidden_output')

        if self._output_dropout_mask is None:
            self._output_dropout_mask = self._get_dropout_mask(
                input=hidden,
                keep_prob=self._output_dropout_keep_prob,
                name="output_dropout_mask"
            )

        if self._output_dropout_mask is not None:
            hidden /= self._output_dropout_keep_prob
            hidden *= self._output_dropout_mask

        new_state = rnn_cell_impl.LSTMStateTuple(cell, hidden)
        return hidden, new_state
