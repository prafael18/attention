import cv2
import numpy as np
import tensorflow as tf

class Logger(object):
    """Logging in tensorboard without tensorflow ops."""

    def __init__(self, log_dir, graph=None):
        """Creates a summary writer logging to log_dir."""
        self.writer = tf.summary.FileWriter(log_dir, graph)

    def log_scalar(self, tag, value, step):
        """Log a scalar variable.
        Parameter
        ----------
        tag : basestring
            Name of the scalar
        value
        step : int
            training iteration
        """
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag,
                                                     simple_value=value)])
        self.writer.add_summary(summary, step)

    def log_images(self, tag, images, step):
        """Logs a list of images."""

        images = images[..., ::-1] # cv2 expects BGR images when encoding
        images = images.astype(np.uint8)
        im_summaries = []
        for nr, img in enumerate(images):
            # Write the image to a string
            encoded_image = cv2.imencode('.png', img)[1].tobytes()

            # Create an Image object
            img_sum = tf.Summary.Image(encoded_image_string=encoded_image,
                                       height=img.shape[0],
                                       width=img.shape[1],
                                       colorspace=img.shape[2])
            # Create a Summary value
            im_summaries.append(tf.Summary.Value(tag='%s/%d' % (tag, nr),
                                                 image=img_sum))

        # Create and write Summary
        summary = tf.Summary(value=im_summaries)
        self.writer.add_summary(summary, step)
