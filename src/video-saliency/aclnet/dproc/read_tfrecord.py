import os
import glob
import time
import argparse

import cv2
import numpy as np
import tensorflow as tf

from util import show_sample


def parse_frame(feature_name, frame_nr, example, flags):
    byte_string = example.features.feature[feature_name].bytes_list.value[frame_nr]
    encoded_frame = np.fromstring(byte_string, dtype=np.uint8)
    frame = cv2.imdecode(encoded_frame, flags)
    if flags == cv2.IMREAD_COLOR:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    return frame

def parse_int(feature_name, example):
    return example.features.feature[feature_name].int64_list.value[0]

def parse_video(feature_name, example, flags):
    video = []
    byte_list = example.features.feature[feature_name].bytes_list.value
    for i in range(len(byte_list)):
        frame = parse_frame(feature_name, i, example, flags)
        video.append(frame)
        video = np.stack(video)
    return video

def show_tfrecords(base_dir, dataset, show):

    if not dataset:
        print('Please specify the name of the split that you want to display tfrecords for')
        raise ValueError

    tfr_dir = os.path.join(base_dir, 'tfr', dataset, '*')
    tfr_fps = glob.glob(tfr_dir)
    total_nr_records = 0
    for tfp in tfr_fps:
        record_iterator = tf.python_io.tf_record_iterator(path=tfp)
        nr_records = 0
        for string_record in record_iterator:
            example = tf.train.Example()
            example.ParseFromString(string_record)

            width = parse_int('width', example)
            height = parse_int('height', example)
            nr_frames = parse_int('nr_frames', example)

            for i in range(nr_frames):
                map = parse_frame('maps', i, example, flags=cv2.IMREAD_GRAYSCALE)
                image = parse_frame('images', i, example, flags=cv2.IMREAD_COLOR)
                fixation = parse_frame('fixations', i, example, flags=cv2.IMREAD_GRAYSCALE)

                if show == 'frames':
                    print("Key press = iterate, Mouse click = next video\r", end=" ")
                    features = (image, fixation, map)
                    if not show_sample(features):
                        break

                elif show == 'video':
                    wait_time = int(1000/30)
                    feats = np.ones((2*height, 2*width, 3), dtype=np.uint8)*255
                    feats[:height, :width] = np.expand_dims(fixation, axis=2)
                    feats[:height, width:] = np.expand_dims(map, axis=2)
                    feats[height:, width >> 1:-(width >> 1)] = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                    cv2.putText(feats, str(nr_records + 1), (5+0, 2*height-10), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 0), 1)
                    cv2.imshow('Features', feats)
                    key = cv2.waitKey(wait_time)
                    if key & 0xFF == ord('q'):
                        break
            if show:
                time.sleep(0.5)
            nr_records+=1
        print(f"Total number of records in file {tfp} is {nr_records}")
        total_nr_records += nr_records
    print(f"Total number of records in directory {tfr_dir} is {total_nr_records}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--base_dir', action='store', required=True,
                                    help='Base dataset directory where all subject dirs are located')
    parser.add_argument('--train', action='store_const', const='train', dest='dataset',
                                    help='Flag indicating whether we want to display the training set tfrecords')
    parser.add_argument('--val', action='store_const', const='val', dest='dataset',
                                    help='Flag indicating whether we want to display the validation set tfrecords')
    parser.add_argument('--test', action='store_const', const='test', dest='dataset',
                                    help='Flag indicating whether we want to display the test set tfrecords')
    parser.add_argument('--show_frames', action='store_const', const='frames', dest='show',
                                    help='Show individual frames for each example in record')
    parser.add_argument('--show_video', action='store_const', const='video', dest='show',
                                    help='Play each example in record as a video')

    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    show_tfrecords(**kwargs)
