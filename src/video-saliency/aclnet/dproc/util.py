import numpy as np
import scipy.io as io
import tensorflow as tf
import matplotlib.pyplot as plt

def load_salicon_fixation(path):
    mat = io.loadmat(path)
    shape = tuple(mat['resolution'][0])
    fixation = np.zeros(shape, dtype=np.uint8)
    subj_fixs = mat['gaze']['fixations']
    for subj in subj_fixs:
        for fix in subj[0]:
            # Subtract one from each dimension because fixations are indexed
            # starting from 1
            pos = tuple(fix[::-1]-np.array([1,1]))
            fixation[pos] = 255
    return fixation

def byte_feature(strings):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=strings))

def float_feature(floats):
    return tf.train.Feature(float_list=tf.train.FloatList(value=floats))

def int_feature(ints):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=ints))

def show_sample(features):
    image, fixation, map = features
    fig = plt.figure(num='Features', figsize=(10,10))
    fig.clear()
    axs = fig.subplots(nrows=2, ncols=2)
    axs[0,0].imshow(fixation, cmap='gray')
    axs[0,1].imshow(map, cmap='gray')
    gs = axs[1,0].get_subplotspec().get_gridspec()
    axs[1,0].remove()
    axs[1,1].remove()
    axbig = fig.add_subplot(gs[1,:])
    axbig.imshow(image)
    return fig.waitforbuttonpress()
