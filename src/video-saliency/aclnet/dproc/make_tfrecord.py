import os
import glob
import math
import argparse

import cv2
import numpy as np
import tensorflow as tf

import util
import sample

def clean_dir(tfr_base_dir):
    splits = ['train', 'val', 'test']
    dirs = list(map(lambda split: os.path.join(tfr_base_dir, split), splits))
    for d in dirs:
        fps = glob.glob(os.path.join(d, '*'))
        for f in fps:
            os.remove(f)

def write_sample(writer, sample):
    # Create feature dictionary
    features = {
        'nr_frames': util.int_feature([sample['nr_frames']]),
        'height': util.int_feature([sample['height']]),
        'width': util.int_feature([sample['width']]),
        'images' : util.byte_feature(sample['images']),
        'fixations' : util.byte_feature(sample['fixations']),
        'maps': util.byte_feature(sample['maps'])
    }

    example = tf.train.Example(features=tf.train.Features(feature=features))
    writer.write(example.SerializeToString())
    return

def parse_samples(name, split, raw_dir, tfr_dir, video_dirs, samples_per_file):

    print(f"Parsing subjects for {name}'s {split} split:")

    # Create a separate directory to store the tfrecords of corresponding dataset split
    tfr_fp = os.path.join(tfr_dir, split)
    os.makedirs(tfr_fp, exist_ok=True)

    # Calculate vars used to print progress
    nr_videos = max(len(video_dirs), 1)
    video_width = 1/nr_videos

    # Intialize counters
    file_id, vid_nr, writer = 1, 0, None

    while vid_nr < len(video_dirs):

        # Open tfrecord writer
        if vid_nr%samples_per_file == 0:
            if writer:
                writer.close()
            tfr_filename = os.path.join(tfr_fp, f'{split}-{file_id}.tfr')
            writer = tf.python_io.TFRecordWriter(tfr_filename)
            file_id += 1

        # Intial sample list is empty
        video_sample = {'height': None, 'width': None, 'nr_frames': None,
                        'images':[], 'fixations':[], 'maps':[]}

        # Instantiate Sample according to its dataset
        vd = video_dirs[vid_nr]
        if name == 'dhf1k':
            base_dir = os.path.dirname(raw_dir)
            video = sample.DHF1KSample(base_dir, vd)
        elif name == 'ucf':
            video = sample.UCFSample(vd)
        elif name == 'hollywood':
            video = sample.HollywoodSample(vd)
        else:
            base_dir = os.path.dirname(raw_dir)
            video = sample.SALICONSample(base_dir, vd)

        video_sample['nr_frames'] = int(video.frame_cnt)
        for frame_nr, frame in enumerate(video.frame_iterator()):
            if frame_nr == 0:
                video_sample['height'] = frame[0].shape[0]
                video_sample['width'] = frame[0].shape[1]

            # Encode features as pngs
            encoded_image, encoded_fixation, encoded_map = tuple(map(
                lambda feat: tf.compat.as_bytes(cv2.imencode('.png', feat)[1].tobytes()),
                frame
            ))

            # Add individual features to video sample lists
            video_sample['images'].append(encoded_image)
            video_sample['fixations'].append(encoded_fixation)
            video_sample['maps'].append(encoded_map)

            # Print current progress
            progress = ((vid_nr/nr_videos) + (frame_nr/video_sample['nr_frames'])*video_width)*100
            print(f'\r\tProgress ({vid_nr}/{nr_videos}): {progress:.2f}%    ', end='')

        write_sample(writer, video_sample)
        vid_nr += 1

    # Print final progress for split
    print(f'\r\tProgress ({nr_videos}/{nr_videos}): {100:.2f}%    ')
    return

def main(raw_base_dir, tfr_base_dir, dhf1k, ucf, hollywood, salicon, rm, samples_per_file):

    if dhf1k + ucf + hollywood + salicon != 1:
        print("Exactly one dataset must be specified at a time to generate tfrecords for.")
        raise NotImplementedError

    dirs = os.listdir(raw_base_dir)
    if 'raw' not in dirs:
        print(f"Specified base dir ({raw_base_dir}) is missing a `raw` directory.")
        raise ValueError

    raw_dir = os.path.join(raw_base_dir, 'raw')
    tfr_dir = os.path.join(tfr_base_dir, 'tfr')
    os.makedirs(tfr_dir, exist_ok=True)

    if rm:
        clean_dir(tfr_dir)
        print(f"Cleaned up base tfrecord directory: {tfr_dir}.")

    if dhf1k:
        dataset_name = 'dhf1k'
        tr = list(range(1, 601))
        va = list(range(601, 701))
        te = []
    elif ucf or hollywood:

        te = glob.glob(os.path.join(raw_dir, 'testing', '*'))
        # Get all training and validation filepaths
        fps = np.array(glob.glob(os.path.join(raw_dir, 'training', '*')))

        if ucf:
            dataset_name = 'ucf'

            # Permute indices for filepaths
            indices = np.random.permutation(range(len(fps)))

            # Sample 10% of files randomly for validation split
            sep = math.ceil(len(fps)/10)
            va = fps[indices <  sep]
            tr = fps[indices >= sep]
        else:
            dataset_name = 'hollywood'
            ids = np.arange(1, 824)
            indices = np.random.permutation(range(len(ids)))

            # Sample 10% of ids randomly for validation split
            sep = math.ceil(len(ids)/10)
            val_ids = ids[indices <  sep]
            train_ids = ids[indices >= sep]

            va = [f for f in fps
                if int(os.path.basename(f).split('train')[-1].split('_')[0]) in val_ids]
            tr = [f for f in fps
                if int(os.path.basename(f).split('train')[-1].split('_')[0]) in train_ids]

            assert len(va) + len(tr) == len(fps)
    else:
        dataset_name = 'salicon'
        te = []
        va = glob.glob(os.path.join(raw_dir, 'image', 'val', '*'))
        tr = glob.glob(os.path.join(raw_dir, 'image', 'train', '*'))

    total_samples = len(tr) + len(va) + len(te)
    print(f'Samples per file = {samples_per_file}')
    print(f'Total of {total_samples} samples.\n'\
          f'  Train: {len(tr)}\n'\
          f'  Validation: {len(va)}\n'\
          f'  Test: {len(te)}')

    parse_samples(dataset_name, 'val', raw_dir, tfr_dir, va, samples_per_file)
    parse_samples(dataset_name, 'test', raw_dir, tfr_dir, te, samples_per_file)
    parse_samples(dataset_name, 'train', raw_dir, tfr_dir, tr, samples_per_file)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--raw_base_dir', action='store', required=True,
                        help='Base dataset directory where all subject dirs are located')
    parser.add_argument('-o', '--tfr_base_dir', action='store', required=True,
                        help='Base dataset directory where all tfrecords should be stored')
    parser.add_argument('-s', '--samples_per_file', action='store', type=int, default=20,
                        help='Number of video samples to store per tfrecord')
    parser.add_argument('--dhf1k', action='store_true', required=False,
                        help='Create tfrecords for dhf1k dataset')
    parser.add_argument('--ucf', action='store_true', required=False,
                        help='Create tfrecords for ucf dataset')
    parser.add_argument('--hollywood', action='store_true', required=False,
                        help='Create tfrecords for hollywood dataset')
    parser.add_argument('--salicon', action='store_true', required=False,
                        help='Create tfrecords for salicon dataset')
    parser.add_argument('-r', '--rm', action='store_true', required=False,
                        help='Flag indicating wether or not to clean tfrecord directory')

    # Get arguments as a dictionary
    kwargs = vars(parser.parse_args())

    main(**kwargs)
