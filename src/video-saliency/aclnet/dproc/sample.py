# Standard libs
import os
import glob

# Third-party libs
import cv2
import scipy
import numpy as np
import matplotlib.pyplot as plt

# Local libs
import util

class DHF1KSample:
    def __init__(self, base_dir, sample_id):
        video_dir = os.path.join(base_dir, 'raw', 'video')
        annotation_dir = os.path.join(base_dir, 'raw', 'annotation', f'{sample_id:04d}')
        self.fixation_dir = os.path.join(annotation_dir, 'fixation')
        self.map_dir = os.path.join(annotation_dir, 'maps')
        self.video_cap = cv2.VideoCapture(os.path.join(video_dir, f'{sample_id:03d}.AVI'))
        self.frame_cnt = self.video_cap.get(cv2.CAP_PROP_FRAME_COUNT)

    def frame_iterator(self):
        frame_nr = 0
        while True:
            ret, image = self.video_cap.read()

            # In case no frame has been grabbed
            if not ret:
                break

            frame_nr += 1
            fixation = cv2.imread(
                filename=os.path.join(self.fixation_dir, f'{frame_nr:04d}.png'),
                flags=cv2.IMREAD_GRAYSCALE
            )
            map = cv2.imread(
                filename=os.path.join(self.map_dir, f'{frame_nr:04d}.png'),
                flags=cv2.IMREAD_GRAYSCALE
            )

            yield image, fixation, map

class UCFSample:
    def __init__(self, sample_dir):
        self.map_dir = os.path.join(sample_dir, 'maps')
        self.image_dir = os.path.join(sample_dir, 'images')
        self.fixation_dir = os.path.join(sample_dir, 'fixation')
        image_fps = glob.glob(os.path.join(self.image_dir, '*'))
        self.frame_cnt = len(image_fps)
        self.basenames = [os.path.basename(fp) for fp in image_fps]
        self.basenames.sort()

    def frame_iterator(self):
        for bn in self.basenames:
            image = cv2.imread(
                filename=os.path.join(self.image_dir, bn),
                flags=cv2.IMREAD_COLOR
            )
            fixation = cv2.imread(
                filename=os.path.join(self.fixation_dir, bn),
                flags=cv2.IMREAD_GRAYSCALE
            )
            map = cv2.imread(
                filename=os.path.join(self.map_dir, bn),
                flags=cv2.IMREAD_GRAYSCALE
            )
            yield image, fixation, map

class HollywoodSample(UCFSample):
    def __init__(self, sample_dir):
        super(HollywoodSample, self).__init__(sample_dir)

    def frame_iterator(self):
        yield from super(HollywoodSample, self).frame_iterator()

class SALICONSample:
    def __init__(self, base_dir, sample_path):
        raw_dir = os.path.join(base_dir, 'raw')
        sample_dir, basename = os.path.split(sample_path)
        bn, _ = os.path.splitext(basename)
        split = os.path.basename(sample_dir)

        self.image_path = os.path.join(raw_dir, 'image', split, bn + '.jpg')
        self.fixation_path = os.path.join(raw_dir, 'fixations', split, bn + '.mat')
        self.map_path = os.path.join(raw_dir, 'maps', split, bn + '.png')
        self.frame_cnt = 1

    def frame_iterator(self):
        image = cv2.imread(self.image_path, cv2.IMREAD_COLOR)
        map = cv2.imread(self.map_path, cv2.IMREAD_GRAYSCALE)
        fixation = util.load_salicon_fixation(self.fixation_path)
        yield image, fixation, map
