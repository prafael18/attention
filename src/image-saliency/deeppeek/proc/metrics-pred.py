import numpy as np
import glob
import os
from scipy.misc import imread, imsave, imresize, imshow

def sim(sim_list, pred, label):
    # Pre-process data:
    # (1) Normalize pred and label between 0 and 1
    # (2) Make sure that all pixel values add up to 1

    pred = (pred - np.min(pred))/(np.max(pred)-np.min(pred))
    label = (label - np.min(label))/(np.max(label)-np.min(label))
    pred = pred/np.sum(pred)
    label = label/np.sum(label)
    sim_coeff = np.minimum(pred, label)
    sim_list.append(np.sum(sim_coeff))

    return


def cc(cc_list, pred, label):
    # Pred and label have shapes (batch_size, frames, height, width, channels)

    # Normalize data to have mean 0 and variance 1
    pred = (pred - np.mean(pred)) / np.std(pred)
    label = (label - np.mean(label)) / np.std(label)

    # Calculate correlation coefficient for every frame
    pd = pred - np.mean(pred)
    ld = label - np.mean(label)
    cc_list.append((pd * ld).sum() / np.sqrt((pd * pd).sum() * (ld * ld).sum()))

    return


def mse(mse_list, pred, label):
    pred = (pred - np.min(pred))/(np.max(pred)-np.min(pred))
    label = (label - np.min(label))/(np.max(label)-np.min(label))
    mse_list.append(np.mean((pred-label)**2))
    return

def append_metrics(pred_id, mse_list, cc_list, sim_list):
    label_fns = glob.glob(os.path.join(pred_id, "*y-true.png"))
    for label in label_fns:
        bn = os.path.basename(label)
        dn = os.path.dirname(label)
        n = bn.split("y-true")[0]
        pred = glob.glob(os.path.join(dn, n + "y-pred.png"))
        pd = imread(pred[0])
        lb = imread(label)
        if np.max(lb) == 0:
            continue
        mse(mse_list, pd.copy(), lb.copy())
        cc(cc_list, pd.copy(), lb.copy())
        sim(sim_list, pd.copy(), lb.copy())

def main():
    mse_list = []
    sim_list = []
    cc_list = []

    append_metrics("preds-2", mse_list, cc_list, sim_list)
    append_metrics("preds-3", mse_list, cc_list, sim_list)

    print("SIM = {:.4f}".format(np.mean(sim_list)))
    print("CC = {:.4f}".format(np.mean(cc_list)))
    print("MSE = {:.4f}".format(np.mean(mse_list)))

if __name__ == "__main__":
    main()
