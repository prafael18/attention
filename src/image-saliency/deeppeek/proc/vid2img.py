import numpy as np
import cv2
from tensorflow.python.platform import gfile
import os

#input_path = "/home/rafael/Documents/ic/data/savam/videos/stimuli"
input_path = ["/home/panda/raw_data/train/inputs", "/home/panda/raw_data/val/inputs", "/home/panda/raw_data/test/inputs"]
input_dest_dir = ["/home/panda/erik/data/savam/train/stimuli", "/home/panda/erik/data/savam/test/stimuli"]
label_path = ["/home/panda/raw_data/train/labels", "/home/panda/raw_data/val/labels", "/home/panda/raw_data/test/labels"]
label_dest_dir = ["/home/panda/erik/data/savam/train/fixmaps", "/home/panda/erik/data/savam/test/fixmaps"]
file_suffix = "*.avi"


def vid2img(filenames, dest_dir):

    if not os.path.isdir(dest_dir):
        print(dest_dir)
        os.makedirs(dest_dir)
        print("Creating {} dir.".format(dest_dir))

    total_frame_cnt = 0
    for fn in filenames:
        frame_cnt = 0
        cap = cv2.VideoCapture(fn)
        ret, frame = cap.read()
        while ret:
            img_name = fn.split("/")[-1].split(".")[0]
            img_fn = img_name + "_" + str(frame_cnt+1) + ".png"
            print(img_fn)
            cv2.imwrite(os.path.join(dest_dir, img_fn), frame)
            frame_cnt += 1
            total_frame_cnt += 1
            ret, frame = cap.read()
        cap.release()
    print ("Total frames processed are ", total_frame_cnt)

def rename(filenames, dest_dir):
    for fn in filenames:
        bn = os.path.basename(fn)
        new_fn = os.path.join(dest_dir, bn)
        os.rename(fn, new_fn)

def main():
    #test_base_dst_dir = "/home/rafael/Documents/ic/data/savam/test"
    #test_set_ids = ["v05", "v07", "v20", "v22", "v23", "v25", "v33", "v38"]
    #for id in test_set_ids:
    #    input_filenames = gfile.Glob(os.path.join(input_dest_dir, id+"*"))
    #    label_filenames = gfile.Glob(os.path.join(label_dest_dir, id+"*"))
    #    rename(input_filenames, os.path.join(test_base_dst_dir, "stimuli"))
    #    rename(label_filenames, os.path.join(test_base_dst_dir, "fixmaps"))

  input_filenames = []
  label_filenames = []
  for ip in input_path[-1:]:
    input_filenames = input_filenames + gfile.Glob(os.path.join(ip, file_suffix))

  for lp in label_path[-1:]:
    label_filenames = label_filenames + gfile.Glob(os.path.join(lp, file_suffix))

  vid2img(filenames=input_filenames, dest_dir=input_dest_dir[1])
  vid2img(filenames=label_filenames, dest_dir=label_dest_dir[1])




if __name__ == "__main__":
    main()
