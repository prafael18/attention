# Papers
Papers are subdivided into their respective topics and are named in the form:
```
[<main authors> <pub_year>] <title>
```
Most of the papers that we chose to study were selected due to their exceptional results in the MIT300 [1] and DHF1K [2] benchmarks.  

1. http://saliency.mit.edu/results_mit300.html
2. https://mmcheng.net/videosal/
